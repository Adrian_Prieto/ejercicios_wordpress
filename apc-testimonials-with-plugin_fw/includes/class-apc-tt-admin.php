<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_TT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_TT_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class APC_TT_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_tt_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		// @var YIT_Plugin_Panel $_panel the panel used by YITH Plugin Panel (no WooCommerce)
		private $_panel;

		/**
		 * @var Panel page
		 */
		protected $panel_page = 'apc_testimonials_with_plugin_fw';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_TT_Admin constructor.
		 */
		private function __construct() {

			/** Admin menu Plugin FW */
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );

			/* Link transient functions */
			add_action( 'save_post', 'apc_tt_update_transient' );   // Updating after saving changes (even creating posts).
			add_action( 'delete_post', 'apc_tt_update_transient' ); // Updating after removing post.
			add_filter( 'option_page_capability_ps-options-page', array( $this, 'allow_menu_page_options' ) );

			/* Plugin fw filters */
			add_filter( 'plugin_action_links_' . plugin_basename( APC_TT_DIR_PATH . '/' . basename( APC_TT_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			/*Plugin FW Metabox*/
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 );
		}

		/** Main function of Plugin FW metabox */
		public function add_metabox() {
			$args = array(
				'label'    => __( 'More Info', 'apc_testimonial' ),
				'pages'    => 'apc_testimonial',
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array(
						'label'  => __( 'Settings', 'apc_testimonial' ),
						'fields' => array(
							'apc_tt_role' => array(
								'label'   => __( 'Role', 'your-plugin-text-domain' ),
								'desc'    => __( 'Role of the Testimonial.', 'apc_testimonial' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
							),
							'apc_tt_company' => array(
								'label'   => __( 'Company', 'your-plugin-text-domain' ),
								'desc'    => __( 'Company of the Testimonial.', 'apc_testimonial' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
							),
							'apc_tt_company_url' => array(
								'label'   => __( 'Company Url', 'your-plugin-text-domain' ),
								'desc'    => __( 'Official Url of the Company.', 'apc_testimonial' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
							),
							'apc_tt_email' => array(
								'label'   => __( 'Email', 'your-plugin-text-domain' ),
								'desc'    => __( 'Email of the testimonial.', 'apc_testimonial' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
							),
							'apc_tt_email' => array(
								'label'   => __( 'Email', 'your-plugin-text-domain' ),
								'desc'    => __( 'Email of the testimonial.', 'apc_testimonial' ),
								'type'    => 'checkbox',
								'private' => true,
								'std'     => 'yes',
							),
							'apc_tt_badge' => array(
								'label'   => __( 'Badge', 'your-plugin-text-domain' ),
								'desc'    => __( 'Enable badge for the testimonial.', 'apc_testimonial' ),
								'type'    => 'checkbox',
								'private' => true,
								'std'     => 'yes',
							),
							'apc_tt_badge_text' => array(
								'label'   => __( 'Badge Text', 'your-plugin-text-domain' ),
								'desc'    => __( 'Text printed on the badge.', 'apc_testimonial' ),
								'type'    => 'text',
								'private' => true,
								'std'     => '',
							),
							'apc_tt_badge_color' => array(
								'label'          => __( 'Badge Color', 'apc_testimonial' ),
								'type'          => 'colorpicker',
								'std'           => '#effeff',
								'desc'          => __( 'Color of which the badge will be displayed.', 'apc_testimonial' ),
								'alpha_enabled' => false,
								'private' => true,
							),
							'apc_tt_rating' => array(
								'id'            => 'tst_rating',
								'class'         => 'star__radio visuhide',
								'label'         => __( 'Badge Color', 'apc_testimonial' ),
								'type'          => 'radio',
								'std'           => '#effeff',
								'desc'          => __( 'Color of which the badge will be displayed.', 'apc_testimonial' ),
								'alpha_enabled' => false,
								'private'       => true,
								'options'       => array(
									'1' => '☆',
									'2' => '☆',
									'3' => '☆',
									'4' => '☆',
									'5' => '☆',
								),
							),
						),
					),
				),
			);
			$metabox1 = YIT_Metabox( 'yith_metabox_test' );
			$metabox1->init( $args );
		}


		/** Main function of YITH_Plugin Panel */
		public function register_panel() {
			if ( !empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings'     => __( 'Settings', 'apc_testimonial' ),
				'testimonials' => __( 'Testimonials', 'apc_testimonial' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'APC Testimonials', // this text MUST be NOT translatable
				'menu_title'         => 'APC Testimonials', // this text MUST be NOT translatable
				'plugin_description' => __( 'General options for APC Testimonials', 'apc_testimonial' ),
				'capability'         => 'edit_apc_testimonials',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'apc_testimonials',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'apc_testimonials_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => APC_TT_DIR_PATH,
				'options-path'       => APC_TT_DIR_PATH . 'plugin-options'
			);

			$this->_panel = new YIT_Plugin_Panel( $args );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( APC_TT_INIT === $plugin_file ) {
				$row_meta_args['slug']       = APC_TT_SLUG;
				$row_meta_args['is_premium'] = true;
			}
			return $row_meta_args;
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, APC_TT_SLUG );
		}

		/** Used by Security Reasons of the Testimonial Settings */
		public function allow_menu_page_options() {
			return 'edit_apc_testimonials';
		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'apc-tt-additional-information',
				__( 'Additional information', 'apc_testimonial' ),
				array(
					$this,
					'add_metabox',
				),
				APC_TT_Post_Types::$post_type
			);
		}

		/**
		 * View meta boxes
		 *
		 * @param object $post Inline argument of WordPress.
		 */
		public function view_meta_boxes( $post ) {
			apc_tt_get_view( '/metaboxes/plugin-skeleton-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save meta box values
		 *
		 * @param int $post_id Inline argument.
		 */
		public function save_meta_box( $post_id ) {

			if ( get_post_type( $post_id ) !== APC_TT_Post_Types::$post_type ) {
				return;
			}

			if ( isset( $_POST['my_apc_testimonial_nonce'] ) ) {

				$nonce = sanitize_text_field( wp_unslash( $_POST['my_apc_testimonial_nonce'] ) );

				if ( ! wp_verify_nonce( $nonce, 'apc_testimonial_nonce' ) ) {
					die( esc_html( 'Security check' ) );
				} else {
					// Save metadatas from form.
					if ( isset( $_POST['_apc_tt_role'] ) ) {
						update_post_meta( $post_id, '_apc_tt_role', sanitize_text_field( wp_unslash( $_POST['_apc_tt_role'] ) ) );
					}
					if ( isset( $_POST['_apc_tt_company'] ) ) {
						update_post_meta( $post_id, '_apc_tt_company', sanitize_text_field( wp_unslash( $_POST['_apc_tt_company'] ) ) );
					}
					if ( isset( $_POST['_apc_tt_company_url'] ) ) {
						update_post_meta( $post_id, '_apc_tt_company_url', sanitize_text_field( wp_unslash( $_POST['_apc_tt_company_url'] ) ) );
					}
					if ( isset( $_POST['_apc_tt_email'] ) ) {
						update_post_meta( $post_id, '_apc_tt_email', sanitize_text_field( wp_unslash( $_POST['_apc_tt_email'] ) ) );
					}

					if ( isset( $_POST['_apc_tt_vip'] ) ) {
						update_post_meta( $post_id, '_apc_tt_vip', 1 );
					} else { // Not marked vip.
						update_post_meta( $post_id, '_apc_tt_vip', 0 );
					}

					if ( isset( $_POST['_apc_tt_badge'] ) ) {
						update_post_meta( $post_id, '_apc_tt_badge', 1 );
					} else { // Not marked vip.
						update_post_meta( $post_id, '_apc_tt_badge', 0 );
					}

					if ( isset( $_POST['_apc_tt_badge_text'] ) ) {
						update_post_meta( $post_id, '_apc_tt_badge_text', sanitize_text_field( wp_unslash( $_POST['_apc_tt_badge_text'] ) ) );
					}
					if ( isset( $_POST['_apc_tt_badge_color'] ) ) {
						update_post_meta( $post_id, '_apc_tt_badge_color', sanitize_text_field( wp_unslash( $_POST['_apc_tt_badge_color'] ) ) );
					}
					if ( isset( $_POST['_apc_tt_rating'] ) ) {
						update_post_meta( $post_id, '_apc_tt_rating', sanitize_text_field( wp_unslash( $_POST['_apc_tt_rating'] ) ) );
					}
				}
			}
		}
		/**
		 * Filters the columns displayed in the Posts list table for plugin skeleton post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_apc_testimonial_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'apc_testimonial_custom_columns',
				array(
					'role'    => esc_html__( 'Role', 'apc_testimonial' ),
					'company' => esc_html__( 'Company', 'apc_testimonial' ),
					'email'   => esc_html__( 'Email', 'apc_testimonial' ),
					'stars'   => esc_html__( 'Stars', 'apc_testimonial' ),
					'vip'     => esc_html__( 'VIP', 'apc_testimonial' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_apc_testimonial_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {
				case 'role':
					$value_role = ( ! empty( get_post_meta( $post_id, '_apc_tt_role', true ) ) ) ? get_post_meta( $post_id, '_apc_tt_role', true ) : 'Empty';
					echo esc_html( $value_role );
					break;
				case 'company':
					$value_company = ( ! empty( get_post_meta( $post_id, '_apc_tt_company', true ) ) ) ? get_post_meta( $post_id, '_apc_tt_company', true ) : 'Empty';
					echo esc_html( $value_company );
					$value_company_url = get_post_meta( $post_id, '_apc_tt_company_url', true );
					if ( ! empty( get_post_meta( $post_id, '_apc_tt_company', true ) ) ) {
						echo ' at <a href="https://' . esc_html( $value_company_url ) . '">' . esc_html( $value_company_url ) . '</a>';
					}
					break;
				case 'email':
					$value_email = ( ! empty( get_post_meta( $post_id, '_apc_tt_email', true ) ) ) ? get_post_meta( $post_id, '_apc_tt_email', true ) : 'Empty';
					echo esc_html( $value_email );
					break;
				case 'stars':
					$value_stars = ( ! empty( get_post_meta( $post_id, '_apc_tt_rating', true ) ) ) ? get_post_meta( $post_id, '_apc_tt_rating', true ) : 'Empty';
					echo esc_html( $value_stars );
					break;
				case 'vip':
					$value_vip = get_post_meta( $post_id, '_apc_tt_vip', true ) ? 'True' : 'False';
					echo esc_html( $value_vip );
					break;

				default:
					do_action( 'apc_testimonial_display_custom_column', $column, $post_id );
					break;
			}

		}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Testimonial Options', 'apc_testimonial' ),
					esc_html__( 'Testimonial Options', 'apc_testimonial' ),
					'edit_apc_testimonials',
					'plugin_skeleton_options',
					array( $this, 'apc_testimonial_custom_menu_page' ),
					'',
					40
				);

		}
		/** Callback custom menu page */
		public function apc_testimonial_custom_menu_page() {
			apc_tt_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/** Add the fields in the shortcode attribute management page */
		public function register_settings() {

			$page_name    = 'ps-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'apc_tt_shortcode_number',
					'title'    => esc_html__( 'Number', 'apc_testimonial' ),
					'callback' => 'print_number_input',
				),
				array(
					'id'       => 'apc_tt_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'apc_testimonial' ),
					'callback' => 'print_show_image',
				),

				array(
					'id'       => 'apc_tt_shortcode_hover_effect',
					'title'    => esc_html__( 'Hover effect', 'apc_testimonial' ),
					'callback' => 'print_hover_effect',
				),

				array(
					'id'       => 'apc_tt_shortcode_border_radius',
					'title'    => esc_html__( 'Border radius', 'apc_testimonial' ),
					'callback' => 'print_border_radius',
				),

				array(
					'id'       => 'apc_tt_shortcode_color_link',
					'title'    => esc_html__( 'Color link', 'apc_testimonial' ),
					'callback' => 'print_color_link',
				),

			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'apc_testimonial' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}
		}

		/**
		 * Print the number input field
		 */
		public function print_number_input() {
			$tst_number = intval( get_option( 'apc_tt_shortcode_number', 6 ) );
			?>
			<input type="number" id="apc_tt_shortcode_number" name="apc_tt_shortcode_number"
				value="<?php echo '' !== $tst_number ? esc_html( $tst_number ) : 6; ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
			<input type="checkbox" class="ps-tst-option-panel__onoff__input" name="apc_tt_shortcode_show_image" value='yes' id="apc_tt_shortcode_show_image"
				<?php checked( get_option( 'apc_tt_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<label for="shortcode_show_image" class="ps-tst-option-panel__onoff__label ">
				<span class="ps-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}

		/**
		 * Print the hover effect field
		 */
		public function print_hover_effect() {
			$tst_hover_effect = get_option( 'apc_tt_shortcode_hover_effect', 'none' );
			?>
			<span class="hover_effect__container">
				<input type="radio" name="apc_tt_shortcode_hover_effect" value="none" id="apc_tt_shortcode_hover_effect-none" 
				<?php
				if ( 'none' === $tst_hover_effect ) {
					echo ' checked';}
				?>
				> None
				<input type="radio" name="apc_tt_shortcode_hover_effect" value="highlight" id="apc_tt_shortcode_hover_effect-highlight" 
				<?php
				if ( 'highlight' === $tst_hover_effect ) {
					echo ' checked';}
				?>
				> Highlight
				<input type="radio" name="apc_tt_shortcode_hover_effect" value="zoom" id="apc_tt_shortcode_hover_effect-zoom" 
				<?php
				if ( 'zoom' === $tst_hover_effect ) {
					echo ' checked';}
				?>
				> Zoom
				</span>
			<?php
		}

		/**
		 * Print the border radius field
		 */
		public function print_border_radius() {
			$tst_border_radius = intval( get_option( 'apc_tt_shortcode_border_radius', 7 ) );
			?>
			<input type="number" id="apc_tt_shortcode_border_radius" name="apc_tt_shortcode_border_radius"
				value="<?php echo '' !== $tst_border_radius ? esc_html( $tst_border_radius ) : 7; ?>">
			<?php
		}

		/**
		 * Print the hover effect field
		 */
		public function print_color_link() {
			$tst_color_link = get_option( 'apc_tt_shortcode_color_link', '#effeff' );
			?>

			<input type="text" class="ga-tst-form__input-badge_color__input" name="apc_tt_shortcode_color_link" id="apc_tt_shortcode_color_link" 
				value="<?php echo esc_html( $tst_color_link ); ?>">
			<?php
		}

	}
}
