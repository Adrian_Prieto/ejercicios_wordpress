<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */


if ( ! function_exists( 'apc_tt_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name Name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function apc_tt_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = APC_TT_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'apc_tt_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function apc_tt_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = APC_TT_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'testimonial_wp_enqueue_scripts' ) ) {
	/** CSS Register Function for FrontEnd */
	function testimonial_wp_enqueue_scripts() {
		$css_url = APC_TT_DIR_ASSETS_CSS_URL . '/apc_tt.css';
		wp_register_style( 'apc-testimonial-style', $css_url );
		wp_enqueue_style( 'apc-testimonial-style' );
	}
}
add_action( 'wp_enqueue_scripts', 'testimonial_wp_enqueue_scripts' );

if ( ! function_exists( 'testimonial_admin_enqueue_scripts' ) ) {
	/** CSS and JS Register Function for Admin */
	function testimonial_admin_enqueue_scripts() {
		$js_url  = APC_TT_DIR_ASSETS_JS_URL . '/apc_tt.js';
		$css_url = APC_TT_DIR_ASSETS_CSS_URL . '/apc_tt_admin.css';

		wp_register_style( 'apc-admin-testimonial-style', $css_url );
		wp_enqueue_style( 'apc-admin-testimonial-style' );

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'my-script-handle', $js_url, array( 'wp-color-picker' ), true, true );
	}
}
add_action( 'admin_enqueue_scripts', 'testimonial_admin_enqueue_scripts' );

if ( ! function_exists( 'apc_shortcode_testimonials' ) ) {
	/**
	 * Basic shortcode to show testimonials
	 *
	 * @param mixed $atts Attributes or parameters of the shortcode, look at array below for possible arguments.
	 * @param mixed $content Show additional content.
	 * @return void
	 */
	function apc_shortcode_testimonials( $atts, $content = null ) {
		/* Process arguments */
		$meta_options = get_option('yit_apc_testimonials_options');

		$atts = shortcode_atts(
			array(
				'number'       => intval( $meta_options['apc_tt_shortcode_number'] ),
				'ids'          => array(),
				'tax_ids'      => array(),
				//'show_image'   => get_option( 'apc_tt_shortcode_show_image', '' ), That's the previous call.
				'show_image'   => $meta_options['apc_tt_shortcode_show_image'],
				'hover_effect' => $meta_options['apc_tt_shortcode_hover_effect'],
			),
			$atts
		);

		/* Initialize empty variables to be used after */
		$query_args = array(
			'post_type' => 'apc_testimonial',
		);

		/** Using one method (last X posts, or an X taxonomy, or a array of selected posts) */
		if ( ! empty( $atts['ids'] ) ) {
			/* Create complex string into array of ints to be used in query */
			$id_array               = array_map( 'intval', explode( ',', $atts['ids'] ) );
			$query_args['post__in'] = $id_array;
		} elseif ( ! empty( $atts['tax_ids'] ) ) {
			$id_array                = array_map( 'intval', explode( ',', $atts['tax_ids'] ) );
			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'apc_tt_tax_hierarchical',
					'field'    => 'id',
					'terms'    => $id_array,
				),
				array(
					'taxonomy' => 'apc_tt_no_hierarchical_tax',
					'field'    => 'id',
					'terms'    => $id_array,
				),
				'relation' => 'OR',
			);
		} else {
			$query_args['posts_per_page'] = $atts['number'];
		}

		$the_query = get_transient( 'apc_tt_transient' );
		if ( false === ( $the_query ) ) {
			$the_query = new WP_Query( $query_args );
		}

		echo '<div class="row">';
		// Start our WP Query.
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			// First get all data from metapost.
			$photo       = get_post_meta( get_the_ID(), '_wp_attached_file', true );
			$role        = get_post_meta( get_the_ID(), '_apc_tt_role', true );
			$email       = get_post_meta( get_the_ID(), '_apc_tt_email', true );
			$company     = get_post_meta( get_the_ID(), '_apc_tt_company', true );
			$company_url = get_post_meta( get_the_ID(), '_apc_tt_company_url', true );
			$vip         = get_post_meta( get_the_ID(), '_apc_tt_vip', true );
			$badge       = get_post_meta( get_the_ID(), '_apc_tt_badge', true );
			$badge_text  = get_post_meta( get_the_ID(), '_apc_tt_badge_text', true );
			$badge_color = get_post_meta( get_the_ID(), '_apc_tt_badge_color', true );
			$rating      = get_post_meta( get_the_ID(), '_apc_tt_rating', true );

			// Admin options.
			$tst_border_radius = intval( $meta_options['apc_tt_shortcode_border_radius'] );
			$tst_color_link    = $meta_options['apc_tt_shortcode_color_link'];
			// Showing terms.
			$term_obj_list_h     = get_the_terms( $post->ID, 'apc_tt_tax_hierarchical' );
			$term_obj_list_non_h = get_the_terms( $post->ID, 'apc_tt_no_hierarchical_tax' );
			$terms_string        = join( ', ', wp_list_pluck( $term_obj_list_h, 'name' ) ) . ', ' . join( ', ', wp_list_pluck( $term_obj_list_non_h, 'name' ) );

			// Next, build the HTML.
			?> 
			<div class="column">
				<div class="card
				<?php
					// Hover effects here.
				if ( 'zoom' === $atts['hover_effect'] ) {
					echo esc_html( ' card_zoom ' );
				} elseif ( 'highlight' === $atts['hover_effect'] ) {
					echo esc_html( 'card_highlight ' );
				}
					// VIP effect here.
				if ( $vip ) {
					echo esc_html( 'testimonial_card_vip' );
				}
				?>
				" <?php echo 'style="border-radius:' . esc_html( $tst_border_radius ) . 'px"'; ?>>
					<div class='testimonial_badge_container'>
						<?php if ( $badge ) { ?>
							<div class='testimonial_badge' style="background-color:<?php echo esc_html( $badge_color ); ?>"> 
								<p>
								<?php echo esc_html( $badge_text ); ?>
								</p>
							</div>
						<?php } ?>
					</div>
					<div class="testimonial_upper_container">
						<?php
						if ( 'yes' === $atts['show_image'] ) {
							echo esc_html( the_post_thumbnail( 'thumbnail', array( 'class' => 'testimonial_image' ) ) );
						}
						?>
							<p class="testimonial_title"><?php echo esc_html( the_title() ); ?></p>
							<p class="testimonial_subtitle"><?php echo esc_html( $role ); ?> at <a <?php echo 'style="color:' . esc_html( $tst_color_link ) . '"'; ?> href="https://<?php echo esc_html( $company_url ); ?>"> <?php echo esc_html( $company ); ?> </a></p>
							<p class="testimonial_email" <?php echo 'style="color:' . esc_html( $tst_color_link ) . '"'; ?>><?php echo esc_html( $email ); ?></p>
							<div class="testimonial_stars">
								<span 
								<?php
								if ( $rating >= 1 ) {
									echo 'class="testimonial_single_star_checked"';}
								?>
									>☆</span>
								<span 
								<?php
								if ( $rating >= 2 ) {
									echo 'class="testimonial_single_star_checked"';}
								?>
									>☆</span>
								<span 
								<?php
								if ( $rating >= 3 ) {
									echo 'class="testimonial_single_star_checked"';}
								?>
									>☆</span>
								<span 
								<?php
								if ( $rating >= 4 ) {
									echo 'class="testimonial_single_star_checked"';}
								?>
									>☆</span>
								<span 
								<?php
								if ( $rating >= 5 ) {
									echo 'class="testimonial_single_star_checked"';}
								?>
									>☆</span>
							</div>
					</div>
					</p>
					<div>
						<p class="testimonial_taxonomy">
							<?php printf( __( '<b>Terms:</b> %s', 'textdomain' ), esc_html( $terms_string ) ); ?>
						</p>
						<p><?php echo esc_html( the_content() ); ?> </p>
					</div>
				</div>
			<?php
			// And repeat.
		endwhile;
		echo '</div>';
		wp_reset_postdata();
	}
}
add_shortcode( 'sc_testimonials', 'apc_shortcode_testimonials' );

if ( ! function_exists( 'apc_shortcode_tt_for_widget' ) ) {
	/**
	 * Shortcode of list testimonial content, such as title, description
	 *
	 * @param  array  $atts Array of arrguments.
	 * @param  string $content Optional text.
	 * @return void
	 */
	function apc_shortcode_tt_for_widget( $atts, $content = null ) {
		/* Process arguments */
		$atts = shortcode_atts(
			array(
				'number' => 3,
			),
			$atts
		);

		/* Execute query for posts */
		$query_args = array(
			'post_type'      => 'apc_testimonial',
			'posts_per_page' => $atts['number'],
		);

		$the_query = new WP_Query( $query_args );

		/* Link transient functions */
		add_action( 'save_post', 'apc_tt_update_transient' );   // Updating after saving changes (even creating posts).
		add_action( 'delete_post', 'apc_tt_update_transient' ); // Updating after removing post.
		?>
		<h2> Recent Testimonials </h2>
		<ul>
			<?php
			// Start our WP Query.
			while ( $the_query->have_posts() ) :
				$the_query->the_post();

				// Next, build the HTML.
				?>
				<li> 
					<a href="<?php echo esc_html( the_permalink() ); ?>"> <?php echo esc_html( the_title() ); ?> </a>
					<p>  <?php echo esc_html( the_excerpt() ); ?> </p>
				</li>
				<?php
				// And repeat.
			endwhile;
			wp_reset_postdata();
			?>
		</ul> 
		<?php
	}
}
add_shortcode( 'sc_widget_testimonials', 'apc_shortcode_tt_for_widget' );

if ( ! function_exists( 'apc_tt_update_transient' ) ) {
	/**
	 * Basic function to update
	 *
	 * @param object $post Inline WordPress argument.
	 * @return void
	 */
	function apc_tt_update_transient( $post ) {
		if ( false === get_transient( 'apc_tt_transient' ) ) {
			/* Execute query for posts */
			$query_args = array(
				'post_type'      => 'apc_testimonial',
				'posts_per_page' => 3,
			);

			$the_query = new WP_Query( $query_args );

			// Put the results in a transient. Expire after 12 hours.
			set_transient( 'apc_tt_transient', $the_query, 15 * MINUTE_IN_SECONDS );
		}
	}
}

if ( ! function_exists( 'apc_create_custom_user' ) ) {
	/** Create TTMLS_Manager custom role */
	function apc_create_custom_user() {

		add_role(
			'TTMLS_Manager',
			'TTMLS Manager',
			array(
				'read_private_apc_testimonials' => true,
				'edit_apc_testimonial'          => true,
				'edit_apc_testimonials'         => true,
				'edit_other_apc_testimonials'   => true,
				'publish_apc_testimonials'      => true,
				'read_apc_testimonial'          => true,
				'read_private_apc_testimonials' => true,
				'delete_apc_testimonial'        => true,
				'read'                          => true,
				'upload_files'                  => true,
			)
		);

		/** Give permissions to admin */
		$admin_role = get_role( 'administrator' );

		$admin_role->add_cap( 'read_private_apc_testimonials' );
		$admin_role->add_cap( 'edit_apc_testimonial' );
		$admin_role->add_cap( 'edit_apc_testimonials' );
		$admin_role->add_cap( 'edit_other_apc_testimonials' );
		$admin_role->add_cap( 'publish_apc_testimonials' );
		$admin_role->add_cap( 'read_apc_testimonial' );
		$admin_role->add_cap( 'read_private_apc_testimonials' );
		$admin_role->add_cap( 'delete_apc_testimonial' );
	}
}

if ( ! function_exists( 'apc_tt_change_capabilities' ) ) {
	/**
	 * Function to set name of posible capabilities of this CPT. Called at CPT class
	 *
	 * @param  array $args Arguments of permissions (set in the function).
	 * @param  mixed $post_type CPT of the permission. In this case it only applies with apc_testimonial CPT.
	 */
	function apc_tt_change_capabilities( $args, $post_type ) {

		// Do not filter any other post type.
		if ( 'apc_testimonial' !== $post_type ) {
			// Give other post_types their original arguments.
			return $args;
		}

		// Change the capabilities of the "course_document" post_type.
		$args['capabilities'] = array(
			'edit_post'          => 'edit_apc_testimonial',
			'edit_posts'         => 'edit_apc_testimonials',
			'edit_others_posts'  => 'edit_other_apc_testimonials',
			'publish_posts'      => 'publish_apc_testimonials',
			'read_post'          => 'read_apc_testimonial',
			'read_private_posts' => 'read_private_apc_testimonials',
			'delete_post'        => 'delete_apc_testimonial',
		);

		// Give the post type its arguments.
		return $args;
	}
}
add_action( 'plugins_loaded', 'apc_create_custom_user', 999 ); // Although admin_init, or even after_setup_theme can also be used.

?>
