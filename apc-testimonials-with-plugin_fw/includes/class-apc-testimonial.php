<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_TT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_Testimonial' ) ) {
	/** Main plugin class */
	class APC_Testimonial {
		/**
		 * Main instance.
		 *
		 * @var undefined
		 */
		private static $instance;

		/**
		 * Main Admin instance.
		 *
		 * @var undefined
		 */
		public $admin = null;

		/**
		 * Main Frontend instance.
		 *
		 * @var undefined
		 */
		public $frontend = null;

		/**
		 * Safe way to call class
		 *
		 * @return class
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 *  Class constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'apc_tt_require_class',
				array(
					'common'   => array(
						'includes/class-apc-tt-post-types.php',
						'includes/functions.php',
						'includes/class-apc-tt-widget.php',
					),
					'admin'    => array(
						'includes/class-apc-tt-admin.php',
						'includes/class-apc-tt-widget.php',
					),
					'frontend' => array(
						'includes/class-apc-tt-frontend.php',
					),
				)
			);

			$this->require( $require );

			$this->init_classes();

			// Finally call the init function.
			$this->init();

			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99);
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( APC_TT_INIT, APC_TT_SECRETKEY, APC_TT_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		*/
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( APC_TT_SLUG, APC_TT_INIT );
			}
		}

		/**
		 * _require Add the main classes' file
		 *
		 * @param  array $main_classes Set of files to be included in the plugin.
		 * @return void
		 */
		protected function require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( APC_TT_DIR_PATH . $class ) ) {
						require_once APC_TT_DIR_PATH . $class;
					}
				}
			}
		}

		/** A safe way to call the construct */
		public function init_classes() {
			APC_TT_Post_Types::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = APC_TT_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = APC_TT_Frontend::get_instance();
			}
		}

		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}
	}
}

if ( ! function_exists( 'apc_testimonial_call' ) ) {
	/** Run a single instance */
	function apc_testimonial_call() {
		return APC_Testimonial::get_instance();
	}
}
