<?php

$settings = array(
    'settings' => array(
        'section-1' => array(
            array(
                'name' => __( 'Testimonials Options', 'apc_testimonial' ),
                'type' => 'title'
            ),
            array(
                'id'   => 'apc_tt_shortcode_number',
                'name' => __( 'Número', 'apc_testimonial' ),
                'type' => 'number',
                'std'  => 6,
                'step' => 1,
                'min'  => 0,
                'max'  => 10,
            ),
            array(
                'id'   => 'apc_tt_shortcode_show_image',
                'name' => __( 'Show image', 'apc_testimonial' ),
                'type' => 'checkbox',
                'std'  => 'yes',
            ),
            array(
                'id'      => 'apc_tt_shortcode_hover_effect',
                'name'    => __( 'Hover effect', 'apc_testimonial' ),
                'type'    => 'radio',
                'std'     => 'none',
                'options' => array(
                    'none'      => 'None',
                    'highlight' => 'Highlight',
                    'zoom'      => 'Zoom',
                ),
            ),

            array(
                'id'   => 'apc_tt_shortcode_border_radius',
                'name' => __( 'Border radius', 'apc_testimonial' ),
                'type' => 'number',
                'std'  => 7,
                'step' => 1,
                'min'  => 0,
                'max'  => 30,
            ),
            array(
                'id'            => 'apc_tt_shortcode_color_link',
                'name'          => __( 'Link Color', 'apc_testimonial' ),
                'type'          => 'colorpicker',
                'std'           => '#effeff',
                'desc'          => __( 'Color of which links will be displayed.', 'apc_testimonial' ),
                'alpha_enabled' => false,
            ),
            array(
                'type' => 'close',
            )
        ),
    )
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
