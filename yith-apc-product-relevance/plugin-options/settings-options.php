<?php

$settings = array(
    'settings' => array(
        'general-options'               => array(
            'title' => __( 'General Options', 'yith-apc-productrelevance' ),
            'type'  => 'title',
            'desc'  => '',
        ),
        'yith-apc-pr-default-relevance' => array(
            'id'        => 'yith-apc-pr-default-relevance',
            'name'      => __( 'Default Relevance', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'number',
            'default'   => '5',
        ),

        'yith-apc-pr-min-relevance'     => array(
            'id'        => 'yith-apc-pr-min-relevance',
            'name'      => __( 'Minimum Relevance', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'number',
            'default'   => '0',
        ),

        'yith-apc-pr-max-relevance'     => array(
            'id'        => 'yith-apc-pr-max-relevance',
            'name'      => __( 'Maximum Relevance', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'number',
            'default'   => '10',
        ),

        'yith-apc-pr-hide-lower'        => array(
            'id'        => 'yith-apc-pr-hide-lower',
            'name'      => __( 'Hide products with relevance lower than', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'number',
            'default'   => '0',
        ),

        'yith-apc-pr-show'              => array(
            'id'        => 'yith-apc-pr-show',
            'name'      => __( 'Show relevance in single product page', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'onoff',
            'default'   => 'no',
        ),

        'yith-apc-pr-relevance-label'   => array(
            'id'        => 'yith-apc-pr-relevance-label',
            'name'      => __( 'Relevance Label', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'text',
            'default'   => '',
        ),

        'yith-apc-pr-position'          => array(
            'id'        => 'yith-apc-pr-position',
            'name'      => __( 'Position', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'select',
            'default'   => 'before_cart',
            'options'   => array (
                'before_title' => esc_html__( 'Before title', 'yith-apc-productrelevance' ),
                'after_title'  => esc_html__( 'After title', 'yith-apc-productrelevance' ),
                'before_price' => esc_html__( 'Before price', 'yith-apc-productrelevance' ),
                'after_price'  => esc_html__( 'After price', 'yith-apc-productrelevance' ),
                'before_cart'  => esc_html__( 'Before add to cart', 'yith-apc-productrelevance' ),
                'after_cart'   => esc_html__( 'After add to cart', 'yith-apc-productrelevance' ),
            ),
        ),

        'yith-apc-pr-show-as'           => array(
            'id'        => 'yith-apc-pr-show-as',
            'name'      => __( 'Show as', 'yith-apc-productrelevance' ),
            'type'      => 'yith-field',
            'yith-type' => 'radio',
            'default'   => 'text',
            'options'   => array(
                'text'         => esc_html__( 'Text', 'yith-apc-productrelevance' ),
                'progress-bar' => esc_html__( 'Progress bar', 'yith-apc-productrelevance' ),
                'stars'        => esc_html__( 'Stars', 'yith-apc-productrelevance' ),
            ),
        ),

        'general-options-end'           => array(
            'type' => 'sectionend',
            'id'   => 'yith-wcbk-general-options',
        ),
    ),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
