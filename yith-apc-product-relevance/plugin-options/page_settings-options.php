<?php

$settings = array(
	'page_settings' => array(
		'general-options'                  => array(
			'title' => __( 'General Options', 'yith-apc-productrelevance' ),
			'type'  => 'title',
			'desc'  => '',
		),

		'yith-apc-pr-page-default'         => array(
			'id'        => 'yith-apc-pr-page-default',
			'name'      => __( 'Default Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '5',
		),

		'yith-apc-pr-page-min'             => array(
			'id'        => 'yith-apc-pr-page-min',
			'name'      => __( 'Minimum Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '0',
		),

		'yith-apc-pr-page-max'             => array(
			'id'        => 'yith-apc-pr-page-max',
			'name'      => __( 'Maximum Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '10',
		),

		'yith-apc-pr-hide-pages-lower'     => array(
			'id'        => 'yith-apc-pr-hide-pages-lower',
			'name'      => __( 'Hide pages with relevance lower than', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '0',
		),
		'yith-apc-pr-page-show'            => array(
			'id'        => 'yith-apc-pr-page-show',
			'name'      => __( 'Show relevance in pages', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
		),

		'yith-apc-pr-page-relevance-label' => array(
			'id'        => 'yith-apc-pr-page-relevance-label',
			'name'      => __( 'Relevance Label', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'text',
			'default'   => '',
			'deps'      => array(
				'id'     => 'yith-apc-pr-page-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),

		'yith-apc-pr-page-position'        => array(
			'id'        => 'yith-apc-pr-page-position',
			'name'      => __( 'Position', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'default'   => 'before_content',
			'options'   => array(
				'before_content' => esc_html__( 'Before content', 'yith-apc-productrelevance' ),
				'after_content'  => esc_html__( 'After content', 'yith-apc-productrelevance' ),
			),
			'deps'      => array(
				'id'     => 'yith-apc-pr-page-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),

		'yith-apc-pr-page-show-as'         => array(
			'id'        => 'yith-apc-pr-page-show-as',
			'name'      => __( 'Show as', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'text',
			'options'   => array(
				'text'         => esc_html__( 'Text', 'yith-apc-productrelevance' ),
				'progress-bar' => esc_html__( 'Progress bar', 'yith-apc-productrelevance' ),
				'stars'        => esc_html__( 'Stars', 'yith-apc-productrelevance' ),
			),
			'deps'      => array(
				'id'     => 'yith-apc-pr-page-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),

		'general-options-end'              => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
