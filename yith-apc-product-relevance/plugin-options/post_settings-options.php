<?php

$settings = array(
	'post_settings' => array(
		'general-options'                  => array(
			'title' => __( 'General Options', 'yith-apc-productrelevance' ),
			'type'  => 'title',
			'desc'  => '',
		),

		'yith-apc-pr-post-default'         => array(
			'id'        => 'yith-apc-pr-post-default',
			'name'      => __( 'Default Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '5',
		),

		'yith-apc-pr-post-min'             => array(
			'id'        => 'yith-apc-pr-post-min',
			'name'      => __( 'Minimum Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '0',
		),

		'yith-apc-pr-post-max'             => array(
			'id'        => 'yith-apc-pr-post-max',
			'name'      => __( 'Maximum Relevance', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '10',
		),

		'yith-apc-pr-hide-posts-lower'     => array(
			'id'        => 'yith-apc-pr-hide-posts-lower',
			'name'      => __( 'Hide posts with relevance lower than', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => '0',
		),
		'yith-apc-pr-post-show'          => array(
			'id'        => 'yith-apc-pr-post-show',
			'name'      => __( 'Show relevance in single posts', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
		),
		'yith-apc-pr-post-relevance-label' => array(
			'id'        => 'yith-apc-pr-post-relevance-label',
			'name'      => __( 'Relevance Label', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'text',
			'default'   => '',
			'deps'      => array(
				'id'     => 'yith-apc-pr-post-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),
		'yith-apc-pr-post-position'        => array(
			'id'        => 'yith-apc-pr-post-position',
			'name'      => __( 'Position', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'default'   => 'before_content',
			'options'   => array(
				'before_content' => esc_html__( 'Before content', 'yith-apc-productrelevance' ),
				'after_content'  => esc_html__( 'After content', 'yith-apc-productrelevance' ),
			),
			'deps'      => array(
				'id'     => 'yith-apc-pr-post-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),

		'yith-apc-pr-post-show-as'         => array(
			'id'        => 'yith-apc-pr-post-show-as',
			'name'      => __( 'Show as', 'yith-apc-productrelevance' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'text',
			'options'   => array(
				'text'         => esc_html__( 'Text', 'yith-apc-productrelevance' ),
				'progress-bar' => esc_html__( 'Progress bar', 'yith-apc-productrelevance' ),
				'stars'        => esc_html__( 'Stars', 'yith-apc-productrelevance' ),
			),
			'deps'      => array(
				'id'     => 'yith-apc-pr-post-show',
				'value'  => 'yes',
				'action' => 'hide',
			),
		),
		'general-options-end'              => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
