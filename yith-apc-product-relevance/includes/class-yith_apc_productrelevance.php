<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'Yith_Apc_ProductRelevance' ) ) {
	/** Main plugin class */
	class Yith_Apc_ProductRelevance {
		/**
		 * Main instance.
		 *
		 * @var undefined
		 */
		private static $instance;

		/**
		 * Main Admin instance.
		 *
		 * @var undefined
		 */
		public $admin = null;

		/**
		 * Main Frontend instance.
		 *
		 * @var undefined
		 */
		public $frontend = null;

		/**
		 * Safe way to call class
		 *
		 * @return class
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 *  Class constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_apc_pr_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith_apc_admin.php',
					),
					'frontend' => array(
						'includes/class-yith_apc_frontend.php',
					),
				)
			);

			$this->require( $require );

			// Finally call the init function.
			$this->init();

			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
		}

		/**
		 * Function used by Free Plugins.
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_APC_PR_INIT, YITH_APC_PR_SECRETKEY, YITH_APC_PR_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_APC_PR_SLUG, YITH_APC_PR_INIT );
			}
		}

		/**
		 * _require Add the main classes' file
		 *
		 * @param  array $main_classes Set of files to be included in the plugin.
		 * @return void
		 */
		protected function require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_APC_PR_DIR_PATH . $class ) ) {
						require_once YITH_APC_PR_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_APC_PR_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_APC_PR_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'yith_apc_productrelevance_call' ) ) {
	/** Run a single instance */
	function yith_apc_productrelevance_call() {
		return Yith_Apc_ProductRelevance::get_instance();
	}
}
