<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_APC_PR_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class YITH_APC_PR_Admin {
		/**
		 * Main Instance
		 *
		 * @var yith_apc_pmhp_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Needed for YIT_Plugin_Panel_WooCommerce.
		 *
		 * @var $_panel the panel  used by YITH Plugin Panel (WooCommerce).
		 */
		private $_panel;

		/**
		 * @var Panel page
		 */
		protected $panel_page = 'yith_apc_pr';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_APC_PR_Admin constructor.
		 */
		private function __construct() {
			/* Plugin fw filters */
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_APC_PR_DIR_PATH . '/' . basename( YITH_APC_PR_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );	

			/* Main Hook for YITH Plugin Panel WooCommerce */
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'yit_panel_wc_before_update', array( $this, 'check_before_update_relevance_options' ) );

			/* Create the custom tab */
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'create_custom_tab_for_product' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_custom_tab_for_product' ) );

			/* Save the custom fields */
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_fields' ), 10, 2 );

			/*Plugin FW Metabox*/
			add_action( 'admin_init', array( $this, 'add_metabox_for_post' ), 10 );
			add_action( 'admin_init', array( $this, 'add_metabox_for_page' ), 10 );
		}

		/** Main functionf of YITH Plugin Panel Woocommerce */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'pd_settings'   => __( 'Product Settings', 'yith-apc-productrelevance' ),
				'page_settings' => __( 'Page Settings', 'yith-apc-productrelevance' ),
				'post_settings' => __( 'Post Settings', 'yith-apc-productrelevance' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH APC Product Relevance',
				'menu_title'         => 'YITH APC Product Relevance',
				'plugin_description' => __( 'Manage the plugin here', 'yith-apc-productrelevance' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith_plugin_panel',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith-apc-productrelevance_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_APC_PR_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_APC_PR_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_APC_PR_SLUG;
				$row_meta_args['is_premium'] = true;
			}
			return $row_meta_args;
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_APC_PR_SLUG );
		}

		/**
		 * Gives a basic description of the new pn_custom_tab
		 *
		 * @param  mixed $tabs Inline argument of Woocommerce hooks.
		 */
		public function create_custom_tab_for_product( $tabs ) {
			// Adds the new tab.
			$tabs['yith-apc-productrelevance'] = array(
				'label'    => esc_html__( 'Product Relevance', 'yith-apc-productrelevance' ),
				'target'   => 'yith-apc-productrelevance', // Will be used to create an anchor link so needs to be unique.
				'class'    => array( 'yith-apc-productrelevance', 'show_if_simple', 'show_if_variable' ), // Class for your panel tab - helps hide/show depending on product type.
				'priority' => 15,
			);
			return $tabs;
		}

		/**
		 * Display the content of Relevance Tab.
		 */
		public function display_custom_tab_for_product() {
				global $post;
				$product           = wc_get_product( $post->ID );
				$product_relevance = ! empty( $product->get_meta( '_yith_apc_single_relevance' ) ) ? $product->get_meta( '_yith_apc_single_relevance' ) : get_option( 'yith-apc-pr-default-relevance', 5 );
			?>
				<div id='yith-apc-productrelevance' class='panel woocommerce_options_panel'>
					<div class="options_group">
						<div class="form-field relevance_field">
							<label for='yith-apc-pr-default-relevance'> <?php echo( esc_html__( 'Product Relevance', 'yith-apc-productrelevance' ) ); ?> </label>
							<?php
								yith_apc_pr_create_form_field(
									array(
										'id'     => 'yith_apc_single_relevance_form',
										'class'  => 'form-field wc_auction_field yith-plugin-ui',
										'title'  => esc_html__( 'Product Relevance', 'yith-apc-productrelevance' ),
										'fields' => array(
											'class' => 'ywcact-product-metabox-onoff',
											'type'  => 'number',
											'id'    => 'yith_apc_single_relevance',
											'name'  => 'yith_apc_single_relevance',
											'value' => $product_relevance,
											'min'   => get_option( 'yith-apc-pr-min-relevance', 0 ),
											'max'   => get_option( 'yith-apc-pr-max-relevance', 10 ),
										),
									)
								);
							?>
						</p>
					</div>
				</div>
			<?php
		}

		/**
		 * Function to save the relevance tab.
		 *
		 * @param  mixed $post_id Needed to associate ID with metadata.
		 */
		public function save_fields( $post_id ) {
			$product = wc_get_product( $post_id );

			if ( isset( $_POST['yith_apc_single_relevance'] ) ) {
				$note_label = sanitize_text_field( wp_unslash( $_POST['yith_apc_single_relevance'] ) );
				$product->update_meta_data( '_yith_apc_single_relevance', sanitize_text_field( $note_label ) );
			}
			$product->save();
		}

		/** Main function of Plugin FW metabox */
		public function add_metabox_for_post() {
				$args = array(
					'label'    => __( 'Relevance of post', 'yith-apc-productrelevance' ),
					'pages'    => 'post',
					'context'  => 'normal',
					'priority' => 'default',
					'tabs'     => array(
						'settings' => array(
							'label'  => __( 'Settings', 'yith-apc-productrelevance' ),
							'fields' => array(
								'yith_apc_single_relevance' => array(
									'label'   => __( 'Post Relevance', 'yith-apc-productrelevance' ),
									'type'    => 'number',
									'private' => true,
									'min'     => get_option( 'yith-apc-pr-post-min', 0 ),
									'max'     => get_option( 'yith-apc-pr-post-max', 10 ),
									'std'     => get_option( 'yith-apc-pr-post-default', 5 ),
								),
							),
						),
					),
				);
				$metabox1 = YIT_Metabox( 'yith_apc_pr_post_meta' );
				$metabox1->init( $args );
		}
		/** Main function of Plugin FW metabox */
		public function add_metabox_for_page() {
				$args = array(
					'label'    => __( 'Relevance of page', 'yith-apc-productrelevance' ),
					'pages'    => 'page',
					'context'  => 'normal',
					'priority' => 'default',
					'tabs'     => array(
						'settings' => array(
							'label'  => __( 'Settings', 'yith-apc-productrelevance' ),
							'fields' => array(
								'yith_apc_single_relevance' => array(
									'label'   => __( 'Page Relevance', 'yith-apc-productrelevance' ),
									'type'    => 'number',
									'private' => true,
									'min'     => get_option( 'yith-apc-pr-page-min', 0 ),
									'max'     => get_option( 'yith-apc-pr-page-max', 10 ),
									'std'     => get_option( 'yith-apc-pr-page-default', 5 ),
								),
							),
						),
					),
				);
				$metabox2 = YIT_Metabox( 'yith_apc_pr_page_meta' );
				$metabox2->init( $args );
		}

		/** Change Posts that will be under min or above max */
		public function check_before_update_relevance_options() {

			// Change products, if we are in product tab.
			if ( isset( $_POST['yith-apc-pr-default-relevance'] ) && isset( $_POST['yith-apc-pr-min-relevance'] ) && isset( $_POST['yith-apc-pr-max-relevance'] ) ) {
				$default_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-default-relevance'] ) );
				$minimum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-min-relevance'] ) );
				$maximum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-max-relevance'] ) );

				$product_ids = get_posts(
					array(
						'post_type'   => 'product',
						'numberposts' => -1,
						'post_status' => 'publish',
						'fields'      => 'ids',
					)
				);
				foreach ( $product_ids as $product_id ) {

					$current_relevance = get_post_meta( $product_id, '_yith_apc_single_relevance', true );

					if ( $current_relevance < $minimum_relevance ) {
						update_post_meta( $product_id, '_yith_apc_single_relevance', $minimum_relevance );
					} elseif ( $current_relevance > $maximum_relevance ) {
						update_post_meta( $product_id, '_yith_apc_single_relevance', $maximum_relevance );
					}
				}
			}

			// Change pages, if we are in page tab.
			if ( isset( $_POST['yith-apc-pr-page-default'] ) && isset( $_POST['yith-apc-pr-page-min'] ) && isset( $_POST['yith-apc-pr-page-max'] ) ) {
				$default_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-page-default'] ) );
				$minimum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-page-min'] ) );
				$maximum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-page-max'] ) );

				$page_ids = get_posts(
					array(
						'post_type'   => 'page',
						'numberposts' => -1,
						'post_status' => 'publish',
						'fields'      => 'ids',
					)
				);
				foreach ( $page_ids as $page_id ) {

					$current_relevance = get_post_meta( $page_id, '_yith_apc_single_relevance', true );

					if ( $current_relevance < $minimum_relevance ) {
						update_post_meta( $page_id, '_yith_apc_single_relevance', $minimum_relevance );
					} elseif ( $current_relevance > $maximum_relevance ) {
						update_post_meta( $page_id, '_yith_apc_single_relevance', $maximum_relevance );
					}
				}
			}

			// Change posts, if we are in post tab.
			if ( isset( $_POST['yith-apc-pr-post-default'] ) && isset( $_POST['yith-apc-pr-post-min'] ) && isset( $_POST['yith-apc-pr-post-max'] ) ) {
				$default_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-post-default'] ) );
				$minimum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-post-min'] ) );
				$maximum_relevance = sanitize_text_field( wp_unslash( $_POST['yith-apc-pr-post-max'] ) );

				$post_ids = get_posts(
					array(
						'post_type'   => 'post',
						'numberposts' => -1,
						'post_status' => 'publish',
						'fields'      => 'ids',
					)
				);
				foreach ( $post_ids as $post_id ) {

					$current_relevance = get_post_meta( $post_id, '_yith_apc_single_relevance', true );

					if ( $current_relevance < $minimum_relevance ) {
						update_post_meta( $post_id, '_yith_apc_single_relevance', $minimum_relevance );
					} elseif ( $current_relevance > $maximum_relevance ) {
						update_post_meta( $post_id, '_yith_apc_single_relevance', $maximum_relevance );
					}
				}
			}
		}
	}
}
