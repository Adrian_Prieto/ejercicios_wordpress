<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! function_exists( 'yith_apc_relevance_include_js_admin' ) ) {
	/**
	 * CSS and JS Register Function for Admin
	 *
	 * @param  mixed $hook Hook to filter the enqueue.
	 */
	function yith_apc_relevance_include_js_admin( $hook ) {

		global $post;

		if ( 'yith-plugins_page_yith-apc-productrelevance_panel' === $hook ) {
			$js_url = YITH_APC_PR_DIR_ASSETS_JS_URL . '/yith_apc_pn_admin.js';
			wp_enqueue_script( 'my-script-handle', $js_url, array( 'wp-color-picker' ), true, true );
		}	
	}
}
add_action( 'admin_enqueue_scripts', 'yith_apc_relevance_include_js_admin', 10, 1 );

if ( ! function_exists( 'yith_apc_pr_create_form_field' ) ) {
	/**
	 * Prints a form field for product metabox.
	 */
	function yith_apc_pr_create_form_field( $field ) {

		$defaults = array(
			'id'        => '',
			'class'     => '',
			'title'     => '',
			'label_for' => '',
			'desc'      => '',
			'data'      => array(),
			'fields'    => array(),
		);
		$field    = apply_filters( 'yith_wcact_product_metabox_form_field_args', wp_parse_args( $field, $defaults ), $field );
		/**
		 * @var string $class
		 * @var string $title
		 * @var string $label_for
		 * @var string $desc
		 * @var array  $data
		 * @var array  $fields
		 */
		extract( $field );

		if ( ! $label_for && $fields ) {
			$first_field = current( $fields );
			if ( isset( $first_field['id'] ) ) {
				$label_for = $first_field['id'];
			}
		}

		$data_html = '';
		foreach ( $data as $key => $value ) {
			$data_html .= "data-{$key}='{$value}' ";
		}

		$html = '';
		$html .= "<div id='{$id}' class='yith-wcact-form-field {$class}' {$data_html}>";
		$html .= "<label class='yith-wcact-form-field__label' for='{$label_for}'>{$title}</label>";

		$html .= "<div class='yith-wcact-form-field__container'>";
		ob_start();
		yith_plugin_fw_get_field( $fields, true ); //Print field using plugin-fw
		$html .= ob_get_clean();
		$html .= '</div><!-- yith-wcact-form-field__container -->';

		if ( $desc ) {
			$html .= "<div class='yith-wcact-form-field__description'>{$desc}</div>";
		}

		$html .= '</div><!-- yith-wcact-form-field -->';

		echo apply_filters( 'yith_wcact_product_metabox_form_field_html', $html, $field );
	}
}
