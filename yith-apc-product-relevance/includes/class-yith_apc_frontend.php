<?php

/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_APC_PR_Frontend' ) ) {
	/**
	 * Class with functions used in frontend
	 */
	class YITH_APC_PR_Frontend {

		/**
		 * Main Instance
		 *
		 * @var object Instance of the class to be called in get_instance
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 *  Relevance of the product (used in single_product functions).
		 *
		 * @var mixed Relevance of the current product.
		 */
		private $relevance_of_product;

		/**
		 * Main plugin Instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Frotend constructor.
		 */
		private function __construct() {
			add_action( 'woocommerce_before_single_product', array( $this, 'show_relevance_on_single_product' ), 10, 1 );

			// This must be called via AJAX?
			add_action( 'woocommerce_product_query', array( $this, 'query_order_relevance_products' ) );
			add_action( 'pre_get_posts', array( $this, 'query_filter_relevance_products' ) );

			// Add new sorting tab.
			add_filter( 'woocommerce_catalog_orderby', array( $this, 'add_relevance_orderby_tab' ) );

			// Add new sorting tab.
			add_filter( 'the_content', array( $this, 'show_relevance_on_single_post' ) );

			add_filter( 'get_pages', array( $this, 'filter_pages_on_menu' ), 10, 1 );

		}

		/**
		 * Add tabs into default Woocommerce sort element.
		 *
		 * @param  mixed $sortby Panel of the default tab. We add more tabs here.
		 */
		public function add_relevance_orderby_tab( $sortby ) {
			$sortby['relevance_asc']  = esc_html__( 'Sort by relevance: low to high', 'yith-apc-productrelevance' );
			$sortby['relevance_desc'] = esc_html__( 'Sort by relevance: high to low', 'yith-apc-productrelevance' );

			return $sortby;
		}

		/**
		 * Check if enable and show it where it's displayed
		 */
		public function show_relevance_on_single_product() {
			if ( is_product() ) {

				global $product;
				$this->relevance_of_product = ! empty( $product->get_meta( '_yith_apc_single_relevance' ) ) ? $product->get_meta( '_yith_apc_single_relevance' ) : get_option( 'yith-apc-pr-default-relevance', 5 );

				if ( 'yes' === get_option( 'yith-apc-pr-show', 'no' ) ) {
					switch ( get_option( 'yith-apc-pr-position', 'before_cart' ) ) {
						// Title has a priority of 5.. (See it in content-single-product.php ).
						case 'before_title':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 4 );
							break;
						case 'after_title':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 6 );
							break;

						// Price has a priority of 10.
						case 'before_price':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 8 );
							break;
						case 'after_price':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 11 );
							break;

						// Add to cart is priority 30.
						case 'before_cart':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 29 );
							break;
						case 'after_cart':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 31 );
							break;
						default:
							add_filter( 'woocommerce_single_product_summary', array( $this, 'show_relevance_html' ), 21 );
					}
				}
			}
		}

		/**
		 * Prints the message shown in single product page
		 */
		public function show_relevance_html() {
			$relevance_label = ! empty( get_option( 'yith-apc-pr-relevance-label', 'Relevance' ) ) ? get_option( 'yith-apc-pr-relevance-label', 'Relevance' ) : 'Relevance';
			echo '<p><strong>' . esc_html( $relevance_label ) . ':</strong> ' . esc_html( $this->relevance_of_product ) . '</p>';

			if ( isset( $_SERVER['REQUEST_URI'] ) ) {
				echo '<p><a href="' . esc_html( get_site_url() ) . '/?orderby=same_relevance&paged=1&post_type=product&relevance=' . esc_html( $this->relevance_of_product ) . '">' . esc_html__( 'Show Products with this relevance', 'yith-apc-productrelevance' ) . '</a></p>';
			}

			//wp_nonce_field( 'yith_apc_pr_single_product', 'yith_apc_pr_single_relevance_nonce' );
		}

		/**
		 * Order the products by relevance.
		 *
		 * @param array $q Database used in query.
		 */
		public function query_order_relevance_products( $q ) {
			// Search in /includes/class-wc-query.php.
			if ( is_shop() ) {

				$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( sanitize_text_field( wp_unslash( $_GET['orderby'] ) ) ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

				$q->query_vars['orderby']  = 'meta_value_num';
				$q->query_vars['meta_key'] = '_yith_apc_single_relevance';

				switch ( $orderby_value ) {
					case 'relevance_asc':
						$q->query_vars['order'] = 'ASC';
						break;
					case 'relevance_desc':
						$q->query_vars['order'] = 'DESC';
						break;
				}
			}
		}

		/**
		 * Here, we filter products with no relevance and same relevance (when we redirect).
		 *
		 * @param  mixed $query Database used in query.
		 */
		public function query_filter_relevance_products( $query ) {

			/*if ( ! isset( $_POST['yith_apc_pr_single_relevance_nonce'] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['yith_apc_pr_single_relevance_nonce'] ), 'yith_apc_pr_single_product' ) ) ) {
				print( 'Sorry, your nonce did not verify.' );
				exit();
			}*/
			if ( is_shop() ) { //is_shop includes also checkout.
				$same_relevance_filter = isset( $_GET['relevance'] );
				$same_relevance        = $same_relevance_filter ? sanitize_text_field( wp_unslash( $_GET['relevance'] ) ) : 0;
				$hide_lower_arg        = get_option( 'yith-apc-pr-hide-lower', 0 );

				// Gets the global query var object.
				global $wp_query;

				if ( ! $query->is_main_query() ) {
					return;
				}

				if ( $same_relevance_filter ) {
					$meta_query = array(
						array(
							'key'     => '_yith_apc_single_relevance',
							'value'   => $same_relevance,
							'compare' => '=',
						),
					);
				} else {
					$meta_query = array(
						array(
							'key'     => '_yith_apc_single_relevance',
							'value'   => $hide_lower_arg,
							'compare' => '>=',
						),
					);
				}
				$query->set( 'meta_query', $meta_query );

			} elseif ( is_front_page() ) { // Product or pages.

				$hide_lower_arg = get_option( 'yith-apc-pr-hide-posts-lower', 0 );
				$same_relevance_filter = isset( $_GET['relevance'] );
				$same_relevance        = $same_relevance_filter ? sanitize_text_field( wp_unslash( $_GET['relevance'] ) ) : 0;

				global $wp_query;

				if ( $same_relevance_filter ) {
					$meta_query = array(
						array(
							'key'     => '_yith_apc_single_relevance',
							'value'   => $same_relevance,
							'compare' => '=',
						),
					);
				} else {
					$meta_query = array(
						array(
							'key'     => '_yith_apc_single_relevance',
							'value'   => $hide_lower_arg,
							'compare' => '>=',
						),
					);
				}

				$query->set( 'meta_query', $meta_query );
			}
		}

		/**
		 * Check if enable and show it where it's displayed
		 *
		 * @param  mixed $content The content of the post. Depending of the loop, we write it before or after it.
		 */
		public function show_relevance_on_single_post( $content ) {

			if ( is_singular( 'post' ) ) {
				$this->relevance_of_product = ! empty( get_post_meta( get_the_ID(), '_yith_apc_single_relevance', true ) ) ? get_post_meta( get_the_ID(), '_yith_apc_single_relevance', true ) : get_option( 'yith-apc-pr-default-relevance', 5 );

				if ( 'yes' === get_option( 'yith-apc-pr-post-show', 'no' ) ) {

					$relevance_label = ! empty( get_option( 'yith-apc-pr-post-relevance-label', 'Relevance' ) ) ? get_option( 'yith-apc-pr-post-relevance-label', 'Relevance' ) : 'Relevance';

					switch ( get_option( 'yith-apc-pr-post-position', 'before_content' ) ) {
						case 'before_content':
							$custom_content  = '<p><strong>' . esc_html( $relevance_label ) . ':</strong> ' . esc_html( $this->relevance_of_product ) . '</p>';
							$custom_content .= '<p><a href="' . esc_html( get_site_url() ) . '/?orderby=same_relevance&paged=1&post_type=post&relevance=' . esc_html( $this->relevance_of_product ) . '">' . esc_html__( 'Show Products with this relevance', 'yith-apc-productrelevance' ) . '</a></p>';
							$custom_content .= $content;
							break;
						case 'after_content':
							$custom_content  = $content;
							$custom_content .= '<p><strong>' . esc_html( $relevance_label ) . ':</strong> ' . esc_html( $this->relevance_of_product ) . '</p>';
							$custom_content .= '<p><a href="' . esc_html( get_site_url() ) . '/?orderby=same_relevance&paged=1&post_type=post&relevance=' . esc_html( $this->relevance_of_product ) . '">' . esc_html__( 'Show Products with this relevance', 'yith-apc-productrelevance' ) . '</a></p>';
							break;
						default:
							$custom_content = $content;
					}

					return $custom_content;
				}
			} elseif ( is_singular( 'page' ) ) {

				if ( 'yes' === get_option( 'yith-apc-pr-page-show', 'no' ) ) {
					$relevance_label = ! empty( get_option( 'yith-apc-pr-page-relevance-label', 'Relevance' ) ) ? get_option( 'yith-apc-pr-page-relevance-label', 'Relevance' ) : 'Relevance';
					$this->relevance_of_product = ! empty( get_post_meta( get_the_ID(), '_yith_apc_single_relevance', true ) ) ? get_post_meta( get_the_ID(), '_yith_apc_single_relevance', true ) : get_option( 'yith-apc-pr-default-relevance', 5 );

					switch ( get_option( 'yith-apc-pr-page-position', 'before_content' ) ) {
						case 'before_content':
							$custom_content  = '<p><strong>' . esc_html( $relevance_label ) . ':</strong> ' . esc_html( $this->relevance_of_product ) . '</p>';
							$custom_content .= $content;
							break;
						case 'after_content':
							$custom_content  = $content;
							$custom_content .= '<p><strong>' . esc_html( $relevance_label ) . ':</strong> ' . esc_html( $this->relevance_of_product ) . '</p>';
							break;
						default:
							$custom_content = $content;
					}

					return $custom_content;
				}
			}

			return $content;
		}

		/**
		 * Filter the pages tab on the menu
		 *
		 * @param array $items List of objects. We filter this with a loop.
		 */
		public function filter_pages_on_menu( $items ) {

			$hide_lower_arg = get_option( 'yith-apc-pr-hide-pages-lower', 0 );

			foreach ( $items as $page_index => $page_item ) {

				$page_relevance = ! empty( get_post_meta( $page_item->ID, '_yith_apc_single_relevance', true ) ) ? get_post_meta( $page_item->ID, '_yith_apc_single_relevance', true ) : get_option( 'yith-apc-pr-default-relevance', 5 );

				if ( $page_relevance < $hide_lower_arg ) {
					unset( $items[ $page_index ] );
				}
			}

			return $items;
		}
	}
}
