��    (      \  5   �      p     q     �     �     �     �     �     �     �     �     �  $     $   ,  '   Q     y     �     �     �     �     �     �     �     �     	          '     7     I     [  !   d     �     �     �  %   �     �     
     )  ,   /     \     a  �  |     %     D     [     o     �     �     �     �     �     �  )    	  (   *	  )   S	     }	     �	     �	     �	     �	  	   �	     �	     
     
     3
     H
     Z
     v
     �
     �
  %   �
     �
     �
  &     $   *     O     o  	   �  7   �     �     �        '            #                           
                               !       (              	                            %             $                      &          "    After add to cart After content After price After title Before add to cart Before content Before price Before title Default Relevance General Options Hide pages with relevance lower than Hide posts with relevance lower than Hide products with relevance lower than Manage the plugin here Maximum Relevance Minimum Relevance Page Relevance Page Settings Position Post Relevance Post Settings Product Relevance Product Settings Progress bar Relevance Label Relevance of page Relevance of post Settings Show Products with this relevance Show as Show relevance in pages Show relevance in single posts Show relevance in single product page Sort by relevance: high to low Sort by relevance: low to high Stars Test plugin that order products by relevance Text YITH APC Product Relevance Project-Id-Version: YITH APC Product Relevance 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-apc-product-relevance
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-03-09 08:31+0000
X-Generator: Poedit 2.4.2
X-Domain: yith-apc-productrelevance
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Después de añadir al carrito Después del contenido Después del precio Después del título Antes de añadir al carito Antes del contenido Antes del precio Antes del título Relevancia por defecto Opciones generales Esconder páginas con relevancia menor a  Esconder entradas con relevancia menor a Esconder productos con relevancia menor a Ajusta el plugin aquí Relevancia máxima Relevancia mínima Relevancia de página Opciones de página Posición Relevancia de la entrada Opciones de entrada Relevancia del producto Opciones de Producto Barra de progreso Etiqueta para la relevancia Relevancia de la página Relevancia de la entrada Opciones Mostrar productos con esta relevancia Mostrar como Mostrar relevancia en páginas Mostrar relevancia en entradas únicas Mostrar relevancia en página única Ordenar relevancia: descendente Ordenar relevancia: ascendente Estrellas Plugin de prueba que ordena los products por relevancia Texto YITH APC Relevancia de Product 