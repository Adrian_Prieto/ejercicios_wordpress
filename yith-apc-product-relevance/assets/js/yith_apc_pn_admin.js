jQuery(document).ready(function($) {

  $('.button-primary').on('click', function() {

    error_message = 'Check that maximum value is greater than the default value, and the minimun is the lowest. \n';

    if ( $('#yith-apc-pr-min-relevance').length && ( ( parseInt($('#yith-apc-pr-min-relevance').val()) > parseInt($('#yith-apc-pr-default-relevance').val()) ) || ( parseInt( $('#yith-apc-pr-default-relevance').val()) > parseInt($('#yith-apc-pr-max-relevance').val() ) ) ) ) {
      event.preventDefault() //Prevents  the submit from happening.
      alert(error_message);
    } else if ( $('#yith-apc-pr-page-min').length && ( ( parseInt($('#yith-apc-pr-page-min').val()) > parseInt($('#yith-apc-pr-page-default').val()) ) || ( parseInt($('#yith-apc-pr-page-default').val()) > parseInt($('#yith-apc-pr-page-max').val()) ) ) ) {
      event.preventDefault() //Prevents  the submit from happening.
      alert(error_message);
    } else if ( $('#yith-apc-pr-post-min').length && ( ( parseInt($('#yith-apc-pr-post-min').val()) > parseInt($('#yith-apc-pr-post-default').val()) ) || ( parseInt($('#yith-apc-pr-post-default').val()) > parseInt($('#yith-apc-pr-post-max').val()) ) ) ) {
      event.preventDefault() //Prevents  the submit from happening. 
      alert(error_message);
    //} else {
      //console.log ("Se cumple la condicion")
    }
  });
});

