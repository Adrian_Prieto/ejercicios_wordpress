<?php
/*
Plugin Name: My Widget
Plugin URI:
Description: This is a description
Author: Adrian Prieto
Version: 1.0.0
Author URI: yithemes.com
*/


// Crear un widget que me muestre la información en el frontend y que yo pueda meter (nombre, apellidos, dirección, telefono)
function prueba_function1() {
	register_widget( 'APC_Form' );
}
add_action( 'widgets_init', 'prueba_function1' );

class APC_Form extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'APC_form',
			'description' => 'My Widget is awesome',
		);
		parent::__construct( 'APC_form', 'APC Form', $widget_ops );
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		echo $args['before_title'] . 'Datos personales:' . $args['after_title']; // Estos argumentos no pueden ser escapados.
		?>
			<ul>
				<li> <b>Nombre: </b> <?php echo esc_html( $instance['nombre'] ); ?> </li>
				<li> <b>Apellidos: </b> <?php echo esc_html( $instance['apellidos'] ); ?> </li>
				<li> <b>Direccion: </b> <?php echo esc_html( $instance['direccion'] ); ?> </li>
				<li> <b>Telefono: </b> <?php echo esc_html( $instance['telefono'] ); ?> </li>
			</ul>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$nombre    = isset( $instance['nombre'] ) ? esc_attr( $instance['nombre'] ) : '';
		$apellidos = isset( $instance['apellidos'] ) ? esc_attr( $instance['apellidos'] ) : '';
		$direccion = isset( $instance['direccion'] ) ? esc_attr( $instance['direccion'] ) : '';
		$telefono  = isset( $instance['telefono'] ) ? esc_attr( $instance['telefono'] ) : '';
		?>

		<div style="max-height: 120px; overflow: auto;">
		<ul>
			<li>
				<label for="nombre"> <?php esc_html_e( 'Nombre:' ); ?> </label>
				<input type="text" name="<?php echo esc_html( $this->get_field_name( 'nombre' ) ); ?>" id="<?php echo esc_html( $this->get_field_id( 'nombre' ) ); ?>" value="<?php echo esc_attr( $nombre ); ?>"/> </br>
				<label for="apellidos"> <?php esc_html_e( 'Apellidos:' ); ?></label>
				<input type="text" name="<?php echo esc_html( $this->get_field_name( 'apellidos' ) ); ?>" id="<?php echo esc_html( $this->get_field_id( 'apellidos' ) ); ?>" value="<?php echo esc_attr( $apellidos ); ?>"/> </br>
				<label for="direccion"> <?php esc_html_e( 'Dirección:' ); ?> </label>
				<input type="text" name="<?php echo esc_html( $this->get_field_name( 'direccion' ) ); ?>" id="<?php echo esc_html( $this->get_field_id( 'direccion' ) ); ?>" value="<?php echo esc_attr( $direccion ); ?>"/> </br>
				<label for="telefono"> <?php esc_html_e( 'Telefono:' ); ?> </label>
				<input type="text" name="<?php echo esc_html( $this->get_field_name( 'telefono' ) ); ?>" id="<?php echo esc_html( $this->get_field_id( 'telefono' ) ); ?>" value="<?php echo esc_attr( $telefono ); ?>"/> </br>
			</li>
		</ul>
		</div>
		<?php
	}

	public function update( $new_instance, $old_instance ) {

		$instance              = $old_instance;
		$instance['nombre']    = wp_strip_all_tags( $new_instance['nombre'] );
		$instance['apellidos'] = wp_strip_all_tags( $new_instance['apellidos'] );
		$instance['direccion'] = wp_strip_all_tags( $new_instance['direccion'] );
		$instance['telefono']  = wp_strip_all_tags( $new_instance['telefono'] );

		return $instance;
	}
}


// Crear un widget que me liste los últimos 5 posts.
function prueba_function2() {
	register_widget( 'APC_RecentPosts' );
}
add_action( 'widgets_init', 'prueba_function2' );


class APC_RecentPosts extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname'   => 'APC_recentPosts',
			'description' => 'My Widget is awesome',
		);
		parent::__construct( 'APC_recentPosts', 'APC RecentPosts', $widget_ops );
	}

	public function widget( $args, $instance ) {
		// outputs the content of the widget
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . esc_html( apply_filters( 'widget_title', $instance['title'] ) ) . $args['after_title'];
		}

		$selected_posts = get_posts( array( 'numberposts' => $instance['posts_show'] ) );
		?>
		<ul>
			<?php foreach ( $selected_posts as $post ) { ?>
			<li>
				<a href="<?php echo esc_html( get_permalink( $post->ID ) ); ?>">
					<?php echo esc_html( $post->post_title ); ?>
				</a>
			</li>
		</ul>
				<?php
			}
			echo $args['after_widget'];
	}

	public function form( $instance ) {
		$posts_show = isset( $instance['posts_show'] ) ? esc_attr( $instance['posts_show'] ) : 5;
		?>

		<div style="max-height: 120px; overflow: auto;">
			<label for="posts_show"> <?php esc_html_e( 'Nº posts:' ); ?> </label>
			<input type="text" name="<?php echo esc_html( $this->get_field_name( 'posts_show' ) ); ?>" id="<?php echo esc_html( $this->get_field_id( 'posts_show' ) ); ?>" value="<?php echo esc_attr( $posts_show ); ?>"/> </br>		
		</div>

		<?php
	}

	public function update( $new_instance, $old_instance ) {
		// Processes widget options to be saved.
		$instance               = $old_instance;
		$instance['posts_show'] = ( ! empty( $new_instance['posts_show'] ) ) ? $new_instance['posts_show'] : 5;
		return $instance;
	}
}

if ( ! class_exists( 'APC_Post_Types' ) ) {
	class APC_Post_Types {

		private static $instance;
		public static $post_type = 'apc-customlibro';

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );

		}

		public function setup_post_type() {
			$args = array(
				'label'       => 'APC-CustomLibro',
				'description' => 'APC Custom Post Type',
				'public'      => true,
			);
			register_post_type( self::$post_type, $args );
		}


		public function register_taxonomy() {
			// Add new taxonomy, make it hierarchical (like categories)
			$labels = array(
				'name'              => _x( 'Hierarchical taxonomy', 'taxonomy general name', 'apc-plugin' ),
				'singular_name'     => _x( 'Hierarchical', 'taxonomy singular name', 'apc-plugin' ),
				'search_items'      => __( 'Search Hierarchical', 'apc-plugin' ),
				'all_items'         => __( 'All Hierarchical', 'apc-plugin' ),
				'parent_item'       => __( 'Parent Hierarchical', 'apc-plugin' ),
				'parent_item_colon' => __( 'Parent Hierarchical:', 'apc-plugin' ),
				'edit_item'         => __( 'Edit Hierarchical', 'apc-plugin' ),
				'update_item'       => __( 'Update Hierarchical', 'apc-plugin' ),
				'add_new_item'      => __( 'Add New Hierarchical', 'apc-plugin' ),
				'new_item_name'     => __( 'New Hierarchical Name', 'apc-plugin' ),
				'menu_name'         => __( 'Hierarchical', 'apc-plugin' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'apc_tax_hierarchical' ),
			);

			register_taxonomy( 'apc_ps_hietatchical_tax', array( self::$post_type ), $args );
		}

	}
}

APC_Post_Types::get_instance();
?>
