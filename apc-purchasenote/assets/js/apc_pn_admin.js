jQuery(document).ready(function($){
  $('.ga-tst-form__input-badge_color__input').wpColorPicker();
});

document.getElementById('enable_purchasenote').onchange = function() {

  if (this.checked) {
    value_ = "block";
    
    if (document.getElementById('note_enable_badge').checked) {
      value_2 = "block";
    } else {
      value_2 = "none";
    }
  
    document.getElementsByClassName('note_badge_text_field')[0].style.display = value_2;
    document.getElementsByClassName('note_badge_bg_color_field')[0].style.display = value_2;
    document.getElementsByClassName('note_badge_text_color_field')[0].style.display = value_2;

  } else {
    value_ = "none";

    document.getElementsByClassName('note_badge_text_field')[0].style.display = value_;
    document.getElementsByClassName('note_badge_bg_color_field')[0].style.display = value_;
    document.getElementsByClassName('note_badge_text_color_field')[0].style.display = value_;  
  }

  document.getElementsByClassName('note_label_field')[0].style.display = value_;
  document.getElementsByClassName('note_description_field')[0].style.display = value_;
  document.getElementsByClassName('note_field_type_field')[0].style.display = value_;
  document.getElementsByClassName('note_price_settings_field')[0].style.display = value_;
  document.getElementsByClassName('note_price_field')[0].style.display = value_;
  document.getElementsByClassName('note_free_characters_field')[0].style.display = value_;
  document.getElementsByClassName('note_enable_badge_field')[0].style.display = value_;

  
};

document.getElementById('note_enable_badge').onchange = function() {

  if (this.checked) {
    value_ = "block";
  } else {
    value_ = "none";
  }

  document.getElementsByClassName('note_badge_text_field')[0].style.display = value_;
  document.getElementsByClassName('note_badge_bg_color_field')[0].style.display = value_;
  document.getElementsByClassName('note_badge_text_color_field')[0].style.display = value_;
};

document.getElementById('note_price_settings').onchange = function() {

  if ( document.getElementsByClassName('note_price_settings_field')[0].value == "free") {
    value_ = "none";
  } else {
    value_ = "block";
  }

  document.getElementsByClassName('note_price_field')[0].style.display = value_;
  document.getElementsByClassName('note_free_characters_field')[0].style.display = value_;
};

$(document).ready() = function(){
  if (document.getElementById('enable_purchasenote').checked) {
    value_ = "block";

    if (document.getElementById('note_enable_badge').checked) {
      value_2 = "block";
    } else {
      value_2 = "none";
    }


    document.getElementsByClassName('note_badge_text_field')[0].style.display = value_2;
    document.getElementsByClassName('note_badge_bg_color_field')[0].style.display = value_2;
    document.getElementsByClassName('note_badge_text_color_field')[0].style.display = value_2;  

  } else {
    value_ = "none";

    
    document.getElementsByClassName('note_badge_text_field')[0].style.display = value_;
    document.getElementsByClassName('note_badge_bg_color_field')[0].style.display = value_;
    document.getElementsByClassName('note_badge_text_color_field')[0].style.display = value_;  
  }

  document.getElementsByClassName('note_label_field')[0].style.display = value_;
  document.getElementsByClassName('note_description_field')[0].style.display = value_;
  document.getElementsByClassName('note_field_type_field')[0].style.display = value_;
  document.getElementsByClassName('note_price_settings_field')[0].style.display = value_;
  document.getElementsByClassName('note_price_field')[0].style.display = value_;
  document.getElementsByClassName('note_free_characters_field')[0].style.display = value_;
  document.getElementsByClassName('note_enable_badge_field')[0].style.display = value_;
  
  }