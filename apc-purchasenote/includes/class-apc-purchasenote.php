<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_PN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_PurchaseNote' ) ) {
	/** Main plugin class */
	class APC_PurchaseNote {
		/**
		 * Main instance.
		 *
		 * @var undefined
		 */
		private static $instance;

		/**
		 * Main Admin instance.
		 *
		 * @var undefined
		 */
		public $admin = null;

		/**
		 * Main Frontend instance.
		 *
		 * @var undefined
		 */
		public $frontend = null;

		/**
		 * Safe way to call class
		 *
		 * @return class
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 *  Class constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'apc_pn_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-apc-pn-admin.php',
					),
					'frontend' => array(
						'includes/class-apc-pn-frontend.php',
					),
				)
			);

			$this->require( $require );

			$this->init_classes();

			// Finally call the init function.
			$this->init();

		}

		/**
		 * _require Add the main classes' file
		 *
		 * @param  array $main_classes Set of files to be included in the plugin.
		 * @return void
		 */
		protected function require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( APC_PN_DIR_PATH . $class ) ) {
						require_once APC_PN_DIR_PATH . $class;
					}
				}
			}
		}

		/** A safe way to call the construct */
		public function init_classes() {

		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = APC_PN_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = APC_PN_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'apc_purchasenote_call' ) ) {
	/** Run a single instance */
	function apc_purchasenote_call() {
		return APC_PurchaseNote::get_instance();
	}
}
