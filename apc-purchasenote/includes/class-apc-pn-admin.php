<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_PN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_PN_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class APC_PN_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_pn_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_PN_Admin constructor.
		 */
		private function __construct() {
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'apc_pn_custom_tab' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_apc_pn_fields' ) );
			// Save the custom fields.
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_apc_pn_fields' ) );

			/** Admin menu */
			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );

			/* Options added from deault WP options. See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns. */
			add_filter( 'manage_apc_testimonial_posts_columns', array( $this, 'add_apc_testimonial_post_type_columns' ) );

			/* Options added from custom function. See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column */
			add_action( 'manage_apc_purchasenote_posts_custom_column', array( $this, 'display_apc_purchasenote_custom_column' ), 10, 2 );
		
			add_filter( 'woocommerce_order_item_display_meta_key',array($this,'show_as_string_order_item_meta_key'),10,1 );
		}

		/**
		 * Gives a basic description of the new pn_custom_tab
		 *
		 * @param  mixed $tabs Inline argument of Woocommerce hooks.
		 */
		public function apc_pn_custom_tab( $tabs ) {
			// Adds the new tab.
			$tabs['decorating_note'] = array(
				'label'    => __( 'Decorating Note', 'apc-purchasenote' ),
				'target'   => 'apc_purchasenote', // Will be used to create an anchor link so needs to be unique.
				'class'    => array( 'apc_purchasenote', 'show_if_simple', 'show_if_variable' ), // Class for your panel tab - helps hide/show depending on product type.
				'priority' => 50,
			);
			return $tabs;
		}

		/**
		 * Display all fields in custom tabs
		 *
		 * @return void
		 */
		public function display_apc_pn_fields() {
			?>
				<div id='apc_purchasenote' class='panel woocommerce_options_panel'>
					<div class="options_group">
						<?php

						global $post;

						$product = wc_get_product( $post->ID );

						$values_meta = array(
							'enable_purchasenote'   => $product->get_meta( 'enable_purchasenote' ),
							'note_label'            => $product->get_meta( 'note_label' ),
							'note_description'      => $product->get_meta( 'note_description' ),
							'note_field_type'       => $product->get_meta( 'note_field_type' ),
							'note_price_settings'   => $product->get_meta( 'note_price_settings' ),
							'note_price'            => $product->get_meta( 'note_price' ),
							'note_free_characters'  => $product->get_meta( 'note_free_characters' ),
							'note_enable_badge'     => $product->get_meta( 'note_enable_badge' ),
							'note_badge_text'       => $product->get_meta( 'note_badge_text' ),
							'note_badge_bg_color'   => $product->get_meta( 'note_badge_bg_color' ),
							'note_badge_text_color' => $product->get_meta( 'note_badge_text_color' ),
						); // Values got from get_meta.

						woocommerce_wp_checkbox(
							array(
								'id'       => 'enable_purchasenote',
								'label'    => __( 'Enable/Disable the note', 'apc-purchasenote' ),
								'desc_tip' => __( 'Toggle this on/off to display the Purchase Note', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['enable_purchasenote'] ) ? 'no' : $values_meta['enable_purchasenote'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_label',
								'label'    => __( 'Note label', 'apc-purchasenote' ),
								'desc_tip' => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_label'] ) ? 'Note' : $values_meta['note_label'],
							)
						);

						woocommerce_wp_textarea_input(
							array(
								'id'       => 'note_description',
								'label'    => __( 'Note description', 'apc-purchasenote' ),
								'desc_tip' => __( 'Select this option to allow customers to include a custom message', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_description'] ) ? '' : $values_meta['note_description'],
							)
						);

						woocommerce_wp_select(
							array(
								'id'       => 'note_field_type',
								'label'    => __( 'Field Type', 'apc-purchasenote' ),
								'desc_tip' => __( 'Check whether it shows as a text or textarea on frontend', 'apc-purchasenote' ),
								'options'  => array(
									'text'      => __( 'Text', 'apc-purchasenote' ),
									'text_area' => __( 'Text Area', 'apc-purchasenote' ),
								),
								'value'    => empty( $values_meta['note_field_type'] ) ? 'text' : $values_meta['note_field_type'],
							)
						);

						woocommerce_wp_select(
							array(
								'id'       => 'note_price_settings',
								'label'    => __( 'Price Settings', 'apc-purchasenote' ),
								'desc_tip' => __( 'Price can be chosen here', 'apc-purchasenote' ),
								'options'  => array(
									'free'  => __( 'Free', 'apc-purchasenote' ),
									'fixed' => __( 'Fixed Price', 'apc-purchasenote' ),
									'ppc'   => __( 'Price per character', 'apc-purchasenote' ),
								),
								'value'    => empty( $values_meta['note_price_settings'] ) ? 'free' : $values_meta['note_price_settings'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_price',
								'label'    => __( 'Price', 'apc-purchasenote' ),
								'desc_tip' => __( 'Price of the Pruchase Note', 'apc-purchasenote' ),
								'type'     => 'number',
								'value'    => empty( $values_meta['note_price'] ) ? '' : $values_meta['note_price'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_free_characters',
								'label'    => __( 'Free characters', 'apc-purchasenote' ),
								'desc_tip' => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'type'     => 'number',
								'value'    => empty( $values_meta['note_free_characters'] ) ? '' : $values_meta['note_free_characters'],
							)
						);

						woocommerce_wp_checkbox(
							array(
								'id'       => 'note_enable_badge',
								'label'    => __( 'Show/hide badge', 'apc-purchasenote' ),
								'desc_tip' => __( 'Display a badge on the product image in shop and single product pages', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_enable_badge'] ) ? 'no' : $values_meta['note_enable_badge'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_badge_text',
								'label'    => __( 'Text of the badge', 'apc-purchasenote' ),
								'desc_tip' => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_badge_text'] ) ? '' : $values_meta['note_badge_text'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_badge_bg_color',
								'label'    => __( 'Badge Background Color', 'apc-purchasenote' ),
								'class'    => 'ga-tst-form__input-badge_color__input',
								'desc_tip' => __( 'The background color of the badge', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_badge_bg_color'] ) ? '#007694' : $values_meta['note_badge_bg_color'],
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'       => 'note_badge_text_color',
								'label'    => __( 'Badge Text Color', 'apc-purchasenote' ),
								'class'    => 'ga-tst-form__input-badge_color__input',
								'desc_tip' => __( 'The text color of the badge', 'apc-purchasenote' ),
								'value'    => empty( $values_meta['note_badge_text_color'] ) ? '#ffffff' : $values_meta['note_badge_text_color'],
							)
						);

						echo '<input type="hidden" name="apc_purchasenote_nonce" value="' . esc_attr( wp_create_nonce() ) . '">';
						?>
					</div>
				</div>
			<?php
		}

		/**
		 * Save all elements after custom tab form.
		 *
		 * @param  mixed $post_id Inline argument.
		 */
		public function save_apc_pn_fields( $post_id ) {

			// Check if our nonce is set.
			if ( ! isset( $_POST['apc_purchasenote_nonce'] ) ) {
				return $post_id;
			}
			$nonce = sanitize_text_field( wp_unslash( $_POST['apc_purchasenote_nonce'] ) );

			// Verify that the nonce is valid.
			if ( ! wp_verify_nonce( $nonce ) ) {
				return $post_id;
			}

			if ( isset( $_POST['enable_purchasenote'] ) && ( 'yes' === $_POST['enable_purchasenote'] ) ) {

				$product = wc_get_product( $post_id );

				// Save all parameters.

				$product->update_meta_data( 'enable_purchasenote', 'yes' ); // Default value, no need to get POST.

				if ( isset( $_POST['note_label'] ) ) {
					$note_label = sanitize_text_field( wp_unslash( $_POST['note_label'] ) );
					$product->update_meta_data( 'note_label', sanitize_text_field( $note_label ) );
				}

				if ( isset( $_POST['note_description'] ) ) {
					$note_description = sanitize_text_field( wp_unslash( $_POST['note_description'] ) );
					$product->update_meta_data( 'note_description', sanitize_text_field( $note_description ) );
				}

				if ( isset( $_POST['note_field_type'] ) ) {
					$note_field_type = sanitize_text_field( wp_unslash( $_POST['note_field_type'] ) );
					$product->update_meta_data( 'note_field_type', sanitize_text_field( $note_field_type ) );
				}

				if ( isset( $_POST['note_price_settings'] ) ) {
					$note_price_settings = sanitize_text_field( wp_unslash( $_POST['note_price_settings'] ) );
					$product->update_meta_data( 'note_price_settings', sanitize_text_field( $note_price_settings ) );
				}

				if ( isset( $_POST['note_price'] ) ) {
					$note_price = sanitize_text_field( wp_unslash( $_POST['note_price'] ) );
					$product->update_meta_data( 'note_price', sanitize_text_field( $note_price ) );
				}

				if ( isset( $_POST['note_price_settings'] ) ) {
					$note_price_settings = sanitize_text_field( wp_unslash( $_POST['note_price_settings'] ) );
					$product->update_meta_data( 'note_price_settings', sanitize_text_field( $note_price_settings ) );
				}

				if ( isset( $_POST['note_free_characters'] ) ) {
					$note_free_characters = sanitize_text_field( wp_unslash( $_POST['note_free_characters'] ) );
					$product->update_meta_data( 'note_free_characters', sanitize_text_field( $note_free_characters ) );
				}

				if ( isset( $_POST['note_enable_badge'] ) ) {
					$note_enable_badge = sanitize_text_field( wp_unslash( $_POST['note_enable_badge'] ) );
					$product->update_meta_data( 'note_enable_badge', sanitize_text_field( $note_enable_badge ) );
				}

				if ( isset( $_POST['note_badge_text'] ) ) {
					$note_badge_text = sanitize_text_field( wp_unslash( $_POST['note_badge_text'] ) );
					$product->update_meta_data( 'note_badge_text', sanitize_text_field( $note_badge_text ) );
				}

				if ( isset( $_POST['note_badge_bg_color'] ) ) {
					$note_badge_bg_color = sanitize_text_field( wp_unslash( $_POST['note_badge_bg_color'] ) );
					$product->update_meta_data( 'note_badge_bg_color', sanitize_text_field( $note_badge_bg_color ) );
				}

				if ( isset( $_POST['note_badge_text_color'] ) ) {
					$note_badge_text_color = sanitize_text_field( wp_unslash( $_POST['note_badge_text_color'] ) );
					$product->update_meta_data( 'note_badge_text_color', sanitize_text_field( $note_badge_text_color ) );
				}

				$note_badge_text_color = isset( $_POST['note_badge_text_color'] );

				$product->save();
			}
		}

		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/.
				add_menu_page(
					esc_html__( 'Purchase Note Options', 'apc-purchasenote' ),
					esc_html__( 'Purchase Note Options', 'apc-purchasenote' ),
					'edit_apc_purchasenote',
					'purchase_note_options',
					array( $this, 'apc_purchase_note_options_page' ),
					'',
					40
				);
		}

		/** Callback custom menu page */
		public function apc_purchase_note_options_page() {
			apc_pn_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/** Add the fields in the shortcode attribute management page */
		public function register_settings() {

			$page_name    = 'pn-options-page';
			$section_name = 'options_section';

			$padding = array( 'top' => 20, 'right' => 20, 'bottom' => 20, 'left' => 25 );
			$border  = array( 'weight' => 1, 'style' => 'solid', 'color' => '#d8d8d8', 'radius' => 7 );
			$badge   = array( 'shop' => 'top-left', 'product' => 'top-left' );

			$setting_fields = array(
				array(
					'id'       => 'apc_pn_padding',
					'title'    => esc_html__( 'Padding', 'apc-purchasenote' ),
					'callback' => 'print_pn_padding',
					'section'  => $section_name,
					'fields'   => array(
						array(
							'type'           => 'number',
							'label'          => __( 'Top', 'apc-purchasenote' ),
							'id'             => 'apc_pn_padding_top',
							'name'           => 'apc_pn_padding[top]',
							'value'          => intval( $padding['top'] ),
							'with_container' => false,
						),
						array(
							'type'           => 'number',
							'label'          => __( 'Right', 'apc-purchasenote' ),
							'id'             => 'apc_pn_padding_right',
							'name'           => 'apc_pn_padding[right]',
							'value'          => intval( $padding['right'] ),
							'with_container' => false,
						),
						array(
							'type'           => 'number',
							'label'          => __( 'Bottom', 'apc-purchasenote' ),
							'id'             => 'apc_pn_padding_bottom',
							'name'           => 'apc_pn_padding[bottom]',
							'value'          => intval( $padding['bottom'] ),
							'with_container' => false,
						),
						array(
							'type'           => 'number',
							'label'          => __( 'Left', 'apc-purchasenote' ),
							'id'             => 'apc_pn_padding_left',
							'name'           => 'apc_pn_padding[left]',
							'value'          => intval( $padding['left'] ),
							'with_container' => false,
						),
					),
				),
				array(
					'id'       => 'apc_pn_border',
					'title'    => esc_html__( 'Border', 'apc-purchasenote' ),
					'callback' => 'print_pn_border',
					'section'  => $section_name,
					'fields'   => array(
						array(
							'type'           => 'number',
							'label'          => __( 'Weight', 'apc-purchasenote' ),
							'id'             => 'apc_pn_border_weight',
							'name'           => 'apc_pn_border[weight]',
							'value'          => intval( $border['weight'] ),
							'with_container' => false,
						),
						array(
							'type'           => 'text',
							'label'          => __( 'Style', 'apc-purchasenote' ),
							'id'             => 'apc_pn_border_style',
							'name'           => 'apc_pn_border[style]',
							'value'          => $border['style'],
							'with_container' => false,
						),
						array(
							'type'           => 'text',
							'label'          => __( 'Color', 'apc-purchasenote' ),
							'id'             => 'apc_pn_border_color',
							'name'           => 'apc_pn_border[color]',
							'value'          => $border['color'],
							'with_container' => false,
						),
						array(
							'type'           => 'number',
							'label'          => __( 'Radius', 'apc-purchasenote' ),
							'id'             => 'apc_pn_border_radius',
							'name'           => 'apc_pn_border[radius]',
							'value'          => intval( $border['color'] ),
							'with_container' => false,
						),
					),
				),

				array(
					'id'       => 'apc_pn_badge_pos',
					'title'    => esc_html__( 'Badge Position', 'apc-purchasenote' ),
					'callback' => 'print_pn_badge_pos',
					'section'  => $section_name,
					'fields'   => array(
						array(
							'type'           => 'shop',
							'label'          => __( 'In Shop', 'apc-purchasenote' ),
							'id'             => 'apc_pn_badge_pos_shop',
							'name'           => 'apc_pn_badge_pos[shop]',
							'value'          => $badge['shop'],
							'with_container' => false,
						),
						array(
							'type'           => 'product',
							'label'          => __( 'In Product', 'apc-purchasenote' ),
							'id'             => 'apc_pn_badge_pos_product',
							'name'           => 'apc_pn_badge_pos[product]',
							'value'          => $badge['product'],
							'with_container' => false,
						),
					),
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'apc-purchasenote' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				$id = $field['id'];

				add_settings_field(
					$id,
					$field['title'],
					array( $this, $field['callback'] ),
					$page_name,
					$field['section'],
					$field['fields']
				);
				register_setting( $page_name, $id );
			}
		}

		/**
		 * Print the show padding field
		 */
		public function print_pn_padding() {

			$default_padding = array( 'top' => 20, 'right' => 20, 'bottom' => 20, 'left' => 25 );
			$padding         = get_option( 'apc_pn_padding', $default_padding );

			?>
			<label class="label_padding_top" for="label_padding_top"><?php esc_html_e( 'Top', 'apc-purchasenote' ); ?></label>
			<input type="number" id="apc_pn_padding_top" name="apc_pn_padding[top]"
				value="<?php echo '' !== $padding['top'] ? esc_html( $padding['top'] ) : 20; ?>">
			<br/>
			<label class="label_padding_right" for="label_padding_right"><?php esc_html_e( 'Right', 'apc-purchasenote' ); ?></label>
			<input type="number" id="apc_pn_padding_right" name="apc_pn_padding[right]"
				value="<?php echo '' !== $padding['right'] ? esc_html( $padding['right'] ) : 25; ?>">
			<br/>
			<label class="label_padding_bottom" for="label_padding_bottom"><?php esc_html_e( 'Bottom', 'apc-purchasenote' ); ?></label>
			<input type="number" id="apc_pn_padding_bottom" name="apc_pn_padding[bottom]"
				value="<?php echo '' !== $padding['bottom'] ? esc_html( $padding['bottom'] ) : 25; ?>">
			<br/>
			<label class="label_padding_left" for="label_padding_left"><?php esc_html_e( 'Left', 'apc-purchasenote' ); ?></label>
			<input type="number" id="apc_pn_padding_left" name="apc_pn_padding[left]"
				value="<?php echo '' !== $padding['left'] ? esc_html( $padding['left'] ) : 25; ?>">
			<?php
		}

		/**
		 * Print the show border field
		 */
		public function print_pn_border() {

			$default_border = array( 'weight' => 1, 'style' => 'solid', 'color' => '#d8d8d8', 'radius' => 7 );
			$border         = get_option( 'apc_pn_border', $default_border );

			?>
			<div>
				<label class="label_border_weight" for="label_border_weight"><?php esc_html_e( 'Weight', 'apc-purchasenote' ); ?></label>
				<input type="number" id="apc_pn_border_weight" name="apc_pn_border[weight]"
					value="<?php echo esc_html( $border['weight'] ); ?>">
				<br/>
				<label class="label_border_style" for="label_border_style"><?php esc_html_e( 'Style', 'apc-purchasenote' ); ?></label>
				<input type="text" id="apc_pn_border_style" name="apc_pn_border[style]"
					value="<?php echo esc_html( $border['style'] ); ?>">
				<br/>
				<?php 
					// View coding preview.
					apc_pn_get_view(
						'/inputs/colorpicker.php',
						array(
							'element_id'      => 'border_color',
							'element_name'    => 'border[color]',
							'element_value'   => $border['color'],
							'element_display' => __( ' Border Color', 'apc-purchasenote' ),
						)
					);
				?>
				<br/>
				<label class="label_border_radius" for="label_border_radius"><?php esc_html_e( 'Radius', 'apc-purchasenote' ); ?></label>
				<input type="number" id="apc_pn_border_radius" name="apc_pn_border[radius]"
					value="<?php echo esc_html( $border['radius'] ); ?>">
			</div>
			<?php
		}

		/**
		 * Print the show badge position in shop field
		 */
		public function print_pn_badge_pos() {

			$default_badge = array( 'shop' => 'top-left', 'product' => 'top-left' );
			$badge         = get_option( 'apc_pn_badge_pos', $default_badge );

			?>
				<label class="label_badge_pos_shop" for="label_badge_pos_shop"><?php esc_html_e( 'In Shop', 'apc-purchasenote' ); ?></label>
				<select name="apc_pn_badge_pos[shop]" id="apc_pn_badge_pos_shop" class="apc_pn_badge_pos[shop]">
					<option value="top-left"> <?php esc_html_e( 'Top Left', 'apc-purchasenote' ); ?> </option>
					<option <?php if ('top-right' === $badge['shop'] ) echo 'selected'; ?> value="top-right"> <?php esc_html_e( 'Top Right', 'apc-purchasenote' ); ?> </option>
				</select>
				</br>

				<label class="label_badge_pos_product" for="label_badge_pos_product"><?php esc_html_e( 'In Product', 'apc-purchasenote' ); ?></label>
				<select name="apc_pn_badge_pos[product]" id="apc_pn_badge_pos_product" class="apc_pn_badge_pos[product]">
					<option value="top-left"> <?php esc_html_e( 'Top Left', 'apc-purchasenote' ); ?> </option>
					<option <?php if ('top-right' === $badge['product'] ) echo 'selected'; ?> value="top-right"> <?php esc_html_e( 'Top Right', 'apc-purchasenote' ); ?> </option>
				</select>
			<?php
		}

		/**
		 * Show Meta Key as a "friendly name" on admin.
		 *
		 * @param  string $display_key Display name. Will be changed here.
		 */
		public function show_as_string_order_item_meta_key( $display_key ) {
			if ( strpos( $display_key, 'content_note' ) !== false ) {
				if ( 'content_note' === $display_key ) {
					$display_key = esc_html__( 'Content Note', 'yith-woocommerce-gift-cards' );
				}
			}

			if ( strpos( $display_key, 'note_price' ) !== false ) {
				if ( 'note_price' === $display_key ) {
					$display_key = esc_html__( 'Note Price', 'apc-purchasenote' );
				}
			}
			return $display_key;
		}
	}
}
