<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! function_exists( 'purchasenote_admin_enqueue_scripts' ) ) {
	/** CSS and JS Register Function for Admin */
	function purchasenote_admin_enqueue_scripts() {
		$js_url = APC_PN_DIR_ASSETS_JS_URL . '/apc_pn_admin.js';

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'my-script-handle', $js_url, array( 'wp-color-picker' ), true, true );
	}
}
add_action( 'admin_enqueue_scripts', 'purchasenote_admin_enqueue_scripts' );

if ( ! function_exists( 'purchasenote_wp_enqueue_scripts' ) ) {
	/** CSS Register Function for FrontEnd */
	function purchasenote_wp_enqueue_scripts() {
		$css_url = APC_PN_DIR_ASSETS_CSS_URL . '/apc_pn.css';
		wp_register_style( 'apc-pn-purchasenote-style', $css_url );
		wp_enqueue_style( 'apc-pn-purchasenote-style' );
	}
}
add_action( 'wp_enqueue_scripts', 'purchasenote_wp_enqueue_scripts' );

if ( ! function_exists( 'apc_pn_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function apc_pn_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = APC_PN_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


if ( ! function_exists( 'apc_pn_admin_give_permissions' ) ) {
	/**
	 * Give plugin permissions to admin
	 *
	 * @return void
	 */
	function apc_pn_admin_give_permissions() {
		$admin_role = get_role( 'administrator' );
		$admin_role->add_cap( 'edit_apc_purchasenote' );
	}
}
add_action( 'admin_init', 'apc_pn_admin_give_permissions', 999 ); // Although admin_init, or even after_setup_theme can also be used.


if ( ! function_exists( 'apc_purchasenote_testimonials_on_cart' ) ) {
	/**
	 * Basic shortcode to show testimonials
	 *
	 * @param mixed $atts Attributes or parameters of the shortcode, look at array below for possible arguments.
	 * @param mixed $content Show additional content.
	 */
	function apc_purchasenote_testimonials_on_cart( $atts, $content = null ) {

		// Get Meta Options.
		$padding = get_option( 'apc_pn_padding' );
		$border  = get_option( 'apc_pn_border' );

		$padding_css = 'margin:' . $padding['top'] . 'px ' . $padding['right'] . 'px ' . $padding['bottom'] . 'px ' . $padding['left'] . 'px; ';
		$border_css  = 'border-style:' . $border['style'] . '; border-color:' . $border['color'] . '; border-width:' . $border['weight'] . 'px; border-radius:' . $border['radius'] . ' px;';
		$style_css   = $padding_css . $border_css;

		global $post;
		$product  = wc_get_product( $post->ID );
		$id_input = 'purchase_input';
		$id_cost  = 'purchase_cost';

		// First get_elements.
		$atts = array(
			'enable_purchasenote'   => $product->get_meta( 'enable_purchasenote' ),
			'note_label'            => $product->get_meta( 'note_label' ),
			'note_description'      => $product->get_meta( 'note_description' ),
			'note_field_type'       => $product->get_meta( 'note_field_type' ),
			'note_price_settings'   => $product->get_meta( 'note_price_settings' ),
			'note_price'            => $product->get_meta( 'note_price' ),
			'note_free_characters'  => $product->get_meta( 'note_free_characters' ),
			'note_enable_badge'     => $product->get_meta( 'note_enable_badge' ),
			'note_badge_text'       => $product->get_meta( 'note_badge_text' ),
			'note_badge_bg_color'   => $product->get_meta( 'note_badge_bg_color' ),
			'note_badge_text_color' => $product->get_meta( 'note_badge_text_color' ),
		);

		// Get cost from calculations.
		$cost = 0;
		if ( 'fixed' === $atts['note_price_settings'] ) {
			$cost = intval( $atts['note_price'] );
		}

		// Second, HTML.
		if ( 'yes' === $atts['enable_purchasenote'] ) {
			?> 
				<div class="purchasenote_box" style="<?php echo esc_attr( $style_css ); ?> ">
						<label class="purchasenote_title"> <?php echo esc_html( $atts['note_label'] ); ?> </label>
						<label class="purchasenote_description"> <?php echo esc_html( $atts['note_description'] ); ?> </label>
						<?php if ( 'text' === $atts['note_field_type'] ) { ?>
							<input id="<?php echo esc_html( $id_input ); ?>" class="purchasenote_input" type="text" name="<?php echo esc_html( $id_input ); ?>"></input>
						<?php } else { ?>
							<textarea id="<?php echo esc_html( $id_input ); ?>" class="purchasenote_input" name="<?php echo esc_html( $id_input ); ?>"></textarea>
						<?php } ?>

						<label id="<?php echo esc_html( $id_cost ); ?>" class="purchasenote_cost"> + <?php echo esc_html( $cost ); ?> € </label>
				</div>

				<?php
				if ( 'ppc' === $atts['note_price_settings'] ) {
					// JS script for each PPC item about changing value.
					?>
					<script type="text/javascript">
						document.getElementById("<?php echo esc_html( $id_input ); ?>").onchange = function() {
							var value = document.getElementById( "<?php echo esc_html( $id_input ); ?>" ).value;
							var free_characters = parseInt ( <?php echo esc_html( $atts['note_free_characters'] ); ?> );
							var price = parseFloat ( <?php echo esc_html( $atts['note_price'] ); ?> ) ;
							var cost = ( value.length < free_characters ) ? 0.00 : price * ( value.length - free_characters );

							document.getElementById("<?php echo esc_html( $id_cost ); ?>").innerHTML = " + " + cost + " € ";
						};
					</script>
					<?php
				}
		}
	}
}
add_action( 'woocommerce_before_add_to_cart_button', 'apc_purchasenote_testimonials_on_cart', 10 );


/**
 * Shortcode that shows the badge style on shop and single-product thumbnails.
 */
function show_badge_on_shop() {
	$badge      = get_option( 'apc_pn_badge_pos' );
	$badge_shop = $badge['shop'];

	global $product;

	$atts = array(
		'note_enable_badge'     => $product->get_meta( 'note_enable_badge' ),
		'note_badge_text'       => $product->get_meta( 'note_badge_text' ),
		'note_badge_bg_color'   => $product->get_meta( 'note_badge_bg_color' ),
		'note_badge_text_color' => $product->get_meta( 'note_badge_text_color' ),
	);

	if ( 'yes' === $atts['note_enable_badge'] ) {
		$css = 'text-align:center; position:absolute; color:' . $atts['note_badge_text_color'] . '; background:' . $atts['note_badge_bg_color'] . ';';

		if ( 'top-left' === $badge_shop ) {
			$css .= 'top:10px; left:10px';
		} else {
			$css .= 'top:10px; right:10px';
		}

		echo '<span style="' . esc_html( $css ) . '">' . esc_html( $atts['note_badge_text'] ) . '</span>';
	}
}

add_action( 'woocommerce_before_shop_loop_item_title', 'show_badge_on_shop' );

/**
 * Shortcode that shows the badge style on shop and single-product thumbnails.
 */
function show_badge_on_product() {
	$badge         = get_option( 'apc_pn_badge_pos' );
	$badge_product = $badge['product'];

	global $product;

	$atts = array(
		'note_enable_badge'     => $product->get_meta( 'note_enable_badge' ),
		'note_badge_text'       => $product->get_meta( 'note_badge_text' ),
		'note_badge_bg_color'   => $product->get_meta( 'note_badge_bg_color' ),
		'note_badge_text_color' => $product->get_meta( 'note_badge_text_color' ),
	);

	if ( 'yes' === $atts['note_enable_badge'] ) {
		$css = 'text-align:center; position:absolute; color:' . $atts['note_badge_text_color'] . '; background:' . $atts['note_badge_bg_color'] . ';';

		if ( 'top-left' === $badge_product ) {
			$css .= 'top:10px; left:10px';
		} else {
			$css .= 'top:10px; right:10px';
		}

		echo '<span style="' . esc_html( $css ) . '">' . esc_html( $atts['note_badge_text'] ) . '</span>';
	}
}

add_action( 'woocommerce_product_thumbnails', 'show_badge_on_product' );


/**
 * Doing the calculations and store arguments for the cart will be done here.
 *
 * @param  mixed $cart_item_data Cart arguments to be stored.
 * @param  mixed $product_id     Related to an Product ID.
 * @param  mixed $variation_id   Will be 0, so optional.
 * @param  mixed $quantity       Quantity, just in case.
 */
function add_cart_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {

	$note_price_settings  = get_post_meta( $product_id, 'note_price_settings', true );
	$note_price           = get_post_meta( $product_id, 'note_price', true );
	$note_free_characters = get_post_meta( $product_id, 'note_free_characters', true );

	$cart_item_data['content_note'] = sanitize_text_field( wp_unslash( $_POST['purchase_input'] ) );

	/* Hacemos la magia del precio aquí. */
	$cost = 0;
	if ( 'fixed' === $note_price_settings ) {
		$cost = intval( $note_price );
	} elseif ( 'ppc' === $note_price_settings ) {
		$cost = ( strlen( $cart_item_data['content_note'] ) < $note_free_characters ) ? 0 : $note_price * ( strlen( $cart_item_data['content_note'] ) - $note_free_characters );
	}

	$cart_item_data['note_price'] = $cost;

	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 4 );

/**
 * Save cart used in the section into real cart data.
 *
 * @param  mixed $cart_item_data Real cart.
 * @param  mixed $cart_item_session_data Session cart.
 * @param  mixed $cart_item_key To check nonce.
 */
function apc_pn_save_cart_session( $cart_item_data, $cart_item_session_data, $cart_item_key ) {
	if ( isset( $cart_item_data['note_price'] ) ) {
		$cart_item_data['note_price'] = $cart_item_session_data['note_price'];
	}

	if ( isset( $cart_item_data['content_note'] ) ) {
		$cart_item_data['content_note'] = $cart_item_session_data['content_note'];
	}

	return $cart_item_data;
}
add_filter( 'woocommerce_new_order_item', 'apc_pn_save_cart_session', 10, 3 );



/**
 * Show note on cart
 *
 * @param  mixed $data      Data to be showed/returned.
 * @param  mixed $cart_item Object where we saved our data before.
 */
function apc_pn_add_order_item_meta( $data, $cart_item ) {

	if ( isset( $cart_item['content_note'] ) ) {
		$data[] = array(
			'name'  => __( 'Note', 'apc-purchasenote' ),
			'value' => $cart_item['content_note'],
		);
	}

	return $data;
}
add_filter( 'woocommerce_get_item_data', 'apc_pn_add_order_item_meta', 10, 2 );


/**
 * Save metadata from cart into db.
 *
 * @param  mixed $item_id Item Id.
 * @param  mixed $values Values retrieved from cart.
 * @param  mixed $key Key from using nonce.
 */
function apc_pn_save_metadata( $item_id, $values, $key ) {
	if ( isset( $values['content_note'] ) ) {
		wc_add_order_item_meta( $item_id, 'content_note', $values['content_note'] );
	}

	if ( isset( $values['note_price'] ) ) {
		wc_add_order_item_meta( $item_id, 'note_price', $values['note_price'] );
	}

}
add_action( 'woocommerce_add_order_item_meta', 'apc_pn_save_metadata', 10, 3 );


/**
 * Function to add the note price on cart items.
 *
 * @param  mixed $cart_object The cart to be querying on.
 */
function apc_pn_recalculate_price( $cart_object ) {
	foreach ( $cart_object->get_cart() as $hash => $value ) {

		if ( isset( $value['note_price'] ) ) {
			$note_total_cost = $value['note_price'];

			$newprice = $value['data']->get_price() + $note_total_cost;
			$value['data']->set_price( $newprice );
		}
	}
}

add_action( 'woocommerce_before_calculate_totals', 'apc_pn_recalculate_price' );

?>
