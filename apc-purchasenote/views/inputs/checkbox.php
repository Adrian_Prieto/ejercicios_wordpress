<?php
/**
 *  @param array  $classes Classes to be assigned to the element.
 *  @param string $element_id   Id of the element.
 *  @param string $element_name Name of the element
 *  @param string $element_display Name displayed to the users (label).
 *  @param bool $checked Checks the state of the checkbox.
 *  @param string $element_class
 */
?>

 <div class="apc_pn_input_checkbox_container
    <?php
    if ( ! empty( $classes ) && is_array( $classes ) ) {
        foreach ( $classes as $class ) {
            echo esc_html( ' ' . $class );
        }
    }
    ?>
">

    <label class="label_checkbox_<?php echo esc_html( $element_name ); ?>" for="input_checkbox_<?php echo esc_html( $element_name ); ?>">
        <?php echo esc_html( ! empty( $element_display ) ? $element_display : $element_name ); ?>
    </label>

    <input
        type="checkbox"
        class="input_checkbox_<?php echo esc_html( $element_name ); ?>"
        name="<?php echo esc_html( $element_name ); ?>"
        id="input_checkbox_<?php echo esc_html( $element_id ); ?>"
        value="<?php echo esc_html( $element_name ); ?>"
        <?php echo ! empty( $checked ) && $checked ? 'checked' : ''; ?>
    >

</div>