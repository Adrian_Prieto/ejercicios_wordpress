<?php
    wp_nonce_field('apc_testimonial_nonce', 'my_apc_testimonial_nonce');
?>

<div class="ga-tst-form">
<!-- Testimonial role field -->
<div class="ga-tst-form__input ga-tst-form__input-role"> 
        <label class="ga-tst-form__input-role__label" for="tst_role"><?php _e( 'Role', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-role__input" name="_apc_tt_role" id="tst_role"
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_role', true ) ); ?>">
        </br>
        <label class="ga-tst-form__input-company__label" for="tst_company"><?php _e( 'Company', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-company__input" name="_apc_tt_company" id="tst_company"
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_company', true ) ); ?>">
        </br>
        <label class="ga-tst-form__input-company_url__label" for="tst_company_url"><?php _e( 'Company URL', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-company_url__input" name="_apc_tt_company_url" id="tst_company_url"
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_company_url', true ) ); ?>">
        </br>
        <label class="ga-tst-form__input-company_url__email" for="tst_email"><?php _e( 'Email', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-email__input" name="_apc_tt_email" id="tst_email"
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_email', true ) ); ?>">
        </br>
        <label class="ga-tst-form__input-vip" for="tst_vip"><?php _e( 'Vip', 'apc_testimonial' ); ?></label>
        <input type="checkbox" class="ga-tst-form__input-vip__input" name="_apc_tt_vip" id="tst_vip" 
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_vip', true ) );?>" checked>
        </br>
        <label class="ga-tst-form__input-badge" for="tst_badge"><?php _e( 'Enable Badge', 'apc_testimonial' ); ?></label>
        <input type="checkbox" class="ga-tst-form__input-badge__input" name="_apc_tt_badge" id="tst_badge" 
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_badge', true ) );?>">
        </br>
        <label class="ga-tst-form__input-badge_text" for="tst_badge_text"><?php _e( 'Badge Text', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-badge_text__input" name="_apc_tt_badge_text" id="tst_badge_text" 
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_badge_text', true ) );?>" disabled >
        </br>
        <label class="ga-tst-form__input-badge_color" for="tst_badge_color"><?php _e( 'Badge Color', 'apc_testimonial' ); ?></label>
        <input type="text" class="ga-tst-form__input-badge_color__input" name="_apc_tt_badge_color" id="tst_badge_color" 
            value="<?php echo esc_html( get_post_meta( $post->ID, '_apc_tt_badge_color', true ) );?>" disabled data-default-color="#effeff">
        </br>
        <label class="ga-tst-form__input-badge_color" for="tst_rating"><?php _e( 'Rating', 'apc_testimonial' ); ?></label>
        <span class="star__container">
            <input type="radio" name="_apc_tt_rating" value="5" id="tst_rating-1" class="star__radio visuhide"></a><label for="tst_rating-1">☆</label>
            <input type="radio" name="_apc_tt_rating" value="4" id="tst_rating-2" class="star__radio visuhide"></a><label for="tst_rating-2">☆</label>
            <input type="radio" name="_apc_tt_rating" value="3" id="tst_rating-3" class="star__radio visuhide"></a><label for="tst_rating-3">☆</label>
            <input type="radio" name="_apc_tt_rating" value="2" id="tst_rating-4" class="star__radio visuhide"></a><label for="tst_rating-4">☆</label>
            <input type="radio" name="_apc_tt_rating" value="1" id="tst_rating-5" class="star__radio visuhide"></a><label for="tst_rating-5">☆</label>
        </span>
    </div>
</div>