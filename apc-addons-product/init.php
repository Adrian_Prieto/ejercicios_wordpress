<?php
/**
 * Plugin Name: APC Addons for Frontend Products
 * Description: Test Plugins for Product Addons
 * Version: 1.0.0
 * Author: Adrián Prieto
 * Text Domain: apc-addonsproduct
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;  // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'APC_AD_VERSION' ) ) {
	define( 'APC_AD_VERSION', '1.0.0' );
}

if ( ! defined( 'APC_AD_DIR_URL' ) ) {
	define( 'APC_AD_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'APC_AD_DIR_ASSETS_URL' ) ) {
	define( 'APC_AD_DIR_ASSETS_URL', APC_AD_DIR_URL . 'assets' );
}

if ( ! defined( 'APC_AD_DIR_ASSETS_CSS_URL' ) ) {
	define( 'APC_AD_DIR_ASSETS_CSS_URL', APC_AD_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'APC_AD_DIR_ASSETS_JS_URL' ) ) {
	define( 'APC_AD_DIR_ASSETS_JS_URL', APC_AD_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'APC_AD_DIR_PATH' ) ) {
	define( 'APC_AD_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'APC_AD_DIR_INCLUDES_PATH' ) ) {
	define( 'APC_AD_DIR_INCLUDES_PATH', APC_AD_DIR_PATH . '/includes' );
}

if ( ! defined( 'APC_AD_DIR_TEMPLATES_PATH' ) ) {
	define( 'APC_AD_DIR_TEMPLATES_PATH', APC_AD_DIR_PATH . '/templates' );
}

if ( ! defined( 'APC_AD_DIR_VIEWS_PATH' ) ) {
	define( 'APC_AD_DIR_VIEWS_PATH', APC_AD_DIR_PATH . 'views' );
}

/**
 * Include the scripts
 */
if ( ! function_exists( 'apc_ad_init_classes' ) ) {

	/** Calling la */
	function apc_ad_init_classes() {

		load_plugin_textdomain( 'apc-addonsproduct', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins.
		require_once APC_AD_DIR_INCLUDES_PATH . '/class-apc-addonsproduct.php'; 

		if ( class_exists( 'APC_AddonsProduct' ) ) {
			/* Call the main function */
			apc_addonsproduct_call();
		}
	}
}

add_action( 'plugins_loaded', 'Apc_ad_init_classes', 11 );




