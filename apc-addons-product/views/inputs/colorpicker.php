<?php
/**
 *  @param array  $classes Classes to be assigned to the element.
 *  @param string $element_id   Id of the element.
 *  @param string $element_name Name of the element
 *  @param string $element_display Name displayed to the users (label).
 *  @param string $element_value Value of the color as default.
 */
?>


 <div class="apc_pn_input_checkbox_container
    <?php
    if ( ! empty( $classes ) && is_array( $classes ) ) {
        foreach ( $classes as $class ) {
            echo esc_html( ' ' . $class );
        }
    }

    error_log ('Called to ' . $element_display);
    ?>
">

    <label class="label_colorpicker_<?php echo esc_html( $element_name ); ?>" for="input_colorpicker_<?php echo esc_html( $element_name ); ?>">
        <?php echo esc_html( ! empty( $element_display ) ? $element_display : $element_name ); ?>
    </label>

    <input
        type="text"
        class="apc_pn_<?php echo esc_html( $element_name ); ?> ga-tst-form__input-badge_color__input"
        name="<?php echo esc_html( $element_name ); ?>"
        id="apc_pb_<?php echo esc_html( $element_id ); ?>"
        value="<?php echo esc_html( $element_value ); ?>"
    >

</div>
