<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_AD_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_AD_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class APC_AD_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_pn_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_PN_Admin constructor.
		 */
		private function __construct() {
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'apc_ad_add_addon_tab' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_apc_addon_form_fields' ) );
			// Save the custom fields.
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_apc_addons_fields' ) );

			/**  VARIANT HOOKS */
			// Display variations.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'display_apc_addon_variants' ), 10, 3 );
			// Save variations.
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'save_apc_addon_variants' ) );
		}


		/** CSS and JS Register Function for Admin */
		/**
		 * Gives a basic description of the new pn_custom_tab
		 *
		 * @param  mixed $tabs Inline argument of Woocommerce hooks.
		 */
		public function apc_ad_add_addon_tab( $tabs ) {
			// Adds the new tab.
			$tabs['general_addons'] = array(
				'label'    => __( 'Add-ons', 'apc-addonsproduct' ),
				'target'   => 'apc_addonsproduct', // Will be used to create an anchor link so needs to be unique.
				'class'    => array( 'apc_addonsproduct', 'show_if_simple', 'show_if_variable' ), // Class for your panel tab - helps hide/show depending on product type.
				'priority' => 50,
			);
			return $tabs;
		}

		/**
		 * Display all fields in custom tabs
		 *
		 * @return void
		 */
		public function display_apc_addon_form_fields() {
			?>
				<div id='apc_addonsproduct' class='panel woocommerce_options_panel'>
					<?php

					global $post;
					$product  = wc_get_product( $post->ID );
					$all_form = $product->get_meta( 'apc_ad_addons' );

					?>
					<button id='new_addon_button' name='new_addon_button' type="button"> <?php echo esc_html__( 'New Add-on', 'apc-addonsproduct' ); ?> </button>
						<!-- Introducimos template de form -->
						<div id="addon_form_0" hidden>
							<h3 class="addon-ui-accordion-header"><?php echo( esc_html( 'Addon 0' ) ); ?> </h3>			
							<div class="content">

							<input type="checkbox" class="addon-switch-enable onoffswitch-checkbox"
								name="addon_enable" id="addon_enable_0" value="yes"
							/><?php echo esc_html__( ' Enable Addon on Frontend', 'apc-addonsproduct' ); ?>
							<?php

								woocommerce_wp_text_input(
									array(
										'id'          => 'addon_name_0',
										'name'        => 'addon_name',
										'label'       => __( 'Add-on name', 'apc-addonsproduct' ),
										'description' => __( 'Name of this Add-on', 'apc-addonsproduct' ),
										'value'       => '',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_textarea_input(
									array(
										'id'          => 'addon_description_0',
										'name'        => 'addon_description',
										'label'       => __( 'Add-on description', 'apc-addonsproduct' ),
										'description' => __( 'Description that will be shown on frontend', 'apc-addonsproduct' ),
										'value'       => '',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_select(
									array(
										'id'          => 'addon_field_type_0',
										'name'        => 'addon_field_type',
										'label'       => __( 'Field Type', 'apc-addonsproduct' ),
										'description' => __( 'Type of the field shown on frontend', 'apc-addonsproduct' ),
										'options'     => array(
											'text'      => __( 'Text', 'apc-addonsproduct' ),
											'text_area' => __( 'Text Area', 'apc-addonsproduct' ),
											'select'    => __( 'Select', 'apc-addonsproduct' ),
											'radio'     => __( 'Radio', 'apc-addonsproduct' ),
											'checkbox'  => __( 'Checkbox', 'apc-addonsproduct' ),
											'onoff'     => __( 'On/Off', 'apc-addonsproduct' ),
										),
										'value'       => 'text',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_checkbox(
									array(
										'id'          => 'addon_default_0',
										'name'        => 'addon_default',
										'label'       => __( 'Default value', 'apc-addonsproduct' ),
										'description' => __( 'Default value the On/off has', 'apc-addonsproduct' ),
										'value'       => 'yes',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_select(
									array(
										'id'          => 'addon_price_settings_0',
										'name'        => 'addon_price_settings',
										'label'       => __( 'Price Settings', 'apc-addonsproduct' ),
										'description' => __( 'Price can be chosen here', 'apc-addonsproduct' ),
										'options'     => array(
											'free'  => __( 'Free', 'apc-addonsproduct' ),
											'fixed' => __( 'Fixed Price', 'apc-addonsproduct' ),
											'ppc'   => __( 'Price per character', 'apc-addonsproduct' ),
										),
										'value'       => 'free',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_text_input(
									array(
										'id'          => 'addon_price_0',
										'name'        => 'addon_price',
										'label'       => __( 'Price', 'apc-addonsproduct' ),
										'description' => __( 'Price of the Pruchase Note', 'apc-addonsproduct' ),
										'type'        => 'number',
										'value'       => '',
										'desc_tip'    => true,
									)
								);

								woocommerce_wp_text_input(
									array(
										'id'          => 'addon_free_characters_0',
										'name'        => 'addon_free_characters',
										'label'       => __( 'Free characters', 'apc-addonsproduct' ),
										'description' => __( 'Characters that will be free when using Price Per Character option', 'apc-addonsproduct' ),
										'type'        => 'number',
										'value'       => '',
										'desc_tip'    => true,
										'style'       => 'display:none',
									)
								);
							?>
								<div class="form-field addon_option_0_whole p">
									<label class="all_options_label" for="addon_all_options_container_0"> <?php echo( esc_html__( 'Options', 'apc-addonsproduct' ) ); ?></label>
									<div id="addon_all_options_container_0" class="addon_container">
										<div id="addon_option_form_0_0"
											class="addon_option_form" style="display:none">
											<div style="float:left;margin-left:40px;">
												<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
												<input class="addon_option_input" type="text" id="add_option_name_0" name="name">
											</div>
											<div style="float:left;margin-left:40px;">
												<label class="addon_option_label"><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
												<input class="addon_option_input" type="number" id="add_option_price_0" name="price">
											</div>
											<br style="clear:both;" />

											<button class="remove_addon_option" id="remove_addon_option_0" onclick="return false;"> <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>
										</div>
									</div>
									<a class="add_new_addon_option" id="add_new_addon_option_0"><?php echo( esc_html__( 'Add New Option', 'apc-addonsproduct' ) ); ?> </a>
								</div>

								<button id="remove_whole_addon_0" onclick="return false;"> <?php echo( esc_html__( 'Remove Addon', 'apc-addonsproduct' ) ); ?> </button>

							</div>
						</div>	

						<div id="accordion">
						<?php
						// Loop of addons.
						if ( is_array( $all_form ) && ! is_null( $all_form ) ) {
							foreach ( $all_form as $var_index => $var_form ) {
								foreach ( $var_form as $index => $value ) {
									if ( 0 === $var_index ) { // Show only general here.
										?>

									<div id="addon_form_<?php echo esc_html( $index ); ?>">
										<h3 class="addon-ui-accordion-header"><?php echo( esc_html( 'Addon ' . $index ) ); ?>  </h3>
										<div class=content>

										<input type="checkbox" class="addon-switch-enable onoffswitch-checkbox"
											name="<?php echo 'addon_form[' . esc_html( $var_index ) . '][' . esc_html( $index ) . '][addon_enable]'; ?>" 
											id="<?php echo esc_html( 'addon_enable_' . $index ); ?>" 
											<?php
											if ( isset( $value['addon_enable'] ) ) {
												echo 'checked="checked"';
											}
											?>
										/><?php echo esc_html__( ' Enable Addon on Frontend', 'apc-addonsproduct' ); ?>
										<?php
											woocommerce_wp_text_input(
												array(
													'id'   => 'addon_name_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_name]',
													'label' => __( 'Add-on name', 'apc-addonsproduct' ),
													'description' => __( 'Name of this Add-on', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_name'] ) ? '' : $value['addon_name'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_textarea_input(
												array(
													'id'   => 'addon_description_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_description]',
													'label' => __( 'Add-on description', 'apc-addonsproduct' ),
													'description' => __( 'Description that will be shown on frontend', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_description'] ) ? '' : $value['addon_description'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_select(
												array(
													'id'   => 'addon_field_type_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_field_type]',
													'label' => __( 'Field Type', 'apc-addonsproduct' ),
													'description' => __( 'Type of the field shown on frontend', 'apc-addonsproduct' ),
													'options' => array(
														'text' => __( 'Text', 'apc-addonsproduct' ),
														'text_area' => __( 'Text Area', 'apc-addonsproduct' ),
														'select' => __( 'Select', 'apc-addonsproduct' ),
														'radio' => __( 'Radio', 'apc-addonsproduct' ),
														'checkbox' => __( 'Checkbox', 'apc-addonsproduct' ),
														'onoff' => __( 'On/Off', 'apc-addonsproduct' ),
													),
													'value' => empty( $value['addon_field_type'] ) ? 'text' : $value['addon_field_type'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_select(
												array(
													'id'   => 'addon_price_settings_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_price_settings]',
													'label' => __( 'Price Settings', 'apc-addonsproduct' ),
													'description' => __( 'Price can be chosen here', 'apc-addonsproduct' ),
													'options' => array(
														'free' => __( 'Free', 'apc-addonsproduct' ),
														'fixed' => __( 'Fixed Price', 'apc-addonsproduct' ),
														'ppc'  => __( 'Price per character', 'apc-addonsproduct' ),
													),
													'value' => empty( $value['addon_price_settings'] ) ? 'free' : $value['addon_price_settings'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_checkbox(
												array(
													'id'   => 'addon_default_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_default]',
													'label' => __( 'Default value', 'apc-addonsproduct' ),
													'description' => __( 'Default value the On/off has', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_enable'] ) ? 'yes' : $value['addon_enable'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_text_input(
												array(
													'id'   => 'addon_price_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_price]',
													'label' => __( 'Price', 'apc-addonsproduct' ),
													'description' => __( 'Price of the Pruchase Note', 'apc-addonsproduct' ),
													'type' => 'number',
													'value' => empty( $value['addon_price'] ) ? '' : $value['addon_price'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_text_input(
												array(
													'id'   => 'addon_free_characters_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_free_characters]',
													'label' => __( 'Free characters', 'apc-addonsproduct' ),
													'description' => __( 'Characters that will be free when using Price Per Character option', 'apc-addonsproduct' ),
													'type' => 'number',
													'value' => empty( $value['addon_free_characters'] ) ? '' : $value['addon_free_characters'],
													'desc_tip' => true,
												)
											);
										?>
											<div class="form-field addon_option_<?php echo esc_html( $index ); ?>_whole p">
												<label class="all_options_label" for="addon_all_options_container_<?php echo esc_html( $index ); ?>"> <?php echo( esc_html__( 'Options', 'apc-addonsproduct' ) ); ?></label>
												<div  id="addon_all_options_container_<?php echo esc_html( $index ); ?>" class="addon_container">

													<!-- Base template for variant -->
													<div id="addon_option_form_<?php echo( esc_html( $index ) ); ?>"
														class="addon_option_form" style="display:none">
														<div style="float:left;margin-left:40px;">
															<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
															<input class="addon_option_input" type="text" id="add_option_name_<?php echo( esc_html( $index ) ); ?>" name="name">
														</div>
														<div style="float:left;margin-left:40px;">
															<label><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
															<input type="number" id="add_option_price_<?php echo( esc_html( $index ) ); ?>" name="price">
														</div>
														<br style="clear:both;" />
														<button class="remove_addon_option" id="remove_addon_option_<?php echo( esc_html( $index ) ); ?>" onclick="return false;" > <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>
													</div>

												<?php
												// Loop of all options.
												if ( isset( $value['addon_option_form'] ) ) {
													foreach ( $value['addon_option_form'] as $option_index => $option_value ) {
														?>
														<div id="addon_option_form_<?php echo esc_html( $index . '_' . $option_index ); ?>"
															class="addon_option_form"
														>
															<div style="float:left;margin-left:40px;">
																<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
																<input class="addon_option_input" type="text" id="add_option_name_<?php echo esc_html( $index . '_' . $option_index ); ?>" value="<?php echo esc_html( $option_value['name'] ); ?>" name="<?php echo esc_html( 'addon_form[' . $var_index . '][' . $index . '][addon_option_form][' . $option_index . '][name]' ); ?>">
															</div>
															<div style="float:left;margin-left:40px;">
																<label class="addon_option_label"><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
																<input class="addon_option_input" type="number" id="add_option_price_<?php echo esc_html( $index . '_' . $option_index ); ?>" value="<?php echo esc_html( $option_value['price'] ); ?>" name="<?php echo esc_html( 'addon_form[' . $var_index . '][' . $index . '][addon_option_form][' . $option_index . '][price]' ); ?>">
															</div>
															<br style="clear:both;" />

															<button class="remove_addon_option" id="remove_addon_option_<?php echo esc_html( $index . '_' . $option_index ); ?>" onclick="return false;" > <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>

															</div>
														<?php
													}
												}
												?>
												</div>
												<a class="add_new_addon_option" id="add_new_addon_option_<?php echo esc_html( $index ); ?>"><?php echo( esc_html__( 'Add New Option', 'apc-addonsproduct' ) ); ?> </a>
											</p>
											</div>

											<button id="remove_whole_addon_<?php echo esc_html( $index ); ?>" onclick="return false;"> <?php echo( esc_html__( 'Remove Addon', 'apc-addonsproduct' ) ); ?> </button>

										</div>
									</div>
										<?php
									}
								}
							}
						}
						?>

					</div>
					<?php
					echo '<input type="hidden" name="apc_addon_nonce" value="' . esc_attr( wp_create_nonce() ) . '">';
					?>
				</div>
			<?php
		}

		/**
		 * Save all elements after custom tab form.
		 *
		 * @param  mixed $post_id Inline argument.
		 */
		public function save_apc_addons_fields( $post_id ) {

			// Check if our nonce is set.
			if ( ! isset( $_POST['apc_addon_nonce'] ) ) {
				return $post_id;
			}
			$nonce = sanitize_text_field( wp_unslash( $_POST['apc_addon_nonce'] ) );

			// Verify that the nonce is valid.
			if ( ! wp_verify_nonce( $nonce ) ) {
				return $post_id;
			}

			if ( isset( $_POST['addon_form'] ) ) {

				$product = wc_get_product( $post_id );

				// Save all parameters in a array. Sanatizing the POST somehow changes its content.
				$addon_options = $_POST['addon_form'];

				$product->update_meta_data( 'apc_ad_addons', $addon_options );
				$product->save();
			}
		}


		/**
		 * Function to be displayed in variants' tab.
		 *
		 * @param  mixed $loop           Loop in case we need to retrieve.
		 * @param  mixed $variation_data The whole variation data.
		 * @param  mixed $variation      Variation ID.
		 */
		public function display_apc_addon_variants( $loop, $variation_data, $variation ) {
			$variation_id = $variation->ID; // Will be used as ID of the whole_form.

			$product_variation = wc_get_product( $variation->ID );
			$all_form          = $product_variation->get_meta( 'apc_ad_addons' );
			?>

			<button id='new_addon_variant_button_<?php echo esc_html( $variation_id ); ?>' name='new_addon_button' type="button"> <?php echo esc_html__( 'New Add-on', 'apc-addonsproduct' ); ?> </button>

				<!-- We can't copy the base template from general, due to events -->
				<div id="addon_var_form_<?php echo esc_html( $variation_id ); ?>_0" hidden>
					<h3 class="addon-ui-accordion-header"><?php echo( esc_html( 'Addon Base' ) ); ?> </h3>			
					<div class="content">

					<input type="checkbox" class="addon-switch-enable onoffswitch-checkbox"
						name="addon_enable" id="var_addon_enable_0" value="yes"
					/><?php echo esc_html__( ' Enable Addon on Frontend', 'apc-addonsproduct' ); ?>
					<?php
						woocommerce_wp_text_input(
							array(
								'id'          => 'var_addon_name_0',
								'name'        => 'addon_name',
								'label'       => __( 'Add-on name', 'apc-addonsproduct' ),
								'description' => __( 'Name of this Add-on', 'apc-addonsproduct' ),
								'value'       => '',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_textarea_input(
							array(
								'id'          => 'var_addon_description_0',
								'name'        => 'addon_description',
								'label'       => __( 'Add-on description', 'apc-addonsproduct' ),
								'description' => __( 'Description that will be shown on frontend', 'apc-addonsproduct' ),
								'value'       => '',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_select(
							array(
								'id'          => 'var_addon_field_type_0',
								'name'        => 'addon_field_type',
								'label'       => __( 'Field Type', 'apc-addonsproduct' ),
								'description' => __( 'Type of the field shown on frontend', 'apc-addonsproduct' ),
								'options'     => array(
									'text'      => __( 'Text', 'apc-addonsproduct' ),
									'text_area' => __( 'Text Area', 'apc-addonsproduct' ),
									'select'    => __( 'Select', 'apc-addonsproduct' ),
									'radio'     => __( 'Radio', 'apc-addonsproduct' ),
									'checkbox'  => __( 'Checkbox', 'apc-addonsproduct' ),
									'onoff'     => __( 'On/Off', 'apc-addonsproduct' ),
								),
								'value'       => 'text',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_checkbox(
							array(
								'id'          => 'var_addon_default_0',
								'name'        => 'addon_default',
								'label'       => __( 'Default value', 'apc-addonsproduct' ),
								'description' => __( 'Default value the On/off has', 'apc-addonsproduct' ),
								'value'       => 'yes',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_select(
							array(
								'id'          => 'var_addon_price_settings_0',
								'name'        => 'addon_price_settings',
								'label'       => __( 'Price Settings', 'apc-addonsproduct' ),
								'description' => __( 'Price can be chosen here', 'apc-addonsproduct' ),
								'options'     => array(
									'free'  => __( 'Free', 'apc-addonsproduct' ),
									'fixed' => __( 'Fixed Price', 'apc-addonsproduct' ),
									'ppc'   => __( 'Price per character', 'apc-addonsproduct' ),
								),
								'value'       => 'free',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'          => 'var_addon_price_0',
								'name'        => 'addon_price',
								'label'       => __( 'Price', 'apc-addonsproduct' ),
								'description' => __( 'Price of the Pruchase Note', 'apc-addonsproduct' ),
								'type'        => 'number',
								'value'       => '',
								'desc_tip'    => true,
							)
						);

						woocommerce_wp_text_input(
							array(
								'id'          => 'var_addon_free_characters_0',
								'name'        => 'addon_free_characters',
								'label'       => __( 'Free characters', 'apc-addonsproduct' ),
								'description' => __( 'Characters that will be free when using Price Per Character option', 'apc-addonsproduct' ),
								'type'        => 'number',
								'value'       => '',
								'desc_tip'    => true,
								'style'       => 'display:none',
							)
						);
					?>
						<div class="form-field var_addon_option_0_whole p">
							<label class="all_options_label" for="addon_all_options_container_0"> <?php echo( esc_html__( 'Options', 'apc-addonsproduct' ) ); ?></label>
							<div id="var_addon_all_options_container_0" class="addon_container">
								<div id="var_addon_option_form_<?php echo esc_html( $variation_id ); ?>_0"
									class="addon_option_form" style="display:none">	
									
									<div style="float:left;margin-left:40px;">
										<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
										<input class="addon_option_input" type="text" id="var_add_option_name_0" name="name">
									</div>
									<div style="float:left;margin-left:40px;">
										<label class="addon_option_input"><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
										<input class="addon_option_label" type="number" id="var_add_option_price_0" name="price">
									</div>
									<br style="clear:both;" />

									<button class="remove_addon_option" id="var_remove_addon_option_0" onclick="return false;"> <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>
								</div>
							</div>
							<a class="add_new_addon_option" id="var_add_new_addon_option_0"><?php echo( esc_html__( 'Add New Option', 'apc-addonsproduct' ) ); ?> </a>
						</div>

						<button id="var_remove_whole_addon_0" onclick="return false;"> <?php echo( esc_html__( 'Remove Addon', 'apc-addonsproduct' ) ); ?> </button>

					</div>
				</div>	
				<div id="accordion_<?php echo esc_html( $variation_id ); ?>">
				<?php

				// Loop of addons.
				if ( is_array( $all_form ) && ! is_null( $all_form ) ) {
					foreach ( $all_form as $var_index => $var_form ) {
						if ( $variation_id === $var_index ) {
							foreach ( $var_form as $index => $value ) {
								?>

								<div id="addon_var_form_<?php echo esc_html( $variation_id . '_' . $index ); ?>" class="">
									<h3 class="addon-ui-accordion-header"><?php echo( esc_html( 'Addon ' . $variation_id . '_' . $index ) ); ?>  </h3>


									<div class=content>

										<input type="checkbox" class="addon-switch-enable onoffswitch-checkbox"
											name="<?php echo 'addon_form[' . esc_html( $var_index ) . '][' . esc_html( $index ) . '][addon_enable]'; ?>" 
											id="<?php echo esc_html( 'addon_enable_' . $index ); ?>" 
											<?php
											if ( isset( $value['addon_enable'] ) ) {
												echo 'checked="checked"';
											}
											?>
										/><?php echo esc_html__( 'Enable Addon on Frontend', 'apc-addonsproduct' ); ?>

										<?php
											woocommerce_wp_text_input(
												array(
													'id'   => 'var_addon_name_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_name]',
													'label' => __( 'Add-on name', 'apc-addonsproduct' ),
													'description' => __( 'Name of this Add-on', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_name'] ) ? '' : $value['addon_name'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_textarea_input(
												array(
													'id'   => 'var_addon_description_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_description]',
													'label' => __( 'Add-on description', 'apc-addonsproduct' ),
													'description' => __( 'Description that will be shown on frontend', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_description'] ) ? '' : $value['addon_description'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_select(
												array(
													'id'   => 'var_addon_field_type_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_field_type]',
													'label' => __( 'Field Type', 'apc-addonsproduct' ),
													'description' => __( 'Type of the field shown on frontend', 'apc-addonsproduct' ),
													'options' => array(
														'text' => __( 'Text', 'apc-addonsproduct' ),
														'text_area' => __( 'Text Area', 'apc-addonsproduct' ),
														'select' => __( 'Select', 'apc-addonsproduct' ),
														'radio' => __( 'Radio', 'apc-addonsproduct' ),
														'checkbox' => __( 'Checkbox', 'apc-addonsproduct' ),
														'onoff' => __( 'On/Off', 'apc-addonsproduct' ),
													),
													'value' => empty( $value['addon_field_type'] ) ? 'text' : $value['addon_field_type'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_select(
												array(
													'id'   => 'var_addon_price_settings_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_price_settings]',
													'label' => __( 'Price Settings', 'apc-addonsproduct' ),
													'description' => __( 'Price can be chosen here', 'apc-addonsproduct' ),
													'options' => array(
														'free' => __( 'Free', 'apc-addonsproduct' ),
														'fixed' => __( 'Fixed Price', 'apc-addonsproduct' ),
														'ppc'  => __( 'Price per character', 'apc-addonsproduct' ),
													),
													'value' => empty( $value['addon_price_settings'] ) ? 'free' : $value['addon_price_settings'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_checkbox(
												array(
													'id'   => 'var_addon_default_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_default]',
													'label' => __( 'Default value', 'apc-addonsproduct' ),
													'description' => __( 'Default value the On/off has', 'apc-addonsproduct' ),
													'value' => empty( $value['addon_enable'] ) ? 'yes' : $value['addon_enable'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_text_input(
												array(
													'id'   => 'var_addon_price_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_price]',
													'label' => __( 'Price', 'apc-addonsproduct' ),
													'description' => __( 'Price of the Pruchase Note', 'apc-addonsproduct' ),
													'type' => 'number',
													'value' => empty( $value['addon_price'] ) ? '' : $value['addon_price'],
													'desc_tip' => true,
												)
											);

											woocommerce_wp_text_input(
												array(
													'id'   => 'var_addon_free_characters_' . $var_index . '_' . $index,
													'name' => 'addon_form[' . $var_index . '][' . $index . '][addon_free_characters]',
													'label' => __( 'Free characters', 'apc-addonsproduct' ),
													'description' => __( 'Characters that will be free when using Price Per Character option', 'apc-addonsproduct' ),
													'type' => 'number',
													'value' => empty( $value['addon_free_characters'] ) ? '' : $value['addon_free_characters'],
													'desc_tip' => true,
												)
											);
										?>
											<div class="form-field var_addon_option_<?php echo esc_html( $var_index . '_' . $index ); ?>_whole p">
												<label class="all_options_label" for="addon_all_options_container_<?php echo esc_html( $index ); ?>"> <?php echo( esc_html__( 'Options', 'apc-addonsproduct' ) ); ?></label>
												<div id="var_addon_all_options_container_<?php echo esc_html( $var_index . '_' . $index ); ?>"
													class="addon_container">

													<!-- Base template for variant -->
													<div id="var_addon_option_form_<?php echo( esc_html( $var_index . '_' . $index . '_0' ) ); ?>"
														class="addon_option_form" style="display:none" >

														<div style="float:left;margin-left:40px;">
															<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
															<input class="addon_option_input" type="text" id="add_option_name_<?php echo( esc_html( $var_index . '_' . $index . '_0' ) ); ?>" name="name">
														</div>
														<div style="float:left;margin-left:40px;">
															<label><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
															<input type="number" id="add_option_price_<?php echo( esc_html( $var_index . '_' . $index . '_0' ) ); ?>" name="price">
														</div>
														<br style="clear:both;" />
														<button class="remove_addon_option" id="remove_addon_option_<?php echo( esc_html( $var_index . '_' . $index . '_0' ) ); ?>" onclick="return false;"> <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>
													</div>

												<?php
												// Loop of all options.
												if ( isset( $value['addon_option_form'] ) ) {
													foreach ( $value['addon_option_form'] as $option_index => $option_value ) {
														?>
														<div id="var_addon_option_form_<?php echo esc_html( $var_index . '_' . $index . '_' . $option_index ); ?>"
															class="addon_option_form"
														>
															<div style="float:left;margin-left:40px;">
																<label class="addon_option_label"><?php echo( esc_html__( 'Name', 'apc-addonsproduct' ) ); ?></label>
																<input class="addon_option_input" type="text" id="var_add_option_name_<?php echo esc_html( $var_index . '_' . $index . '_' . $option_index ); ?>" value="<?php echo esc_html( $option_value['name'] ); ?>" name="<?php echo esc_html( 'addon_form[' . $var_index . '][' . $index . '][addon_option_form][' . $option_index . '][name]' ); ?>">
															</div>
															<div style="float:left;margin-left:40px;">
																<label class="addon_option_label"><?php echo( esc_html__( 'Price', 'apc-addonsproduct' ) ); ?></label>
																<input class="addon_option_input" type="number" id="var_add_option_price_<?php echo esc_html( $var_index . '_' . $index . '_' . $option_index ); ?>" value="<?php echo esc_html( $option_value['price'] ); ?>" name="<?php echo esc_html( 'addon_form[' . $var_index . '][' . $index . '][addon_option_form][' . $option_index . '][price]' ); ?>">
															</div>
															<br style="clear:both;" />
															<button class="remove_addon_option" id="var_remove_addon_option_<?php echo esc_html( $var_index . '_' . $index . '_' . $option_index ); ?>" onclick="return false;"> <?php echo( esc_html__( 'Remove Option', 'apc-addonsproduct' ) ); ?> </button>

														</div>
														<?php
													}
												}
												?>
												</div>
												<a class="add_new_addon_option" id="var_add_new_addon_option_<?php echo esc_html( $var_index . '_' . $index ); ?>"> <?php echo( esc_html__( 'Add New Option', 'apc-addonsproduct' ) ); ?> </a>
											</p>
											</div>

											<button id="var_remove_whole_addon_<?php echo esc_html( $var_index . '_' . $index ); ?>" onclick="return false;"> <?php echo( esc_html__( 'Remove Addon', 'apc-addonsproduct' ) ); ?> </button>
									</div>
								</div>
								<?php
							}
						}
					}
				}

				?>
				<?php
				echo '<input type="hidden" name="apc_addon_nonce" value="' . esc_attr( wp_create_nonce() ) . '">';
				?>
				</div>
			<?php
		}

		/**
		 * Save all data from display_variatns into the DB.
		 *
		 * @param  mixed $variation Variation given by hook.
		 */
		public function save_apc_addon_variants( $variation ) {

			// Check if our nonce is set.
			if ( ! isset( $_POST['apc_addon_nonce'] ) ) {
				return $post_id;
			}
			$nonce = sanitize_text_field( wp_unslash( $_POST['apc_addon_nonce'] ) );

			// Verify that the nonce is valid.
			if ( ! wp_verify_nonce( $nonce ) ) {
				return $post_id;
			}

			if ( isset( $_POST['addon_form'] ) ) {

				// Save all parameters in a array. Sanatizing it somehow changes the content.
				$addon_options = $_POST['addon_form'];

				$variation->update_meta_data( 'apc_ad_addons', $addon_options );
				$variation->save();
			}
		}

	}
}
