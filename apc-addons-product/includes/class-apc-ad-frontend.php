<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'APC_AD_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_AD_Frontend' ) ) {
	/**
	 * Class with functions used in frontend
	 */
	class APC_AD_Frontend {

		/**
		 * Main Instance
		 *
		 * @var object Instance of the class to be called in get_instance
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Main plugin Instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Frotend constructor.
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', 'addon_wp_enqueue_scripts' );
			add_action( 'woocommerce_before_add_to_cart_button', 'apc_addon_show_on_cart', 10, 2 );
			add_filter( 'woocommerce_add_cart_item_data', 'addon_add_cart_item_data', 10, 4 );
			add_filter( 'woocommerce_new_order_item', 'apc_ad_save_cart_session', 10, 3 );
			add_filter( 'woocommerce_get_item_data', 'apc_ad_add_order_item_meta', 10, 2 );
			add_action( 'woocommerce_add_order_item_meta', 'apc_ad_save_metadata', 10, 3 );
			add_action( 'woocommerce_before_calculate_totals', 'apc_ad_recalculate_price' );

			/** VARIATION ACTIONS */
			// Hook for no-logged users.
			add_action( 'wp_ajax_nopriv_add_addons_variation', 'apc_addon_on_change_variant' );

			// Hook for logged users.
			add_action( 'wp_ajax_add_addons_variation', 'apc_addon_on_change_variant' );
		}
	}

	/** CSS Register Function for FrontEnd */
	function addon_wp_enqueue_scripts() {
		$css_url  = APC_AD_DIR_ASSETS_CSS_URL . '/apc_ad.css';
		$ajax_url = APC_AD_DIR_ASSETS_JS_URL . '/apc_addon_ajax.js';
		$js_url   = APC_AD_DIR_ASSETS_JS_URL . '/apc_ad.js';

		wp_register_style( 'apc-ad-style', $css_url );
		wp_enqueue_style( 'apc-ad-style' );

		// Add Ajax here.
		wp_register_script( 'apc-ajax-js', $ajax_url, array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'apc-ajax-js' );
		wp_localize_script( 'apc-ajax-js', 'apc_ad_addon_vars', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		wp_register_script( 'apc-ad-js', $js_url, array(), '1.0', true );
		wp_enqueue_script( 'apc-ad-js' );

	}

	/**
	 * Basic shortcode to show testimonials
	 *
	 * @param mixed $atts Attributes or parameters of the shortcode, look at array below for possible arguments.
	 * @param mixed $content Show additional content.
	 */
	function apc_addon_show_on_cart( $atts, $content = array() ) {

		// Get Meta Options.
		global $post;

		$product = wc_get_product( $post->ID );

		// First get_elements.
		$atts = $product->get_meta( 'apc_ad_addons' );

		?>
		<div class="apc-addon-main-container">

		<p id="addon_loading_ajax" hidden><?php echo( esc_html__( 'Loading...', 'apc-addonsproduct' ) ); ?> </p>
		<?php 
		// Whole Loop of Addon here.
		if ( is_array( $atts ) || is_object( $atts ) ) {
			foreach ( $atts as $variation_id => $list_of_addons) {
				if ( ( 0 === $variation_id ) ) {

					foreach ( $list_of_addons as $addon_index => $addon ) {
						if ( isset( $addon['addon_enable'] ) ) {

							// For JS change.
							$id_input   = 'addon_input_' . $variation_id . '_' . $addon_index;
							$name_input = 'addon_input[' . $variation_id . '][' . $addon_index . ']'; // To be in array.
							$id_cost    = 'addon_cost_' . $variation_id . '_' . $addon_index;

							?>
							<div class="addon_whole_box" style="">
								<p class="addon_title"> <?php echo esc_html( $addon['addon_name'] ); ?> </p>
								<?php

								switch ( $addon['addon_field_type'] ) {
									case 'text':
										?>
										<p class="purchasenote_description"> <?php echo esc_html( $addon['addon_description'] ); ?> </p>

										<input id="<?php echo esc_html( $id_input ); ?>" class="apc_addon_input" type="text" 
											name="<?php echo esc_html( $name_input ); ?>"
											data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
											data-price_settings="<?php echo esc_html( $addon['addon_price_settings'] ); ?>"
											data-free_characters="<?php echo esc_html( $addon['addon_free_characters'] ); ?>"
											data-price="<?php echo esc_html( $addon['addon_price'] ); ?>"
										></input>
										<?php
										break;
									case 'text_area':
										?>
										<p class="purchasenote_description"> <?php echo esc_html( $addon['addon_description'] ); ?> </p>

										<textarea id="<?php echo esc_html( $id_input ); ?>" class="apc_addon_input" 
											name="<?php echo esc_html( $name_input ); ?>"
											data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
											data-price_settings="<?php echo esc_html( $addon['addon_price_settings'] ); ?>"
											data-free_characters="<?php echo esc_html( $addon['addon_free_characters'] ); ?>"
											data-price="<?php echo esc_html( $addon['addon_price'] ); ?>"
										></textarea>
										<?php
										break;
									case 'select':
										// Loop here.
										?>
										<p class="purchasenote_description"> <?php echo esc_html( $addon['addon_description'] ); ?> </p>
										<?php
										if ( isset( $addon['addon_option_form'] ) ) {
											?>
											<select  id="<?php echo esc_html( $id_input ); ?>" 
												class="apc_addon_input" name="<?php echo esc_html( $name_input ); ?>"
												data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
											> 
											<?php
											foreach ( $addon['addon_option_form'] as $option_index => $option_value ) {
												?>
												<option data-price="<?php echo esc_html( $option_value['price'] ); ?>" 
													value="option_<?php echo esc_html( $option_index ); ?>"
													id="option_<?php echo esc_html( $variation_id . '_' . $addon_index . '_' . $option_index ); ?>"
												><?php echo esc_html( $option_value['name'] ); ?></option>
												<?php
											}
										}
										?>
										</select>
										<?php
										break;
									case 'radio':
										// Loop here.
										?>
										<p class="purchasenote_description"> <?php echo esc_html( $addon['addon_description'] ); ?> </p>

										<div> 
										<?php
										if ( isset( $addon['addon_option_form'] ) ) {
											foreach ( $addon['addon_option_form'] as $option_index => $option_value ) {
												?>
												<label for="option_<?php echo esc_html( $variation_id . '_' . $addon_index ); ?>">
													<input type="radio" class="apc_addon_input" 
														data-price="<?php echo esc_html( $option_value['price'] ); ?>"
														data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
														id="option_<?php echo esc_html( $variation_id . '_' . $addon_index ); ?>" 
														name="<?php echo esc_html( $name_input ); ?>"
														value="option_<?php echo esc_html( $option_index ); ?>"
													>
												<?php echo esc_html( $option_value['name'] ); ?></label>
												<?php
											}
										}
										?>
										</div>
										<?php
										break;
									case 'checkbox':
										?>
										<label for="<?php echo esc_html( $id_input ); ?>" class="purchasenote_description">
										
										<input type="checkbox" <?php echo ( isset( $addon['addon_default'] ) ) ? esc_html( 'checked="checked"' ) : ''; ?> 
											id="<?php echo esc_html( $id_input ); ?>" class="apc_addon_input" 
											name="<?php echo esc_html( $name_input ); ?>"
											data-price="<?php echo esc_html( $addon['addon_price'] ); ?>"
											data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
										/>
										<?php echo esc_html( $addon['addon_description'] ); ?></label>
										<?php
										break;
									case 'onoff':
										?>
										<label for="<?php echo esc_html( $id_input ); ?>" class="purchasenote_description">
										
										<input type="checkbox" <?php echo ( isset( $addon['addon_default'] ) ) ? esc_html( 'checked="checked"' ) : ''; ?> 
											id="<?php echo esc_html( $id_input ); ?>" class="apc_addon_input" 
											name="<?php echo esc_html( $name_input ); ?>"
											data-price="<?php echo esc_html( $addon['addon_price'] ); ?>"
											data-field_type="<?php echo esc_html( $addon['addon_field_type'] ); ?>"
										/>
										<?php echo esc_html( $addon['addon_description'] ); ?></label>

										<?php
										break;
								}
								?>

								<label id="<?php echo esc_html( $id_cost ); ?>" class="addon_cost"> + 0 € </label>
							</div>

							<?php
						}
					}
				}
			}
		}
		?>
		</div>
		<div class="apc-addon-variant-container"></div>
		<div class="apc-total-price-container">

		</div>
			<h4> <?php echo( esc_html__( 'Price totals:', 'apc-addonsproduct' ) ); ?> </h4>
			<div class="apc-product-base-price">
				<p> <?php echo( esc_html__( 'Product price:', 'apc-addonsproduct' ) ); ?> </p>
				<label id="apc_base_price_label" class="apc_price_format"><?php echo esc_html( $product->get_price() . '€' ); ?></label>
			</div>

			<div class="apc-product-addon-price">
				<p> <?php echo( esc_html__( 'Additional Options total:', 'apc-addonsproduct' ) ); ?> </p>
				<label id="apc_addon_price_label" class="apc_price_format"> </label>
			</div>

			<div class="apc-product-total-price">
				<p> <?php echo( esc_html__( 'Total:', 'apc-addonsproduct' ) ); ?> </p>
				<label id="apc_addon_total_label" class="apc_price_format"> </label>
			</div>
		</div>
		<?php
	}


	/**
	 * Doing the calculations and store arguments for the cart will be done here.
	 *
	 * @param  mixed $cart_item_data Cart arguments to be stored.
	 * @param  mixed $product_id     Related to an Product ID.
	 * @param  mixed $variation_id   ID of the variation.
	 * @param  mixed $quantity       Quantity, just in case.
	 */
	function addon_add_cart_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {

		$whole_form = get_post_meta( $product_id, 'apc_ad_addons', true );
		$var_form   = get_post_meta( $variation_id, 'apc_ad_addons', true );

		// Loop of all addons here. Regardless of the variation.
		$cart_index = 1;

		error_log (print_r ($whole_form, true) );
		error_log (print_r ($_POST, true));

		if ( isset( $_POST['addon_input'] ) ) {
			foreach ( $_POST['addon_input'] as $var_index => $var_value ) {
				foreach ( $var_value as $addon_index => $addon_value ) {

					if ($var_index === 0) {
						$addon_form = $whole_form[ $var_index ][ $addon_index ];
					} else {
						$addon_form = $var_form[ $var_index ][ $addon_index ];
					}

					$addon_field_type = $addon_form['addon_field_type'];
					$addon_price      = $addon_form['addon_price'];

					$cost = ! empty( $addon_price ) ? $addon_price : 0;

					/* Do the price magic trick here */
					if ( ( 'text' === $addon_field_type ) || ( 'text_area' === $addon_field_type ) ) {

						$addon_price_settings  = $addon_form['addon_price_settings'];
						$addon_free_characters = ! empty( $addon_form['addon_free_characters'] ) ? $addon_form['addon_free_characters'] : 0;

						$cost = 0;
						if ( 'fixed' === $addon_price_settings ) {
							$cost = intval( $note_price );
						} elseif ( 'ppc' === $addon_price_settings ) {
							$cost = ( strlen( $addon_value ) < $addon_free_characters ) ? 0 : $addon_price * ( strlen( $addon_value ) - $addon_free_characters );
						}
					} elseif ( ( 'select' === $addon_field_type ) || ( 'radio' === $addon_field_type ) ) {
						$addon_option_list = $addon_form['addon_option_form'];

						// Get index of option_X.
						preg_match_all( '!\d+!', $_POST['addon_input'][ $var_index ][ $addon_index ], $matches );
						$option_index = implode( ' ', $matches[0] );

						$cost = $addon_option_list[ $option_index ]['price'];
					}

					$cart_item_data['addon'][ $cart_index ]['price_addon']   = $cost;
					$cart_item_data['addon'][ $cart_index ]['content_addon'] = sanitize_text_field( wp_unslash( $_POST['addon_input'][ $var_index ][ $addon_index ] ) );

					$cart_index++;
				}
			}
		}
		return $cart_item_data;
	}

		/**
		 * Save cart used in the section into real cart data.
		 *
		 * @param  mixed $cart_item_data Real cart.
		 * @param  mixed $cart_item_session_data Session cart.
		 * @param  mixed $cart_item_key To check nonce.
		 */
	function apc_ad_save_cart_session( $cart_item_data, $cart_item_session_data, $cart_item_key ) {

		foreach ( $cart_item_data as $index => $content ) {
			if ( isset( $content['price_addon'] ) ) {
				$cart_item_data['addon'][ $index ]['price_addon'] = $cart_item_session_data['addon'][ $index ]['price_addon'];
			}

			if ( isset( $content['content_addon'] ) ) {
				$cart_item_data['addon'][ $index ]['content_addon'] = $cart_item_session_data['addon'][ $index ]['content_addon'];
			}
		}
		return $cart_item_data;
	}

	/**
	 * Show note on cart
	 *
	 * @param  mixed $data      Data to be showed/returned.
	 * @param  mixed $cart_item Object where we saved our data before.
	 */
	function apc_ad_add_order_item_meta( $data, $cart_item ) {

		if ( isset( $cart_item['addon'] ) ) {
			$data = array();

			foreach ( $cart_item['addon'] as $addon_index => $addon_value ) {
				
				if ( ! empty( $addon_value['content_addon'] ) ) {
					$data[ $addon_index ] = array(
						'name'  => __( 'Addon ', 'apc-addonsproduct' ) . $addon_index,
						'value' => $addon_value['content_addon'] . ' (' . $addon_value['price_addon'] . '€)',
					);
				}
			}
		}
		return $data;
	}

	/**
	 * Save metadata from cart into db.
	 *
	 * @param  mixed $item_id Item Id.
	 * @param  mixed $values Values retrieved from cart.
	 * @param  mixed $key Key from using nonce.
	 */
	function apc_ad_save_metadata( $item_id, $values, $key ) {

		if ( isset( $values['addon'] ) ) {
			foreach ( $values['addon'] as $addon_index => $addon_value ) {

					// Don't we include empty data?
					$save_data = $addon_value['content_addon'] . ' (' . $addon_value['price_addon'] . '€)';
					// As a test we include both fields into a single metadata.
					wc_add_order_item_meta( $item_id, 'Addon ' . $addon_index, $save_data );
			}
		}
	}

	/**
	 * Function to add the note price on cart items.
	 *
	 * @param  mixed $cart_object The cart to be querying on.
	 */
	function apc_ad_recalculate_price( $cart_object ) {
		foreach ( $cart_object->get_cart() as $hash => $value ) {
			if ( isset( $value['addon'] ) ) {	
				$addon_total_cost = 0;

				foreach ( $value['addon'] as $addon_index => $addon_value ) {
					$addon_total_cost += $addon_value['price_addon'];
				}

				$newprice = $value['data']->get_price() + $addon_total_cost;
				$value['data']->set_price( $newprice );
			}
		}
	}


	/** VARIATION CODE */

	/**
	 * Function that processes the AJAX call
	 *
	 * @return void
	 */
	function apc_addon_on_change_variant() {
		// Send and create whole HTML.
		$variant_id = $_POST['variation_id']['variation_id'];

		$result = apc_addon_variant_on_cart( array(), $variant_id );
		wp_send_json( array( 'message' => $result ) );
	}

	/**
	 * A variant of the show HTML in, but with all HTML stored in a variable instead of echo, to be sent to AJAX.
	 *
	 * @param  mixed $atts  Whole form to be get from meta.
	 * @param  mixed $current_var Variation ID.
	 */
	function apc_addon_variant_on_cart( $atts, $current_var = 0 ) {

		// Get Meta Options.
		global $post;

		$product = wc_get_product( $current_var );

		// First get_elements.
		$atts = $product->get_meta( 'apc_ad_addons' );

		// Output html.
		$html_returned = '';

		// Whole Loop of Addon here.
		if ( is_array( $atts ) || is_object( $atts ) ) {
			foreach ( $atts as $variation_id => $list_of_addons ) {
				if ( $variation_id == $current_var ) {
					foreach ( $list_of_addons as $addon_index => $addon ) {
						if ( isset( $addon['addon_enable'] ) ) {
							// Get cost from calculations.
							$cost = 0;
							if ( 'fixed' === $addon['addon_price_settings'] ) {
								$cost = intval( $addon['addon_price'] );
							}

							// For JS change.
							$id_input   = 'addon_input_' . $variation_id . '_' . $addon_index;
							$name_input = 'addon_input[' . $variation_id . '][' . $addon_index . ']'; // To be in array.
							$id_cost    = 'addon_cost_' . $variation_id . '_' . $addon_index;

							$html_returned .= ' 
								<div class="purchasenote_box" style="">
									<p class="addon_title">' . $addon['addon_name'] . ' </p>';

							switch ( $addon['addon_field_type'] ) {
								case 'text':
									$html_returned .= '<p class="purchasenote_description">' . $addon['addon_description'] . '</p>';
									$html_returned .= '<input id="' . $id_input . '" class="apc_addon_input" type="text" name="' . $name_input . '"
											data-field_type="' . $addon['addon_field_type'] . '"
											data-price_settings="' . $addon['addon_price_settings'] . '"
											data-free_characters="' . $addon['addon_free_characters'] . '"
											data-price="' . $addon['addon_price'] . '"></input>';
									break;
								case 'text_area':
									$html_returned .= '<p class="purchasenote_description">' . $addon['addon_description'] . '</p>';
									$html_returned .= '<textarea id="' . $id_input . '" name="' . $name_input . '" class="apc_addon_input" 
										data-field_type="' . $addon['addon_field_type'] . '"
										data-price_settings="' . $addon['addon_price_settings'] . '"
										data-free_characters="' . $addon['addon_free_characters'] . '"
										data-price="' . $addon['addon_price'] . '">
									</textarea>';
									break;

								case 'select':
									$html_returned .= '<p class="purchasenote_description">' . $addon['addon_description'] . '</p>';
									// Loop here.
									if ( isset( $addon['addon_option_form'] ) ) {
										$html_returned .= '<select  id="' . $id_input . '" class="apc_addon_input" name="' . $name_input . '" 
											data-field_type="' . $addon['addon_field_type'] . '">';
										foreach ( $addon['addon_option_form'] as $option_index => $option_value ) {
											$html_returned .= '<option data-price="' . $option_value['price'] .
												'" value="option_' . $option_index . '"
												id="option_' . $variation_id . '_' . $addon_index . '_' . $option_index  . '">' . $option_value['name'] . '</option>';
										}
									}
									$html_returned .= '</select>';
									break;

								case 'radio':
									$html_returned .= '<p class="purchasenote_description">' . $addon['addon_description'] . '</p>';
									// Loop here.
									$html_returned .= '<div>';
									if ( isset( $addon['addon_option_form'] ) ) {
										foreach ( $addon['addon_option_form'] as $option_index => $option_value ) {
											$html_returned .= '<label for="option_' . $variation_id . '_' . $addon_index . '">';
											$html_returned .= '<input type="radio" data-price="' . $option_value['price'] . '" 
												id="option_' . $variation_id . '_' . $addon_index . '" 
												name="' . $name_input . '"
												value="option_' . $option_index . '"
												class="apc_addon_input"
												data-price="' . $option_value['price'] . '"
												data-field_type="' . $addon['addon_field_type'] .'"
												>';
											$html_returned .= $option_value['name'] . '</label>';
										}
									}
									$html_returned .= '</div>';
									break;

								case 'checkbox':
									$html_returned .= '<label class="purchasenote_description">';
									$html_returned .= '<input type="checkbox" ';
									if ( isset( $addon['addon_default'] ) ) {
										$html_returned .= ' checked="checked" ';
									}
									$html_returned .= 'id="' . $id_input . '"  name="' . $name_input . '" class="apc_addon_input"
										data-price="' . $addon['addon_price'] . '"
										data-field_type="' . $addon['addon_field_type'] . '"
									>';
									$html_returned .= $addon['addon_description'] . '</label>';
									break;

								case 'onoff':
									$html_returned .= '<label class="purchasenote_description">';
									$html_returned .= '<input type="checkbox" ';
									if ( isset( $addon['addon_default'] ) ) {
										$html_returned .= ' checked="checked" ';
									}
									$html_returned .= ' id="' . $id_input . '" name="' . $name_input . '" class="apc_addon_input"
										data-price="' . $addon['addon_price'] . '"
										data-field_type="' . $addon['addon_field_type'] . '"
									>';
									$html_returned .= $addon['addon_description'] . '</label>';
									break;
							}

							$html_returned .= '<label id="' . $id_cost . '" class="addon_cost"> + ' . $cost . ' € </label>
							</div>';
						}
					}
				}
			}
		}
		return $html_returned;
	}
}
