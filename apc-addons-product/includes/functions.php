<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! function_exists( 'addons_admin_enqueue_scripts' ) ) {
	/** CSS and JS Register Function for Admin */
	function addons_admin_enqueue_scripts() {
		$js_url  = APC_AD_DIR_ASSETS_JS_URL . '/apc_ad_admin.js';
		$css_url = APC_AD_DIR_ASSETS_CSS_URL . '/apc_ad_admin.css';

		wp_register_style( 'apc-ad-admin-style', $css_url );
		wp_enqueue_style( 'apc-ad-admin-style' );

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'apc-ad-admin-js', $js_url, array( 'wp-color-picker', 'jquery', 'jquery-ui-core', 'jquery-ui-accordion' ), true, true );
		wp_localize_script( 'apc-ad-admin-js', 'apc-ad-admin-vars', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
}
add_action( 'admin_enqueue_scripts', 'addons_admin_enqueue_scripts' );
?>
