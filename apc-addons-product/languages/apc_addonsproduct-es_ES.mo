��             +         �      �     �     �       B        [     d     r  *   �  
   �     �     �     �  
   �     �     �  
   
               $     *     9     R     m     �     �     �     �     �  	   �  #   �  �  �  !   �     �     �     �  O   �     ?     F  '   X  .   �     �     �     �     �     �     �     �               !     *     1     D     d     v     �     �  
   �  ,   �     �     �  %   �                                                     
                                                                          	                 APC Addons for Frontend Products Add New Option Add-on description Add-on name Characters that will be free when using Price Per Character option Checkbox Default value Default value the On/off has Description that will be shown on frontend Field Type Fixed Price Free Free characters Loading... Name Name of this Add-on New Add-on On/Off Options Price Price Settings Price can be chosen here Price of the Pruchase Note Price per character Radio Remove Option Select Test Plugins for Product Addons Text Text Area Type of the field shown on frontend Project-Id-Version: APC Addons for Frontend Products 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/apc-addons-product
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-01-18 12:10+0100
X-Generator: Poedit 2.4.2
X-Domain: apc-addonsproduct
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Addons para Productos en Frontend Añadir nueva opción Descripción del Add-on Nombre de Add-on Número de caracteres de gratis cuando se usa la opción "Precio por carácter" Botón Valor por defecto Valor por defecto en switches o botones Descripción que será mostrada en el Frontend Tipo de campo Precio fijo Gratis Caracteres gratis Cargando... Nombre Nombre para este Add-on Nuevo Add-on Switch Opciones Precio Opciones de precio El precio puede cambiarse aquí Precio del Add-on Precio por carácter Radio Quitar opción Selección Plugins de prueba para Productos en Frontend Texto Zona de Texto Tipo de campo mostrado en el Frontend 