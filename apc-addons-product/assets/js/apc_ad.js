
function addon_change_inputs() {

  console.log ("PULSADO");
  var idArray = jQuery(this).attr("id").split("_");
  var num = idArray[idArray.length-1];
  var variation_id = idArray[idArray.length-2];

  var cost = 0;
  var field_type = jQuery(this).attr('data-field_type')

  if (field_type == 'text' || field_type == 'text_area') {

    var price_settings = jQuery(this).attr('data-price_settings')
    var price = jQuery(this).attr('data-price');

    if ('fixed' === price_settings) {
      cost = parseInt( jQuery(this).attr("data-price") );
    }
    
    else if ('ppc' === price_settings ) {
      var free_characters = jQuery(this).attr('data-free_characters');
      var value = jQuery(this).val();
      
      cost = ( value.length < free_characters ) ? 0.00 : price * ( value.length - free_characters );
    }
    
  } else if (field_type == 'select' ) {
    
    var option_index = parseInt( jQuery(this).prop("value").match(/\d+/g), 10 );
    cost = parseInt( jQuery('#option_' + variation_id + '_' + num + '_' + option_index ).attr("data-price") );

    console.log("COSTE " + cost + " " + '#option_' + variation_id + '_' + num + '_' + option_index );
  
  } else if (field_type == 'radio') {
    cost = parseInt( jQuery(this).attr('data-price') );
  } else {
    cost = jQuery(this).prop('checked') ? parseInt( jQuery(this).attr('data-price') ) : 0;
  }

  // Change price
  jQuery("#addon_cost_" + variation_id + "_" + num ).html( " + " + cost + " € " );

  //Then, change price of addon

  var addon_cost = 0;
  jQuery('.addon_cost').each(function(){
    var results = parseFloat( jQuery(this).html().match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0] );
    addon_cost += results;
  });

  if ( isNaN(addon_cost) ) addon_cost = 0; // Segurity code.
  jQuery ('#apc_addon_price_label').html( addon_cost + '€')
  
  var product_price = parseFloat( jQuery('#apc_base_price_label').html().match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0] );
  var total_cost = product_price + addon_cost;

  jQuery ('#apc_addon_total_label').html( total_cost + '€')
}


jQuery(".apc_addon_input").on('change', addon_change_inputs);

jQuery ('.apc_addon_input').each(function () {
  jQuery(this).change()
});


jQuery(document).on('found_variation', function(event, variation) {

  /*
      $('select[id^="pa_"]').each(function(){
      });
  */
  
       //La llamada AJAX
       $.ajax({
          type : "post",
          url : apc_ad_addon_vars.ajax_url,
  
          data : {
              action: "add_addons_variation", 
              message : "Button has been clicked!",
              variation_id: variation
          },
  
          beforeSend: function (qXHR, settings) {
              $('p#addon_loading_ajax').fadeIn();
          },
          complete: function () {
              $('p#addon_loading_ajax').fadeOut();

              $('div.apc-addon-variant-container').find(".apc_addon_input").each( function($){
                jQuery(this).bind('change', addon_change_inputs);
              });

              jQuery ('.apc_addon_input').each(function () {
                jQuery(this).change()
              });
          },
  
          success: function(response) {

              $('div.apc-addon-variant-container').empty().append( $.parseHTML( response.message )).fadeIn();

          }
      })
  });