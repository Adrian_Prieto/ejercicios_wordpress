jQuery(document).ready(function($){
  $('.ga-tst-form__input-badge_color__input').wpColorPicker();
});

jQuery( function($) {
  var icons = {
    header: "ui-icon-circle-arrow-e",
    activeHeader: "ui-icon-circle-arrow-s"
  };

  $("#accordion" ).accordion({
    icons: icons,
    collapsible: true,
    heightStyle: "content",
    header: "> div > h3"
  })
  .sortable({
    axis: "y",
    handle: "h3",
    stop: function( event, ui ) {
      // IE doesn't register the blur when sorting
      // so trigger focusout handlers to remove ui-state-focus
      ui.item.children( "h3" ).triggerHandler( "focusout" );
      // Refresh accordion to handle new order
      $( this ).accordion( "refresh" );
    }
  });
});

jQuery ('[id^="addon_field_type_"]').on('change', (function ($) {
  var num = parseInt( jQuery(this).prop("id").match(/\d+/g), 10 );

  jQuery(".addon_price_settings_" + num + "_field").show();
  jQuery(".addon_default_" + num + "_field").show();
  jQuery(".addon_price_" + num + "_field").show();
  jQuery(".addon_free_characters_" + num + "_field").show();
  jQuery(".addon_option_" + num + "_whole").show();

  if ( ( (jQuery(this).val() == 'text' ) || (jQuery(this).val() == 'text_area' ) ) )
  {
    // Hide Price Settings
    jQuery(".addon_default_" + num + "_field").hide();
    jQuery(".addon_option_" + num + "_whole").hide();

    if ( jQuery(`#addon_price_settings_${num}`).val() == 'free' ) {
      //Hide Price
      jQuery(".addon_price_" + num + "_field").hide();
      jQuery(".addon_free_characters_" + num + "_field").hide();
    }
  }
  if ( (jQuery(this).val() == 'select' ) || (jQuery(this).val() == 'radio' ) )
  {
    jQuery(".addon_price_settings_" + num + "_field").hide();
    jQuery(".addon_default_" + num + "_field").hide();
    jQuery(".addon_price_" + num + "_field").hide();
    jQuery(".addon_free_characters_" + num + "_field").hide();
  }
  if ( (jQuery(this).val() == 'checkbox' ) || (jQuery(this).val() == 'onoff' ) )
  {
    // Hide Default
    jQuery(".addon_price_settings_" + num + "_field").hide();
    jQuery(".addon_free_characters_" + num + "_field").hide();
    jQuery(".addon_option_" + num + "_whole").hide();
  }
}));

jQuery ('[id^="addon_price_settings_"]').on('change', (function ($) {

  var num = parseInt( jQuery(this).prop("id").match(/\d+/g), 10 );

  jQuery(".addon_price_" + num + "_field").show();
  jQuery(".addon_free_characters_" + num + "_field").show();

  if ( jQuery(this).val() == 'free' ) {
    //Hide Price
    jQuery(".addon_price_" + num + "_field").hide();
    jQuery(".addon_free_characters_" + num + "_field").hide();
  }
}));


jQuery ('[id^="add_new_addon_option_"]').on('click', (function($){



  //Current addon's number
  var idArray = jQuery(this).attr("id").split("_");
  var num = idArray[idArray.length-1];
  
  var $base_div = jQuery('div[id^="addon_option_form_0_0"');  
  var $last_div = jQuery('div[id^="addon_option_form_' + num + '"]:last');

  // Read the Number from that DIV's ID and increment that number by 1
  var idArray = jQuery($last_div).attr("id").split("_");
  var index = ($last_div.length) ? parseInt(idArray[idArray.length -1]) + 1 : 1;

  // Clone it and assign the new ID
  var $new_div = $base_div.clone(true, true).prop('id', 'addon_option_form_' + num + "_" + index ).show();

  // Loop en todos los elementos
  jQuery ($new_div).find("*[id]").each(function(){    
    // Sustituir todas las Ids 
    jQuery(this).val('');
    var tID = jQuery(this).attr("id");
    var idArray = tID.split("_");
    var idArrayLength = idArray.length;
    var newId = tID.replace(idArray[idArrayLength-1], num + "_" + index);
/*
    var tNAME = jQuery(this).attr("name");
    jQuery(this).attr("name", `addon_form[0][${index}][addon_option_form][${num}][${tNAME}]` )
*/  
    jQuery(this).attr('id', newId);
  });

  //Sustituir todos los nombres para POST
  jQuery($new_div).find("input[id*='add_option_name']").each(function(){
    jQuery(this).attr("name", `addon_form[0][${num}][addon_option_form][${index}][name]` )
  });
  jQuery($new_div).find("input[id*='add_option_price']").each(function(){
    jQuery(this).attr("name", `addon_form[0][${num}][addon_option_form][${index}][price]` )
  });


  jQuery ('#addon_all_options_container_' + num ).append($new_div);
}));

jQuery( "#new_addon_button" ).on('click', (function($) {

  // get the last DIV which ID
  var $base_div = jQuery('div[id^="addon_form"]:first');
  var $last_div = jQuery('div[id^="addon_form"]:last');

  // Read the Number from that DIV's ID and increment that number by 1
  var num = parseInt( $last_div.prop("id").match(/\d+/g), 10 ) +1;

  // Clone it and assign the new ID
  var $new_div = $base_div.clone(true, true).prop('id', 'addon_form_'+ num ).show();

  //Sustituir titulo
  var $new_title = jQuery($new_div).find("h3").html( "Addon " + num );

  // Loop en todos los elementos
  jQuery ($new_div).find("*[id]").each(function(){    
    // Sustituir todas las Ids 
    jQuery(this).val('');
    var tID = jQuery(this).attr("id");
    var idArray = tID.split("_");
    var idArrayLength = idArray.length;
    var newId = tID.replace(idArray[idArrayLength-1], num);

    //Sustituir todos los nombres para POST
    var tNAME = jQuery(this).attr("name");

    if (tNAME !== undefined) {
      jQuery(this).attr("name", "addon_form[0][" + num + "][" + tNAME+ "]" )
    }
    
    jQuery(this).attr('id', newId);
  });

  //Copy default values of selects
  var $origSelects = jQuery('select', $base_div);
  var $clonedSelects = jQuery('select', $new_div);
  $origSelects.each(function(i) {
    $clonedSelects.eq(i).val( jQuery(this).val());
  });

  //Sustituir fields también
  jQuery ($new_div).find("[class*='form-field']").each(function(){
    var tCLASS = jQuery(this).attr("class").replace(/\d+/, num);
    jQuery(this).attr("class", tCLASS);
  });

  jQuery($new_div).find("span[class*='addon_all_options_container']").attr("class", 'addon_all_options_container_' + num);

  jQuery($new_div).removeAttr('hidden').show();

  jQuery ('#accordion').append($new_div).accordion("refresh");

}));


jQuery ('[id^="addon_field_type_"]').each(function () {
  jQuery(this).change()
});

jQuery ('[id^="addon_price_settings_"]').each(function () {
  jQuery(this).change()
});

jQuery ('[id^="remove_addon_option_"]').on('click', (function($) {
  var idArray = jQuery(this).attr("id").split("_");
  var index = idArray[idArray.length-1];
  var num = idArray[idArray.length-2];

  jQuery('#addon_option_form_' + num + '_' + index ).empty().remove();
}));

jQuery ('[id^="remove_whole_addon_"]').on('click', (function($) {
  var idArray = jQuery(this).attr("id").split("_");
  var num = idArray[idArray.length-1];

  jQuery('#addon_form_' + num ).empty().remove();
}));





/* VARIANTS EVENT */
// They will be generally a copy, but with variant ID
//
jQuery(document).on('woocommerce_variations_loaded', function(event) {
  
  jQuery('[id*="accordion"]' ).accordion({
      collapsible: true,
      heightStyle: "content",
      header: "> div > h3"
    })
    .sortable({
      axis: "y",
      handle: "h3",
      stop: function( event, ui ) {
        ui.item.children( "h3" ).triggerHandler( "focusout" );
        jQuery( this ).accordion( "refresh" );
      }
  });

  jQuery( '[id*="new_addon_variant_button"]' ).on('click', (function($) {

      variation_id = jQuery(this).attr("data-variation");

      var idArray = jQuery(this).attr("id").split("_");
      var variation_id = idArray[idArray.length-1];

      // get the last DIV which ID
      var $base_div = jQuery('div[id^="addon_var_form_' + variation_id +'_0"]');
      var $last_div = jQuery('div[id^="addon_var_form_' + variation_id + '"]:last');

      // Read the Number from that DIV's ID and increment that number by 1
      var idArray = jQuery($last_div).attr("id").split("_");
      var num = ($last_div.length) ? parseInt(idArray[idArray.length -1]) + 1 : 1;
      //var num = ($last_div.length) ? parseInt( $last_div.prop("id").match(/\d+/g), 10 ) + 1 : 1;


      // Clone it and assign the new ID
      var $new_div = $base_div.clone(true, true).prop('id', 'addon_var_form_' + variation_id + '_' + num ).show();

      //Sustituir titulo
      var $new_title = jQuery($new_div).find("h3").html( "Addon " + variation_id + '_' + num );

      // Loop en todos los elementos
      jQuery ($new_div).find("*[id]").each(function(){    
        // Sustituir todas las Ids 
        jQuery(this).val('');
        var tID = jQuery(this).attr("id");
        var idArray = tID.split("_");
        var idArrayLength = idArray.length;
        //var newID = jQuery(this).attr("id", 'var_' + tID)
        var newId = tID.replace(idArray[idArrayLength-1], variation_id + '_' + num);
        
        //Sustituir todos los nombres para POST
        var tNAME = jQuery(this).attr("name");

        if (tNAME !== undefined) {
          jQuery(this).attr("name", "addon_form[" + variation_id + "][" + num + "][" + tNAME+ "]" )
        }

        jQuery(this).attr('id', newId);
      });

      //Copy default values of selects
      var $origSelects = jQuery('select', $base_div);
      var $clonedSelects = jQuery('select', $new_div);
      $origSelects.each(function(i) {
        $clonedSelects.eq(i).val(jQuery(this).val());
      });

      //Trigger de eventos
      jQuery($new_div).find('[id*="var_addon_price_settings"]').trigger("change");
      jQuery($new_div).find('[id*="var_addon_field_type_var"]').trigger("change");

      //Sustituir fields también
      jQuery ($new_div).find("[class*='form-field']").each(function(){
        var tCLASS = jQuery(this).attr("class").replace(/\d+/, variation_id + "_" + num);
        jQuery(this).attr("class", tCLASS);
      });

      jQuery($new_div).find("span[class*='addon_all_options_container']").attr("class", 'addon_all_options_container_' + num)

      jQuery($new_div).removeAttr('hidden').show();


      //Añadir título
      //$new_div.prepend('<h3 class="addon-ui-accordion-header" style="display:block"> Addon ' + variation_id + "." + num + "</h3>").show();
      jQuery ('#accordion_' + variation_id ).append($new_div).accordion("refresh");
  }));


  jQuery ('[id*="var_addon_price_settings"]').on('change', (function ($) {

    var idArray = jQuery(this).attr("id").split("_");
    var num = idArray[idArray.length-1];
    var variation_id = idArray[idArray.length-2];
  
    jQuery(".var_addon_price_" + variation_id + "_" + num + "_field").show();
    jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").show();

    if ( jQuery(this).val() == 'free' ) {
      //Hide Price
      jQuery(".var_addon_price_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").hide();
    }
  }));

      
  jQuery ('[id*="var_addon_field_type"]').on('change', (function ($) {
   
      
    var idArray = jQuery(this).attr("id").split("_");
    var num = idArray[idArray.length-1];
    var variation_id = idArray[idArray.length-2];

    jQuery(".var_addon_price_settings_" + variation_id + "_" + num + "_field").show();
    jQuery(".var_addon_default_" + variation_id + "_" + num + "_field").show();
    jQuery(".var_addon_price_" + variation_id + "_" + num + "_field").show();
    jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").show();
    jQuery(".var_addon_option_" + variation_id + "_" + num  + "_whole").show();

    if ( ( (jQuery(this).val() == 'text' ) || (jQuery(this).val() == 'text_area' ) ) )
    {
      jQuery(".var_addon_default_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_option_" + variation_id + "_" + num  + "_whole").hide();

      if ( jQuery(`#addon_price_settings_${num}`).val() == 'free' ) {
        //Hide Price
        jQuery(".var_addon_price_" + variation_id + "_" + num + "_field").hide();
        jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").hide();
      }
    }
    if ( (jQuery(this).val() == 'select' ) || (jQuery(this).val() == 'radio' ) )
    {
      jQuery(".var_addon_price_settings_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_default_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_price_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").hide();
    }
    if ( (jQuery(this).val() == 'checkbox' ) || (jQuery(this).val() == 'onoff' ) )
    {
      jQuery(".var_addon_price_settings_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_free_characters_" + variation_id + "_" + num + "_field").hide();
      jQuery(".var_addon_option_" + variation_id + "_" + num  + "_whole").hide();
    }

  }));

  jQuery ('[id*="var_add_new_addon_option_"]').on('click', (function($){

    //Current addon's number
    //var index = parseInt( jQuery(this).prop("id").match(/\d+/g), 10 );

    var idArray = jQuery(this).attr("id").split("_");
    var num = idArray[idArray.length-1];
    var variation_id = idArray[idArray.length-2];
  
    var $base_div = jQuery('div[id^="var_addon_option_form_' + variation_id + '_0"');
    var $last_div = jQuery('div[id^="var_addon_option_form_' + variation_id + '_' + num + '"]:last');
  
    // Read the Number from that DIV's ID and increment that number by 1
    
    var index = 1;

    if ($last_div.length) {
      var idArray = jQuery($last_div).attr("id").split("_");
      index = parseInt(idArray[idArray.length -1]) + 1;
    }
  
    // Clone it and assign the new ID
    var $new_div = $base_div.clone(true, true).prop('id', 'var_addon_option_form_'+ variation_id + '_' + num + "_" + index ).show();
  
    // Loop en todos los elementos
    jQuery ($new_div).find("*[id]").each(function(){    
      // Sustituir todas las Ids 
      jQuery(this).val('');
      var tID = jQuery(this).attr("id");
      var idArray = tID.split("_");
      var idArrayLength = idArray.length;
      var newId = tID.replace(idArray[idArrayLength-1], variation_id + '_' + num + "_" + index);
  /*
      var tNAME = jQuery(this).attr("name");
      jQuery(this).attr("name", `addon_form[0][${index}][addon_option_form][${num}][${tNAME}]` )
  */  
      jQuery(this).attr('id', newId);
    });
  
    //Sustituir todos los nombres para POST
    jQuery($new_div).find("input[id*='add_option_name']").each(function(){
      jQuery(this).attr("name", `addon_form[${variation_id}][${num}][addon_option_form][${index}][name]` )
    });
    jQuery($new_div).find("input[id*='add_option_price']").each(function(){
      jQuery(this).attr("name", `addon_form[${variation_id}][${num}][addon_option_form][${index}][price]` )
    });

  
    jQuery ('#var_addon_all_options_container_' + variation_id + '_' + num ).append($new_div);
  }));

  jQuery ('[id*="var_remove_addon_option_"]').on('click', (function($) {
    var idArray = jQuery(this).attr("id").split("_");
    var index = idArray[idArray.length-1];
    var num = idArray[idArray.length-2];
    var var_id = idArray[idArray.length-3];
  
    jQuery('#var_addon_option_form_'  + var_id + '_' + num + '_' + index ).empty().remove()
  }));
  
  jQuery ('[id^="var_remove_whole_addon_"]').on('click', (function($) {
    var idArray = jQuery(this).attr("id").split("_");
    var num = idArray[idArray.length-1];
    var var_id = idArray[idArray.length-2];
  
    jQuery('#addon_var_form_' + var_id + '_' + num ).empty().remove();
  }));

  
  jQuery ('[id*="var_addon_field_type"]').each(function () {
    jQuery(this).change()
  });

  jQuery ('[id*="var_addon_price_settings"]').each(function () {
    jQuery(this).change()
  });

});