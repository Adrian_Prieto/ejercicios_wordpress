<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Settings', 'apc-purchasenote' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pn-options-page' );
		    do_settings_sections( 'pn-options-page' );
            submit_button();
        ?>

    </form>
</div>