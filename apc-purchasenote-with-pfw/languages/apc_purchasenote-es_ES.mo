��    0      �  C         (     )     ;     R     a     r     y  8   �     �     �  E   �       
   -     8     D     I  
   Y     d     l     q     v  
   �     �     �     �     �     �     �     �                 A   "     d     m     }  &   �     �  	   �     �  !   �     �      	  /   *     Z     ^  	   g     q  �  x     	     	     7	     P	     l	     s	  &   y	     �	     �	  \   �	     
     -
     ;
     G
     N
     `
     l
  	   y
     �
     �
     �
  	   �
     �
     �
     �
     �
          "     =     C     K  Z   T     �     �     �  %   �                     +     L  !   k  3   �     �     �     �     �     (   .                       '             &          +   #   ,                           $                    
          /                                                  -          	   !      )      0                *      "   %       APC Purchase Note Badge Background Color Badge Position Badge Text Color Border Bottom Check whether it shows as a text or textarea on frontend Color Decorating Note Display a badge on the product image in shop and single product pages Enable/Disable the note Field Type Fixed Price Free Free characters In Product In Shop Left Note Note description Note label Padding Price Price Settings Price can be chosen here Price of the Pruchase Note Price per character Purchase Note Options Radius Right Section Select this option to allow customers to include a custom message Settings Show/hide badge Style Test Plugins for Product Purchase Note Text Text Area Text of the badge The background color of the badge The text color of the badge The text to be displayed on item Toggle this on/off to display the Purchase Note Top Top Left Top Right Weight Project-Id-Version: APC Purchase Note 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/apc-purchasenote
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-12-29 11:53+0100
X-Generator: Poedit 2.4.2
X-Domain: apc-purchasenote
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 APC Notas de Compra Color de fondo en la insignia Posición de la insignia Color del texto de Insignia Bordes Abajo Mostrar text o textarea en el frontend Color Nota Decorativo Mostrar insignia en la imagen del producto tanto para tienda y productos de un solo producto Activar / Desactivar nota Tipo de campo Precio fijo Gratis Caracteres gratis En Producto En la tienda Izquierda Nota Descripción de nota Título de Nota Márgenes Precio Opciones de precio Precio puede ser elegido aquí Precio de la Nota de Compra Precio por carácter. Opciones de Nota de Compra Radio Derecha Sección Selecciona esta opción para permitir a los clientes que incluyan un mensaje personalizado Opciones Mostrar o esconder insignia Estilo Plugin de Pureba para Notas de Compra Texto Área de texto Texto de la insignia El color de fondo de la insignia Color del texto de la insignia Texto para mostrarse en el objeto Activa / desactiva para mostrar las Notas de Compra Arriba Arriba Izquierda Arriba Derecha Anchura 