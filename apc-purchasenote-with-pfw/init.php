<?php

/*
 * Plugin Name: APC Purchase Note with Plugin FW
 * Description: Test Plugins for Product Purchase Note
 * Version: 1.0.0
 * Author: Adrián Prieto
 * Text Domain: apc-purchasenote
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;  // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'APC_PN_VERSION' ) ) {
	define( 'APC_PN_VERSION', '1.0.0' );
}

if ( ! defined( 'APC_PN_DIR_URL' ) ) {
	define( 'APC_PN_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'APC_PN_DIR_ASSETS_URL' ) ) {
	define( 'APC_PN_DIR_ASSETS_URL', APC_PN_DIR_URL . 'assets' );
}

if ( ! defined( 'APC_PN_DIR_ASSETS_CSS_URL' ) ) {
	define( 'APC_PN_DIR_ASSETS_CSS_URL', APC_PN_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'APC_PN_DIR_ASSETS_JS_URL' ) ) {
	define( 'APC_PN_DIR_ASSETS_JS_URL', APC_PN_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'APC_PN_DIR_PATH' ) ) {
	define( 'APC_PN_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'APC_PN_DIR_INCLUDES_PATH' ) ) {
	define( 'APC_PN_DIR_INCLUDES_PATH', APC_PN_DIR_PATH . '/includes' );
}

if ( ! defined( 'APC_PN_DIR_TEMPLATES_PATH' ) ) {
	define( 'APC_PN_DIR_TEMPLATES_PATH', APC_PN_DIR_PATH . '/templates' );
}

if ( ! defined( 'APC_PN_DIR_VIEWS_PATH' ) ) {
	define( 'APC_PN_DIR_VIEWS_PATH', APC_PN_DIR_PATH . 'views' );
}

! defined( 'APC_PN_FILE' ) && define( 'APC_PN_FILE', __FILE__ );
! defined( 'APC_PN_SLUG' ) && define( 'APC_PN_SLUG', 'apc_testimonial' );
! defined( 'APC_PN_INIT' ) && define( 'APC_PN_INIT', 'apc-purchasenote-with-pfw/init.php' );
! defined( 'APC_PN_SECRETKEY' ) && define( 'APC_PN_SECRETKEY', 'zd9egFgFdF1D8Azh2ifK' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'apc_pn_init_classes' ) ) {

	/** Calling la */
	function apc_pn_init_classes() {

		load_plugin_textdomain( 'apc-purchasenote', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins.
		require_once APC_PN_DIR_INCLUDES_PATH . '/class-apc-purchasenote.php';

		if ( class_exists( 'APC_PurchaseNote' ) ) {
			/* Call the main function */
			apc_purchasenote_call();
		}
	}
}


add_action( 'plugins_loaded', 'APC_PN_init_classes', 11 );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( APC_PN_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once APC_PN_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( APC_PN_DIR_PATH );

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

if ( ! function_exists( 'yit_deactive_free_version' ) ) {
	require_once 'plugin-fw/yit-deactive-plugin.php';
}

yit_deactive_free_version( 'MY_PLUGIN_FREE_INIT', plugin_basename( __FILE__ ) );




