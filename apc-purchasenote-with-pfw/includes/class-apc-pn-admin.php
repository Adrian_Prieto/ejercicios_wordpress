<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_PN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_PN_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class APC_PN_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_pn_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/* @var YIT_Plugin_Panel_WooCommerce $_panel the panel  used by YITH Plugin Panel (WooCommerce) */
		private $_panel;

		/**
		 * @var Panel page
		 */
		protected $panel_page = 'apc_purchasenote_with_plugin_fw';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_PN_Admin constructor.
		 */
		private function __construct() {
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'apc_pn_custom_tab' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_apc_pn_fields' ) );
			// Save the custom fields.
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_apc_pn_fields' ) );

			/* Options added from deault WP options. See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns. */
			add_filter( 'manage_apc_testimonial_posts_columns', array( $this, 'add_apc_testimonial_post_type_columns' ) );

			/* Options added from custom function. See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column */
			add_action( 'manage_apc_purchasenote_posts_custom_column', array( $this, 'display_apc_purchasenote_custom_column' ), 10, 2 );

			add_filter( 'woocommerce_order_item_display_meta_key',array( $this,'show_as_string_order_item_meta_key' ),10,1 );

			/* Plugin fw filters */
			add_filter( 'plugin_action_links_' . plugin_basename( APC_PN_DIR_PATH . '/' . basename( APC_PN_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );


			/* Main Hook for YITH Plugin Panel WooCommerce */
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
		}

		/* Main functionf of YITH Plugin Panel Woocommerce */
		public function register_panel() {
			if ( !empty( $this->_panel ) ) {
				return;
			}
	
			$admin_tabs = array(
				'settings' => __( 'Settings', 'apc-purchasenote' ),
			);
	
			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'APC Purchase Note', // this text MUST be NOT translatable
				'menu_title'         => 'APC Purchase Note', // this text MUST be NOT translatable
				'plugin_description' => __( 'Put here your plugin description', 'yith-test-plugin' ),
				'capability'         => 'edit_apc_purchasenote',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'apc_purchasenote',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'apc_purchasenote_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => APC_PN_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}


		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( APC_PN_INIT === $plugin_file ) {
				$row_meta_args['slug']       = APC_PN_SLUG;
				$row_meta_args['is_premium'] = true;
			}
			return $row_meta_args;
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, APC_PN_SLUG );
		}

		/**
		 * Gives a basic description of the new pn_custom_tab
		 *
		 * @param  mixed $tabs Inline argument of Woocommerce hooks.
		 */
		public function apc_pn_custom_tab( $tabs ) {
			// Adds the new tab.
			$tabs['decorating_note'] = array(
				'label'    => __( 'Decorating Note', 'apc-purchasenote' ),
				'target'   => 'apc_purchasenote', // Will be used to create an anchor link so needs to be unique.
				'class'    => array( 'apc_purchasenote', 'show_if_simple', 'show_if_variable' ), // Class for your panel tab - helps hide/show depending on product type.
				'priority' => 15,
			);
			return $tabs;
		}

		/**
		 * Display all fields in custom tabs
		 *
		 * @return void
		 */
		public function display_apc_pn_fields() {
			?>
				<div id='apc_purchasenote' class='panel woocommerce_options_panel'>
					<div class="options_group">
						<?php

						global $post;

						$product = wc_get_product( $post->ID );

						$values_meta = array(
							'enable_purchasenote'   => $product->get_meta( 'enable_purchasenote' ),
							'note_label'            => $product->get_meta( 'note_label' ),
							'note_description'      => $product->get_meta( 'note_description' ),
							'note_field_type'       => $product->get_meta( 'note_field_type' ),
							'note_price_settings'   => $product->get_meta( 'note_price_settings' ),
							'note_price'            => $product->get_meta( 'note_price' ),
							'note_free_characters'  => $product->get_meta( 'note_free_characters' ),
							'note_enable_badge'     => $product->get_meta( 'note_enable_badge' ),
							'note_badge_text'       => $product->get_meta( 'note_badge_text' ),
							'note_badge_bg_color'   => $product->get_meta( 'note_badge_bg_color' ),
							'note_badge_text_color' => $product->get_meta( 'note_badge_text_color' ),
						); // Values got from get_meta.

						apc_pn_create_form_field(
							array(
								'id'     => 'enable_purchasenote_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Enable/Disable the note', 'apc-purchasenote' ),
								'desc'   => __( 'Toggle this on/off to display the Purchase Note', 'apc-purchasenote' ),
								'fields' => array(
									'class'   => 'ywcact-product-metabox-onoff',
									'type'    => 'onoff',
									'id'      => 'enable_purchasenote',
									'name'    => 'enable_purchasenote',
									'value'   => empty( $values_meta['enable_purchasenote'] ) ? 'no' : $values_meta['enable_purchasenote'],
									'default' => 'no',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_label_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Note label', 'apc-purchasenote' ),
								'desc'   => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'fields' => array(
									'class' => 'ywcact-product-metabox-text',
									'type'  => 'text',
									'value' => empty( $values_meta['note_label'] ) ? 'Note' : $values_meta['note_label'],
									'id'    => 'note_label',
									'name'  => 'note_label',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_description_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Note description', 'apc-purchasenote' ),
								'desc'   => __( 'Select this option to allow customers to include a custom message', 'apc-purchasenote' ),
								'fields' => array(
									'class' => 'ywcact-product-metabox-price wc_input_price',
									'type'  => 'textarea',
									'value' => empty( $values_meta['note_description'] ) ? '' : $values_meta['note_description'],
									'id'    => 'note_description',
									'name'  => 'note_description',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_field_type_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Field Type', 'apc-purchasenote' ),
								'desc'   => __( 'Check whether it shows as a text or textarea on frontend', 'apc-purchasenote' ),
								'fields' => array(
									'class'   => 'ywcact-product-metabox-price wc_input_price',
									'type'    => 'select',
									'value'   => empty( $values_meta['note_field_type'] ) ? 'text' : $values_meta['note_field_type'],
									'id'      => 'note_field_type',
									'name'    => 'note_field_type',
									'options'  => array(
										'text'      => __( 'Text', 'apc-purchasenote' ),
										'text_area' => __( 'Text Area', 'apc-purchasenote' ),
									),
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_price_settings_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Price Settings', 'apc-purchasenote' ),
								'desc'   => __( 'Price can be chosen here', 'apc-purchasenote' ),
								'fields' => array(
									'class'   => 'ywcact-product-metabox-price wc_input_price',
									'type'    => 'select',
									'id'      => 'note_price_settings',
									'name'    => 'note_price_settings',
									'options'  => array(
										'free'  => __( 'Free', 'apc-purchasenote' ),
										'fixed' => __( 'Fixed Price', 'apc-purchasenote' ),
										'ppc'   => __( 'Price per character', 'apc-purchasenote' ),
									),
									'value'   => empty( $values_meta['note_price_settings'] ) ? 'free' : $values_meta['note_price_settings'],
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_price_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Price', 'apc-purchasenote' ),
								'desc'   => __( 'Price of the Pruchase Note', 'apc-purchasenote' ),
								'fields' => array(
									'class' => 'ywcact-product-metabox-price wc_input_price',
									'type'  => 'number',
									'value' => empty( $values_meta['note_price'] ) ? 0 : $values_meta['note_price'],
									'id'    => 'note_price',
									'name'  => 'note_price',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_free_characters_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Free characters', 'apc-purchasenote' ),
								'desc'   => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'fields' => array(
									'class' => 'ywcact-product-metabox-price wc_input_price',
									'type'  => 'number',
									'value' => empty( $values_meta['note_free_characters'] ) ? '' : $values_meta['note_free_characters'],
									'id'    => 'note_free_characters',
									'name'  => 'note_free_characters',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_enable_badge_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Show/hide badge', 'apc-purchasenote' ),
								'desc'   => __( 'Display a badge on the product image in shop and single product pages', 'apc-purchasenote' ),
								'fields' => array(
									'class'   => 'ywcact-product-metabox-onoff',
									'type'    => 'onoff',
									'id'      => 'note_enable_badge',
									'name'    => 'note_enable_badge',
									'value'   => empty( $values_meta['note_enable_badge'] ) ? 'no' : $values_meta['note_enable_badge'],
									'default' => 'no',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_badge_text_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Text of the badge', 'apc-purchasenote' ),
								'desc'   => __( 'The text to be displayed on item', 'apc-purchasenote' ),
								'fields' => array(
									'class' => 'ywcact-product-metabox-text',
									'type'  => 'text',
									'value' => empty( $values_meta['note_badge_text'] ) ? '' : $values_meta['note_badge_text'],
									'id'    => 'note_badge_text',
									'name'  => 'note_badge_text',
								),
							)
						);

						apc_pn_create_form_field(
							array(
								'id'     => 'note_badge_bg_color_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Badge Background Color', 'apc-purchasenote' ),
								'desc'   => __( 'The background color of the badge', 'apc-purchasenote' ),
								'fields' => array(
									'class'         => 'ywcact-product-metabox-price wc_input_price yith-plugin-fw-colorpicker color-picker',
									'type'          => 'colorpicker',
									'value'         => empty( $values_meta['note_badge_bg_color'] ) ? '#007694' : $values_meta['note_badge_bg_color'],
									'id'            => 'note_badge_bg_color',
									'name'          => 'note_badge_bg_color',
									'alpha_enabled' => false,
								),
							)
						);


						apc_pn_create_form_field(
							array(
								'id'     => 'note_badge_text_color_field',
								'class'  => 'form-field wc_auction_field yith-plugin-ui',
								'title'  => __( 'Badge Text Color', 'apc-purchasenote' ),
								'desc'   => __( 'The text color of the badge', 'apc-purchasenote' ),
								'fields' => array(
									'class'         => 'ywcact-product-metabox-price wc_input_price yith-plugin-fw-colorpicker color-picker',
									'type'          => 'colorpicker',
									'value'         => empty( $values_meta['note_badge_text_color'] ) ? '#ffffff' : $values_meta['note_badge_text_color'],
									'id'            => 'note_badge_text_color',
									'name'          => 'note_badge_text_color',
									'alpha_enabled' => false,
								),
							)
						);


						echo '<input type="hidden" name="apc_purchasenote_nonce" value="' . esc_attr( wp_create_nonce() ) . '">';
						?>
					</div>
				</div>
			<?php
		}

		/**
		 * Save all elements after custom tab form.
		 *
		 * @param  mixed $post_id Inline argument.
		 */
		public function save_apc_pn_fields( $post_id ) {

			// Check if our nonce is set.
			if ( ! isset( $_POST['apc_purchasenote_nonce'] ) ) {
				return $post_id;
			}
			$nonce = sanitize_text_field( wp_unslash( $_POST['apc_purchasenote_nonce'] ) );

			// Verify that the nonce is valid.
			if ( ! wp_verify_nonce( $nonce ) ) {
				return $post_id;
			}

			if ( isset( $_POST['enable_purchasenote'] ) && ( 'yes' === $_POST['enable_purchasenote'] ) ) {

				$product = wc_get_product( $post_id );

				// Save all parameters.

				$product->update_meta_data( 'enable_purchasenote', 'yes' ); // Default value, no need to get POST.

				if ( isset( $_POST['note_label'] ) ) {
					$note_label = sanitize_text_field( wp_unslash( $_POST['note_label'] ) );
					$product->update_meta_data( 'note_label', sanitize_text_field( $note_label ) );
				}

				if ( isset( $_POST['note_description'] ) ) {
					$note_description = sanitize_text_field( wp_unslash( $_POST['note_description'] ) );
					$product->update_meta_data( 'note_description', sanitize_text_field( $note_description ) );
				}

				if ( isset( $_POST['note_field_type'] ) ) {
					$note_field_type = sanitize_text_field( wp_unslash( $_POST['note_field_type'] ) );
					$product->update_meta_data( 'note_field_type', sanitize_text_field( $note_field_type ) );
				}

				if ( isset( $_POST['note_price_settings'] ) ) {
					$note_price_settings = sanitize_text_field( wp_unslash( $_POST['note_price_settings'] ) );
					$product->update_meta_data( 'note_price_settings', sanitize_text_field( $note_price_settings ) );
				}

				if ( isset( $_POST['note_price'] ) ) {
					$note_price = sanitize_text_field( wp_unslash( $_POST['note_price'] ) );
					$product->update_meta_data( 'note_price', sanitize_text_field( $note_price ) );
				}

				if ( isset( $_POST['note_price_settings'] ) ) {
					$note_price_settings = sanitize_text_field( wp_unslash( $_POST['note_price_settings'] ) );
					$product->update_meta_data( 'note_price_settings', sanitize_text_field( $note_price_settings ) );
				}

				if ( isset( $_POST['note_free_characters'] ) ) {
					$note_free_characters = sanitize_text_field( wp_unslash( $_POST['note_free_characters'] ) );
					$product->update_meta_data( 'note_free_characters', sanitize_text_field( $note_free_characters ) );
				}

				if ( isset( $_POST['note_enable_badge'] ) ) {
					$note_enable_badge = sanitize_text_field( wp_unslash( $_POST['note_enable_badge'] ) );
					$product->update_meta_data( 'note_enable_badge', sanitize_text_field( $note_enable_badge ) );
				}

				if ( isset( $_POST['note_badge_text'] ) ) {
					$note_badge_text = sanitize_text_field( wp_unslash( $_POST['note_badge_text'] ) );
					$product->update_meta_data( 'note_badge_text', sanitize_text_field( $note_badge_text ) );
				}

				if ( isset( $_POST['note_badge_bg_color'] ) ) {
					$note_badge_bg_color = sanitize_text_field( wp_unslash( $_POST['note_badge_bg_color'] ) );
					$product->update_meta_data( 'note_badge_bg_color', sanitize_text_field( $note_badge_bg_color ) );
				}

				if ( isset( $_POST['note_badge_text_color'] ) ) {
					$note_badge_text_color = sanitize_text_field( wp_unslash( $_POST['note_badge_text_color'] ) );
					$product->update_meta_data( 'note_badge_text_color', sanitize_text_field( $note_badge_text_color ) );
				}

				$note_badge_text_color = isset( $_POST['note_badge_text_color'] );

				$product->save();
			}
		}

		/**
		 * Print the show border field
		 */
		public function print_pn_border() {

			$default_border = array( 'weight' => 1, 'style' => 'solid', 'color' => '#d8d8d8', 'radius' => 7 );
			$border         = get_option( 'apc_pn_border', $default_border );

			?>
			<div>
				<label class="label_border_weight" for="label_border_weight"><?php esc_html_e( 'Weight', 'apc-purchasenote' ); ?></label>
				<input type="number" id="apc_pn_border_weight" name="apc_pn_border[weight]"
					value="<?php echo esc_html( $border['weight'] ); ?>">
				<br/>
				<label class="label_border_style" for="label_border_style"><?php esc_html_e( 'Style', 'apc-purchasenote' ); ?></label>
				<input type="text" id="apc_pn_border_style" name="apc_pn_border[style]"
					value="<?php echo esc_html( $border['style'] ); ?>">
				<br/>
				<?php 
					// View coding preview.
					apc_pn_get_view(
						'/inputs/colorpicker.php',
						array(
							'element_id'      => 'border_color',
							'element_name'    => 'border[color]',
							'element_value'   => $border['color'],
							'element_display' => __( ' Border Color', 'apc-purchasenote' ),
						)
					);
				?>
				<br/>
				<label class="label_border_radius" for="label_border_radius"><?php esc_html_e( 'Radius', 'apc-purchasenote' ); ?></label>
				<input type="number" id="apc_pn_border_radius" name="apc_pn_border[radius]"
					value="<?php echo esc_html( $border['radius'] ); ?>">
			</div>
			<?php
		}

		/**
		 * Show Meta Key as a "friendly name" on admin.
		 *
		 * @param  string $display_key Display name. Will be changed here.
		 */
		public function show_as_string_order_item_meta_key( $display_key ) {
			if ( strpos( $display_key, 'content_note' ) !== false ) {
				if ( 'content_note' === $display_key ) {
					$display_key = esc_html__( 'Content Note', 'yith-woocommerce-gift-cards' );
				}
			}

			if ( strpos( $display_key, 'note_price' ) !== false ) {
				if ( 'note_price' === $display_key ) {
					$display_key = esc_html__( 'Note Price', 'apc-purchasenote' );
				}
			}
			return $display_key;
		}
	}
}
