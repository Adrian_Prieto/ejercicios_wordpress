jQuery(document).ready(function($){

  $('#enable_purchasenote').on('change',function() {


    $('#note_label_field').show()
    $('#note_description_field').show()
    $('#note_field_type_field').show()
    $('#note_price_settings_field').show()
    $('#note_price_field').show()
    $('#note_free_characters_field').show()
    $('#note_enable_badge_field').show()

    $('#note_badge_text_field').show()
    $('#note_badge_bg_color_field').show()
    $('#note_badge_text_color_field').show()

    if ( ! $(this).prop( "checked" ) ) {

      $('#note_label_field').hide()
      $('#note_description_field').hide()
      $('#note_field_type_field').hide()
      $('#note_price_settings_field').hide()
      $('#note_price_field').hide()
      $('#note_free_characters_field').hide()
      $('#note_enable_badge_field').hide()

      $('#note_badge_text_field').hide()
      $('#note_badge_bg_color_field').hide()
      $('#note_badge_text_color_field').hide()
    }
      
    if ( ! $('#note_enable_badge').prop( "checked" ) ) {

      $('#note_badge_text_field').hide()
      $('#note_badge_bg_color_field').hide()
      $('#note_badge_text_color_field').hide()
    }
   
  
    
  });
  
  $('#note_enable_badge').on('change', function() {
  
    if ( $(this).prop( "checked" ) ) {
      $('#note_badge_text_field').show()
      $('#note_badge_bg_color_field').show()
      $('#note_badge_text_color_field').show()
    } else {
      $('#note_badge_text_field').hide()
      $('#note_badge_bg_color_field').hide()
      $('#note_badge_text_color_field').hide()
    }
  
    
  });
  
 $('#note_price_settings').on('change', function() {
  

    if ( $(this).val() != "free") {
      $('#note_price_field').show()
      $('#note_free_characters_field').show()
    } else {
      $('#note_price_field').hide()
      $('#note_free_characters_field').hide()
    }
  });
  

  jQuery ('#enable_purchasenote').each(function () {
    jQuery(this).change()
  });
  
  jQuery ('#note_enable_badge').each(function () {
    jQuery(this).change()
  });

  jQuery ('#note_price_settings').each(function () {
    jQuery(this).change()
  });
});

