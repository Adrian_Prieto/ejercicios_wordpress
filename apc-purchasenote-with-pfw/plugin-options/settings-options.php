<?php

$pn_settings = array(
    'settings' => array(
        'general-options'     => array(
            'name' => __( 'Purchase Note Options', 'apc_testimonial' ),
            'type' => 'title',
            'desc' => '',
        ),
        
        'apc_pn_padding' => array(
            'id'           => 'apc_pn_padding',
            'name'         => __( 'Padding', 'apc_testimonial' ),
            'type'         => 'yith-field',
            'yith-type'    => 'dimensions',
            'default' => array(
                'dimensions' => array(
                    'top'    => 5,
                    'right'  => 10,
                    'bottom' => 15,
                    'left'   => 25,
                ),
            ),
            'desc'         => '',
            'min'          => 0,
            'allow_linked' => false,
            'unit'       => 'percentage',
            'linked'     => 'no',
        ),


        'apc_pn_border' => array(
            'id'      => 'apc_pn_padding',
            'name'    => __( 'Padding', 'apc_testimonial' ),
            'type'      => 'yith-field',
            'yith-type' => 'custom',
            'action'  => 'apc_print_pn_border',
            'desc'    => '',
        ),

        'apc_pn_badge_pos[shop]' => array(
            'id'      => 'apc_pn_badge_pos[shop]',
            'name'    => __( 'Padding', 'apc_testimonial' ),
            'type'      => 'yith-field',
            'yith-type' => 'select',
            'options'   => array(
                'top-left' => __( 'Top Left', 'apc-purchasenote' ),
                'top-right' => __( 'Top Right', 'apc-purchasenote' ),
            ),
            'desc'    => '',
        ),

        'apc_pn_badge_pos[product]' => array(
            'id'      => 'apc_pn_badge_pos[product]',
            'name'    => __( 'Padding', 'apc_testimonial' ),
            'type'      => 'yith-field',
            'yith-type' => 'select',
            'options'   => array(
                'top-left' => __( 'Top Left', 'apc-purchasenote' ),
                'top-right' => __( 'Top Right', 'apc-purchasenote' ),
            ),
            'desc'    => '',
        ),

        'general-options-end' => array(
            'type' => 'sectionend',
            'id'   => 'yith-wcbk-general-options'
        ),
    ),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $pn_settings );
