<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! function_exists( 'purchasenote_admin_enqueue_scripts' ) ) {
	/** CSS and JS Register Function for Admin */
	function purchasenote_admin_enqueue_scripts() {
		$js_url = YITH_APC_PMHP_DIR_ASSETS_JS_URL . '/yith_apc_pm_admin.js';

		wp_register_style( 'yith-wcact-admin-css', YITH_APC_PMHP_DIR_ASSETS_CSS_URL . '/yith_apc_pm_admin.css', array( 'yith-plugin-fw-fields', 'yith-wcact-admin-settings-sections' ) );
	}
}
add_action( 'admin_enqueue_scripts', 'purchasenote_admin_enqueue_scripts' );

if ( ! function_exists( 'yith_apc_productmessage_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_apc_productmessage_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_APC_PMHP_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


if ( ! function_exists( 'yith_apc_productmessage_admin_give_permissions' ) ) {
	/**
	 * Give plugin permissions to admin
	 *
	 * @return void
	 */
	function yith_apc_productmessage_admin_give_permissions() {
		$admin_role = get_role( 'administrator' );
		$admin_role->add_cap( 'edit_yith_apc_pmhp' );
		$admin_role->add_cap( 'edit_yith_apc_pmhps' );
		$admin_role->add_cap( 'edit_other_yith_apc_pmhps' );
		$admin_role->add_cap( 'publish_yith_apc_pmhps' );
		$admin_role->add_cap( 'read_yith_apc_pmhp' );
		$admin_role->add_cap( 'read_private_yith_apc_pmhps' );
		$admin_role->add_cap( 'delete_yith_apc_pmhp' );
	}
}
add_action( 'admin_init', 'yith_apc_productmessage_admin_give_permissions', 999 ); // Although admin_init, or even after_setup_theme can also be used.
