<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PMHP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'Yith_Apc_PMHP_Post_Types' ) ) {
	/**
	 * Main class for CPT
	 */
	class Yith_Apc_PMHP_Post_Types {

		/**
		 * Main instance.
		 *
		 * @var object
		 */
		private static $instance;

		/**
		 * Name of the CPT.
		 *
		 * @var string
		 */
		public static $post_type = 'yith_apc_pmhp';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
		}

		/**
		 * Setup the custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label'              => __( 'Product Message', 'yith-apc-productmessage' ),
				'description'        => __( 'Product Message Post Type', 'yith-apc-productmessage' ),
				'public'             => false,
				'publicly_queryable' => true,
				'menu_icon'          => '',
				'capability_type'    => 'post',
				'show_in_menu'       => false,
				'show_ui'            => true,
				'has_archive'        => true,
				'rewrite'            => false,
				'capabilities'       => array(
					'edit_post'          => 'edit_yith_apc_pmhp',
					'edit_posts'         => 'edit_yith_apc_pmhps',
					'edit_others_posts'  => 'edit_other_yith_apc_pmhps',
					'publish_posts'      => 'publish_yith_apc_pmhps',
					'read_post'          => 'read_yith_apc_pmhp',
					'read_private_posts' => 'read_private_yith_apc_pmhps',
					'delete_post'        => 'delete_yith_apc_pmhp',
				),
				'supports'           => array( '' ),
			);
			register_post_type( self::$post_type, $args );
		}
	}
}
