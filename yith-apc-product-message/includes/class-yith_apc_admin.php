<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PMHP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_APC_PMHP_Admin' ) ) {
	/**
	 * Class with admin functions and hooks.
	 */
	class YITH_APC_PMHP_Admin {
		/**
		 * Main Instance
		 *
		 * @var yith_apc_pmhp_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**  @var YIT_Plugin_Panel_WooCommerce $_panel the panel  used by YITH Plugin Panel (WooCommerce) */
		private $_panel;

		/**
		 * @var Panel page
		 */
		protected $panel_page = 'yith_apc_pmhp';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_APC_PMHP_Admin constructor.
		 */
		private function __construct() {
			/* Plugin fw filters */
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_APC_PMHP_DIR_PATH . '/' . basename( YITH_APC_PMHP_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			/* Main Hook for YITH Plugin Panel WooCommerce */
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );

			/* Action for custom tab in YITH Plugin Panel WooCommerce */
			add_action( 'yith_apc_pmhp_print_message_tab', array( $this, 'print_message_tab' ), 10, 1 );

			/*Plugin FW Metabox*/
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 ); // Can't add another metabox with another priority (fixed 10).
		}

		/** Main functionf of YITH Plugin Panel Woocommerce */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'message' => __( 'Product Message', 'yith-apc-productmessage' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH APC Product Message',
				'menu_title'         => 'YITH APC Product Message',
				'plugin_description' => __( 'Manage the plugin here', 'yith-apc-productmessage' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith_plugin_panel',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith-apc-productmessage_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_APC_PMHP_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_APC_PMHP_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_APC_PMHP_SLUG;
				$row_meta_args['is_premium'] = true;
			}
			return $row_meta_args;
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_APC_PMHP_SLUG );
		}

		/**
		 * Adds the metabox to be shown when creating a new CPT
		 */
		public function add_metabox() {

			// Cannot access post_id from here.

			$all_products_array = array();

			$all_ids = get_posts(
				array(
					'post_type'   => 'product',
					'numberposts' => -1,
					'post_status' => 'publish',
					'fields'      => 'ids',
				)
			);

			foreach ( $all_ids as $id ) {
				$all_products_array[ $id ] = wc_get_product( $id )->get_title();
			}

			$args = array(
				'label'    => __( 'Product Message Options', 'yith-apc-productmessage' ),
				'pages'    => 'yith_apc_pmhp',
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array(
						'label'  => __( 'Settings', 'yith-apc-productmessage' ),
						'fields' => array(
							'yith_apc_pmhp_apply' => array(
								'label'   => __( 'Apply this rule', 'yith-apc-productmessage' ),
								'desc'    => __( 'Check whether this message will be shown.', 'yith-apc-productmessage' ),
								'type'    => 'onoff',
								'private' => true,
								'std'     => 'yes',
							),

							'yith_apc_pmhp_message' => array(
								'label'   => __( 'Message', 'yith-apc-productmessage' ),
								'type'    => 'textarea-editor',
								'private' => true,
								'std'     => '',
							),
							
							'yith_apc_pmhp_product' => array(
								'label'    => __( 'Select Products', 'yith-apc-productmessage' ),
								'desc'     => __( 'Choose which products will be followed by this rule.', 'yith-apc-productmessage' ),
								'type'     => 'select-buttons',
								'private'  => true,
								'multiple' => 'true',
								'options'  => $all_products_array,
							),
					

							/*
							'yith_apc_pmhp_product' => array (
								'label'    => __( 'Select Products', 'yith-apc-productmessage' ),
								'desc'     => __( 'Choose which products will be followed by this rule.', 'yith-apc-productmessage' ),
								'type'     => 'ajax-products',
								'private'  => true,	
							),
							*/

							'yith_apc_pmhp_where' => array(
								'label'   => __( 'Where to display the message on product page', 'yith-apc-productmessage' ),
								'type'    => 'select',
								'private' => true,
								'std'     => 'before_cart',
								'options' => array(
									'before_title' => __( 'Before title', 'yith-apc-productmessage' ),
									'after_title'  => __( 'After title', 'yith-apc-productmessage' ),
									'before_price' => __( 'Before price', 'yith-apc-productmessage' ),
									'after_price'  => __( 'After price', 'yith-apc-productmessage' ),
									'before_cart'  => __( 'Before add to cart', 'yith-apc-productmessage' ),
									'after_cart'   => __( 'After add to cart', 'yith-apc-productmessage' ),
								),
							),

							'yith_apc_pmhp_hide' => array(
								'label'   => __( "Hide the product's price", 'yith-apc-productmessage' ),
								'desc'    => __( 'Check whether this message will be shown.', 'yith-apc-productmessage' ),
								'type'    => 'onoff',
								'private' => true,
								'std'     => 'yes',
							),
						),
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_metabox_test' );
			$metabox1->init( $args );
		}
	}
}
