<?php

/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_APC_PMHP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_APC_PMHP_Frontend' ) ) {
	/**
	 * Class with functions used in frontend
	 */
	class YITH_APC_PMHP_Frontend {

		/**
		 * Main Instance
		 *
		 * @var object Instance of the class to be called in get_instance
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Single message to be post in the message, so it counts the restriction of single message per product
		 *
		 * @var array
		 */
		private $message_array;

		private $current_id;

		/**
		 * Main plugin Instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Frotend constructor.
		 */
		private function __construct() {
			$this->current_message_ = array();

			! is_admin() && add_action( 'init', array( $this, 'load_message_from_db' ) );

			add_filter( 'woocommerce_after_shop_loop_item_title', array( $this, 'phmp_remove_price' ), 10, 1 );
			add_action( 'woocommerce_before_single_product', array( $this, 'show_message_on_single_product' ), 10, 1 );
	
			// Remove price in cart & checkout.
			add_filter( 'woocommerce_cart_item_price', '__return_false' );
			add_filter( 'woocommerce_cart_item_subtotal', '__return_false' );
		}


		public function load_message_from_db() {
			// Query for searching the current product's message.
			$args = array(
				'post_type' => 'yith_apc_pmhp',
			);
			$my_query = new WP_Query( $args );

			if ( $my_query->have_posts() ) :
				while ( $my_query->have_posts() ) :
					$my_query->the_post();
					$my_id = get_the_ID();

					if ( get_post_meta( $my_id, '_yith_apc_pmhp_apply', true ) ) {

						$product_array = get_post_meta( $my_id, '_yith_apc_pmhp_product', true );
						$where_        = get_post_meta( $my_id, '_yith_apc_pmhp_where', true );
						$hide_         = get_post_meta( $my_id, '_yith_apc_pmhp_hide', true );
						foreach ( $product_array as $index => $product_id ) {
							$this->message_array[ $product_id ] = array(
								'where' => $where_,
								'hide'  => $hide_,
							);
						}
					}
				endwhile;
				wp_reset_postdata();
			endif;
		}

		/**
		 * Do the database query of all products with messages, and hide their price.
		 *
		 * @param array $args Used in query, declared in function.
		 */
		public function phmp_remove_price( $args ) {
			global $product;

			if ( true === $this->message_array[ $product->get_ID() ]['hide'] ) {
				add_filter( 'woocommerce_get_price_html', '__return_false' );
			}
		}

		/**
		 * Called in single-product view, get their message (the last one which their rule is on) and display it.
		 */
		public function show_message_on_single_product() {
			if ( is_product() ) {

				global $product;
				$this->current_id = $product->get_id();

				// Where to display it?
				if ( isset( $this->message_array[ $this->current_id ] ) ) {

					if ( $this->message_array[ $this->current_id ]['hide'] ) {

						// Remove price in single product (even base and variation).
						remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
						remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
					}

					switch ( $this->message_array[ $this->current_id ]['where'] ) {
						// Title has a priority of 5.. (See it in content-single-product.php ).
						case 'before_title':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 4 );
							break;
						case 'after_title':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 6 );
							break;

						// Price has a priority of 10.
						case 'before_price':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 8 );
							break;
						case 'after_price':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 11 );
							break;

						// Add to cart is priority 30.
						case 'before_cart':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 29 );
							break;
						case 'after_cart':
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 31 );
							break;
						default:
							add_filter( 'woocommerce_single_product_summary', array( $this, 'phmp_message_html' ), 21 );
					}
				}
			}
		}

		/**
		 * Function to print the message html
		 */
		public function phmp_message_html() {
			if ( $this->current_id && isset( $this->message_array[ $this->current_id ]['message'] ) ) {
				echo '<p>' . esc_html( $this->message_array[ $this->current_id ]['message'] ) . '</p>';
			}
		}
	}
}
