<?php

if ( ! class_exists( ' Yith_Apc_ProductMessage_ListTable' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

	/**
	 * WP_Table for display datatable in admin.
	 */
	class Yith_Apc_ProductMessage_ListTable extends WP_List_Table {

		/** Class constructor */
		public function __construct() {

			parent::__construct(
				array(
					'singular' => __( 'Message', 'yith-apc-productmessage' ), // singular name of the listed records.
					'plural'   => __( 'Messages', 'yith-apc-productmessage' ), // plural name of the listed records.
					'ajax'     => false, // should this table support ajax?
				)
			);
		}

		/**
		 * Retrieve customer’s data from the database
		 *
		 * @param int $per_page Entries displayed per page.
		 * @param int $page_number Number page.
		 *
		 * @return mixed
		 */
		public static function get_meta_messages( $per_page = 8, $page_number = 1 ) {

			$result = array();

			$args = array(
				'post_type'      => 'yith_apc_pmhp',
				'posts_per_page' => $per_page,
			);
			
			$my_query = new WP_Query( $args );

			if ( $my_query->have_posts() ) :
				while ( $my_query->have_posts() ) :
					$my_query->the_post();
					$my_id = get_the_ID();
					$result[ $my_id ]['id_post']         = get_the_ID();
					$result[ $my_id ]['message_product'] = get_post_meta( $my_id, '_yith_apc_pmhp_message', true );
					$result[ $my_id ]['product']         = get_post_meta( $my_id, '_yith_apc_pmhp_product', true );
					$result[ $my_id ]['apply_rule']      = get_post_meta( $my_id, '_yith_apc_pmhp_apply', true );
					$result[ $my_id ]['where_display']   = get_post_meta( $my_id, '_yith_apc_pmhp_where', true );
					$result[ $my_id ]['hide_price']      = get_post_meta( $my_id, '_yith_apc_pmhp_hide', true );
				endwhile;
				wp_reset_postdata();
			endif;
			return $result;
		}

		/** Text displayed when no customer data is available */
		public function no_items() {
			esc_attr_e( 'No products to be shown.', 'sp' );
		}

		/**
		 * Returns the count of records in the database.
		 *
		 * @return null|string
		 */
		public static function record_count() {
			return wp_count_posts( 'yith_apc_pmhp' )->publish;
		}

		/**
		 *  Associative array of columns
		 *
		 * @return array
		 */
		public function get_columns() {
			$columns = array(
				'message_product' => esc_html__( 'Message', 'yith-apc-productmessage' ),
				'product'         => esc_html__( 'Product', 'yith-apc-productmessage' ),
				'apply_rule'      => esc_html__( 'Apply Rule', 'yith-apc-productmessage' ),
				'where_display'   => esc_html__( 'Where it is displayed', 'yith-apc-productmessage' ),
				'hide_price'      => esc_html__( 'Hidden Price', 'yith-apc-productmessage'),
			);

			return $columns;
		}

		/**
		 * Handles data query and filter, sorting, and pagination.
		 */
		public function prepare_items() {

			$columns = $this->get_columns();

			$this->_column_headers = array( $columns );

			$per_page     = $this->get_items_per_page( 'messages_per_page', 8 );
			$current_page = $this->get_pagenum();
			$total_items  = self::record_count();

			$this->set_pagination_args(
				array(
					'total_items' => $total_items, // WE have to calculate the total number of items.
					'per_page'    => $per_page, // WE have to determine how many items to show on a page.
				)
			);

			$this->items = self::get_meta_messages( $per_page, $current_page );
		}


		/**
		 * Render a column when no column specific method exists.
		 *
		 * @param array  $item Name Array of all results.
		 * @param string $column_name Columns listed in get_columns().
		 *
		 * @return mixed
		 */
		public function column_default( $item, $column_name ) {

			switch ( $column_name ) {
				case 'message_product':
					return '<a href="post.php?post=' . $item['id_post'] . '&action=edit">' . $item[ $column_name ] . '</a>';
				case 'product':
					$n_of_products = count( $item[ $column_name ] );
					$string = '';

					for ( $i = 0; $i < $n_of_products; ++$i ) {
						$sep     = ( $n_of_products - 1 === $i ) ? '.' : ', ';
						$string .= wc_get_product( $item[ $column_name ][ $i ] )->get_title() . $sep;
					}

					return $string;
				case 'hide_price':
					$field = array(
						'id'      => 'yith_apc_pmhp_hide',
						'name'    => 'yith_apc_pmhp_hide',
						'type'    => 'onoff',
						'private' => true,
						'std'     => 'yes',
						'value'   => $item[ $column_name ],
					);
					return yith_plugin_fw_get_field( $field, true ); // Show Html of the on/off.

				case 'apply_rule':
					$field = array(
						'id'      => 'yith_apc_pmhp_apply',
						'name'    => 'yith_apc_pmhp_apply',
						'type'    => 'onoff',
						'private' => true,
						'std'     => 'yes',
						'value'   => $item[ $column_name ],
					);
					return yith_plugin_fw_get_field( $field, true ); // Show Html of the on/off.

				case 'where_display':
					$where_array = array(
						'before_title' => esc_html__( 'Before title', 'yith-apc-productmessage' ),
						'after_title'  => esc_html__( 'After title', 'yith-apc-productmessage' ),
						'before_price' => esc_html__( 'Before price', 'yith-apc-productmessage' ),
						'after_price'  => esc_html__( 'After price', 'yith-apc-productmessage' ),
						'before_cart'  => esc_html__( 'Before add to cart', 'yith-apc-productmessage' ),
						'after_cart'   => esc_html__( 'After add to cart', 'yith-apc-productmessage' ),
					);
					return $where_array[ $item[ $column_name ] ];
				default:
					return $item; // Show the whole array for troubleshooting purposes.
			}
		}
	}
}


