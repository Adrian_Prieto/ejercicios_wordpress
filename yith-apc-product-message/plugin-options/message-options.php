<?php

$message = array(
    'message' => array(
        'general-options'     => array(
            'title' => __( 'Table', 'yith-apc-productmessage' ),
            'type'  => 'title',
            'desc'  => '',
        ),
        'message_table'       => array(
            'id'                   => 'yith_apc_pmhp_list_table',
            'type'                 => 'yith-field',
            'yith-type'            => 'list-table',
            'list_table_class'     => 'Yith_Apc_ProductMessage_ListTable',
            'list_table_class_dir' => YITH_APC_PMHP_DIR_PATH . 'includes/class-yith_apc_customtable.php',
            'title'                => 'Product Messages',
            'add_new_button'       => 'New Message',
            'post_type'            => 'yith_apc_pmhp',
        ),
        'general-options-end' => array(
            'type' => 'sectionend',
            'id'   => 'yith-wcbk-general-options',
        ),
    ),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $message );

?>