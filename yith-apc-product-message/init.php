<?php

/**
 * Plugin Name: YITH APC Product Message and Hide Price
 * Description: Test plugin that adds & hides messages in the product page.
 * Version: 1.0.0
 * Author: Adrián Prieto
 * Text Domain: yith-apc-productmessage
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;  // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_APC_PMHP_VERSION' ) ) {
	define( 'YITH_APC_PMHP_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_URL' ) ) {
	define( 'YITH_APC_PMHP_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_APC_PMHP_DIR_ASSETS_URL', YITH_APC_PMHP_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_APC_PMHP_DIR_ASSETS_CSS_URL', YITH_APC_PMHP_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_APC_PMHP_DIR_ASSETS_JS_URL', YITH_APC_PMHP_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_PATH' ) ) {
	define( 'YITH_APC_PMHP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_APC_PMHP_DIR_INCLUDES_PATH', YITH_APC_PMHP_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_APC_PMHP_DIR_TEMPLATES_PATH', YITH_APC_PMHP_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_APC_PMHP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_APC_PMHP_DIR_VIEWS_PATH', YITH_APC_PMHP_DIR_PATH . 'views' );
}

! defined( 'YITH_APC_PMHP_FILE' ) && define( 'YITH_APC_PMHP_FILE', __FILE__ );
! defined( 'YITH_APC_PMHP_SLUG' ) && define( 'YITH_APC_PMHP_SLUG', 'yith-apc-productmessage' );
! defined( 'YITH_APC_PMHP_INIT' ) && define( 'YITH_APC_PMHP_INIT', 'yith-apc-product-message/init.php' );
! defined( 'YITH_APC_PMHP_SECRETKEY' ) && define( 'YITH_APC_PMHP_SECRETKEY', 'zd9egFgFdF1D8Azh2ifK' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_apc_pmhp_init_classes' ) ) {

	/** Calling la */
	function yith_apc_pmhp_init_classes() {

		load_plugin_textdomain( 'yith-apc-productmessage', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins.
		require_once YITH_APC_PMHP_DIR_INCLUDES_PATH . '/class-yith_apc_productmessage.php';

		if ( class_exists( 'Yith_Apc_ProductMessage' ) ) {
			/* Call the main function */
			yith_apc_productmessage_call();
		}
	}
}


add_action( 'plugins_loaded', 'YITH_APC_PMHP_init_classes', 11 );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_APC_PMHP_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_APC_PMHP_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( YITH_APC_PMHP_DIR_PATH );

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

if ( ! function_exists( 'yit_deactive_free_version' ) ) {
	require_once 'plugin-fw/yit-deactive-plugin.php';
}

yit_deactive_free_version( 'MY_PLUGIN_FREE_INIT', plugin_basename( __FILE__ ) );




