��          �   %   �      @     A     S     _  
   k     v     �     �     �  )   �  4   �          )     1     :     B     R     j     �     �     �  ;   �     �  ,   �  '   "  �  J  '   �     $     8     M     [  $   n     �     �  #   �  3   �          (     0     9     B      V  "   w     �     �     �  R   �       2   %  .   X                            	                                    
                                                              After add to cart After price After title Apply Rule Apply this rule Before add to cart Before price Before title Check whether this message will be shown. Choose which products will be followed by this rule. Manage the plugin here Message Messages Product Product Message Product Message Options Product Message Post Type Select Products Settings Table Test plugin that adds & hides messages in the product page. Where it is displayed Where to display the message on product page YITH APC Product Message and Hide Price Project-Id-Version: YITH APC Product Message and Hide Price 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-apc-product-message
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-02-22 13:33+0100
X-Generator: Poedit 2.4.2
X-Domain: yith-apc-productmessage
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Después del botón de añadir al carro Después del precio Después del título Aplicar Regla Aplicar esta regla Antes del botón de añadir al carro Antes del precio Antes del título Marcar si este mensaje se mostrará Elegir a qué productos se les aplicará esta regla Opciones del plugin aquí Mensaje Mensajes Producto Mensaje de Producto Opciones del Mensaje de Producto Tipo del post Mensaje del Producto Seleccionar productos Opciones Tabla Plugin de prueba que añade mensajes y esconde precios en la página de productos. Donde se mostrará Donde mostrar el mnesjae en la página de producto YITH APC Mensaje de Producto y Esconder Precio 