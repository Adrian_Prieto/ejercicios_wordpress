<?php
/**
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_DB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_DB_Admin' ) ) {

	class apc_db_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_db_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_DB_Admin constructor.
		 */
		private function __construct() {

			/* Action realted to metaboxes */
			//add_action( 'save_post', array( $this, 'save_meta_box' ) );

			/* Options added from deault WP options. See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns. */
			//add_filter( 'manage_apc_testimonial_posts_columns', array( $this, 'add_apc_testimonial_post_type_columns' ) );

			/* Options added from custom function. See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column */
			//add_action( 'manage_apc_testimonial_posts_custom_column', array( $this, 'display_apc_testimonial_post_type_custom_column' ), 10, 2 );

			/** Admin menu */
			add_action( 'admin_menu', array( $this, 'create_menu_for_database_view' ) );
			//add_action( 'admin_init', array( $this, 'register_settings' ) );

			/* Link transient functions */	
			//add_filter( 'option_page_capability_ps-options-page', array( $this, 'allow_menu_page_options' ) );


		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			apc_tt_get_view( '/metaboxes/plugin-skeleton-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Filters the columns displayed in the Posts list table for plugin skeleton post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_apc_testimonial_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'apc_testimonial_custom_columns',
				array(
					'role'    => esc_html__( 'Role', 'apc_testimonial' ),
					'company' => esc_html__( 'Company', 'apc_testimonial' ),
					'email'   => esc_html__( 'Email', 'apc_testimonial' ),
					'stars'   => esc_html__( 'Stars', 'apc_testimonial' ),
					'vip'     => esc_html__( 'VIP', 'apc_testimonial' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_database_view() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Raffle Database', 'apc_database' ),
					esc_html__( 'View Raffle Database', 'apc_database' ),
					'view_apc_database',
					'menu_apc_database',
					array( $this, 'apc_database_custom_menu_page' ),
					'',
					40
				);

				$this->db_obj = new Datatable_List();

		}
		/** Callback custom menu page */
		public function apc_database_custom_menu_page() {
			//apc_db_get_view( '/admin/plugin-options-panel.php', array() );
			?>
				<div class="wrap">
					<h2>WP_List_Table Class Example</h2>

					<div id="poststuff">
						<div id="post-body" class="metabox-holder columns-2">
							<div id="post-body-content">
								<div class="meta-box-sortables ui-sortable">
									<form method="post">
										<?php
											$this->db_obj->prepare_items();
											$this->db_obj->display();
										?>
									</form>
								</div>
							</div>
						</div>
					<br class="clear">
					</div>
				</div>
			<?php
		}
	}
}
?>