<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_DB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_Main_DB' ) ) {
	/** Main plugin class */
	class APC_Main_DB {
		/**
		 * Main instance.
		 *
		 * @var object
		 */
		private static $instance;

		/**
		 * Main Admin instance.
		 *
		 * @var object
		 */
		public $admin = null;

		/**
		 * Main Frontend instance.
		 *
		 * @var object
		 */
		public $frontend = null;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 *  Class constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'apc_db_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-apc-db.php',
						'includes/class-apc-db-wp-list.php',
					),
					'admin'    => array(
						'includes/class-apc-db-admin.php',
					),
					'frontend' => array(),
				)
			);

			$this->require( $require );

			// Finally call the init function.
			$this->init();

		}

		/**
		 * _require Add the main classes' file
		 *
		 * @param  array $main_classes Set of files to be included in the plugin.
		 * @return void
		 */
		protected function require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( APC_DB_DIR_PATH . $class ) ) {
						require_once APC_DB_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = APC_DB_Admin::get_instance();
			}
		}
	}
}

if ( ! function_exists( 'apc_main_db_call' ) ) {
	/** Run a single instance */
	function apc_main_db_call() {
		return APC_Main_DB::get_instance();
	}
}
