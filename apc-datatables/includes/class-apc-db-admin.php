<?php
/**
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_DB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_DB_Admin' ) ) {
	/**
	 * Class admin of Database plugin Apc_db_Admin
	 */
	class Apc_DB_Admin {
		/**
		 * Main Instance
		 *
		 * @var apc_db_Admin
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * APC_DB_Admin constructor.
		 */
		private function __construct() {
			add_action( 'admin_menu', array( $this, 'create_menu_for_database_view' ) );
		}

		/**
		 * View_meta_boxes
		 *
		 * @param mixed $post WordPress inline argument for post.
		 * @return void
		 */
		public function view_meta_boxes( $post ) {
			apc_tt_get_view( '/metaboxes/plugin-skeleton-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 *  Create submenu for general options
		 */
		public function create_menu_for_database_view() {
				add_menu_page(
					esc_html__( 'Raffle Database', 'apc_database' ),
					esc_html__( 'View Raffle Database', 'apc_database' ),
					'view_apc_database',
					'menu_apc_database',
					array( $this, 'apc_database_custom_menu_page' ),
					'',
					40
				);

				$this->db_obj = new Datatable_List();

		}
		/** Callback custom menu page */
		public function apc_database_custom_menu_page() {
			?>
				<div class="wrap">
					<h2>WP_List_Table Class Example</h2>

					<div id="poststuff">
						<div id="post-body" class="metabox-holder columns-2">
							<div id="post-body-content">
								<div class="meta-box-sortables ui-sortable">
									<form method="post">
										<?php
											$this->db_obj->prepare_items();
											$this->db_obj->display();
										?>
									</form>
								</div>
							</div>
						</div>
					<br class="clear">
					</div>
				</div>
			<?php
		}
	}
}
?>
