<?php

if ( ! class_exists( 'Datatable_List' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

	/**
	 * WP_Table for display datatable in admin.
	 */
	class Datatable_List extends WP_List_Table {

		/** Class constructor */
		public function __construct() {

			parent::__construct(
				array(
					'singular' => __( 'Participant', 'sp' ), // singular name of the listed records.
					'plural'   => __( 'Participants', 'sp' ), // plural name of the listed records.
					'ajax'     => false, // should this table support ajax?
				)
			);
		}

		/**
		 * Retrieve customer’s data from the database
		 *
		 * @param int $per_page Entries displayed per page.
		 * @param int $page_number Number page.
		 *
		 * @return mixed
		 */
		public static function get_participants( $per_page = 5, $page_number = 1 ) {

			global $wpdb;

			$sql  = 'SELECT * FROM wp_apc_db_raffle';
			$sql .= " LIMIT $per_page";

			$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

			$result = $wpdb->get_results( $sql, 'ARRAY_A' );
			return $result;
		}

		/** Text displayed when no customer data is available */
		public function no_items() {
			esc_attr_e( 'No participants have entered :(.', 'sp' );
		}

		/**
		 * Returns the count of records in the database.
		 *
		 * @return null|string
		 */
		public static function record_count() {
			global $wpdb;

			$sql = 'SELECT COUNT(*) FROM wp_apc_db_raffle';

			return $wpdb->get_var( $sql );
		}


		/**
		 *  Associative array of columns
		 *
		 * @return array
		 */
		public function get_columns() {
			$columns = array(
				'nombre'    => __( 'First Name', 'sp' ),
				'apellidos' => __( 'Last Name', 'sp' ),
				'email'     => __( 'Email', 'sp' ),
			);

			return $columns;
		}

		/**
		 * Handles data query and filter, sorting, and pagination.
		 */
		public function prepare_items() {

			$columns = $this->get_columns();

			$this->_column_headers = array( $columns );

			$per_page     = $this->get_items_per_page( 'customers_per_page', 5 );
			$current_page = $this->get_pagenum();
			$total_items  = self::record_count();

			$this->set_pagination_args(
				array(
					'total_items' => $total_items, // WE have to calculate the total number of items.
					'per_page'    => $per_page, // WE have to determine how many items to show on a page.
				)
			);

			$this->items = self::get_participants( $per_page, $current_page );
		}


		/**
		 * Render a column when no column specific method exists.
		 *
		 * @param array  $item Name Array of all results.
		 * @param string $column_name Columns listed in get_columns().
		 *
		 * @return mixed
		 */
		public function column_default( $item, $column_name ) {
			switch ( $column_name ) {
				case 'nombre':
				case 'apellidos':
				case 'email':
					return $item[ $column_name ];
				default:
					return $item; // Show the whole array for troubleshooting purposes.
			}
		}
	}
}


