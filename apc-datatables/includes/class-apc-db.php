<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'APC_DB' ) ) {
	/**
	 * Booking Database
	 */
	class APC_DB {
		/**
		 * DB version.
		 *
		 * @var string $version
		 */
		public static $version = '1.1.0';

		/**
		 * Name of the table.
		 *
		 * @var string $auction_table
		 */
		public static $auction_table = 'apc_db_raffle';

		/**
		 * Constructor
		 *
		 * @return YITH_WCBK_DB
		 */
		private function __construct() {
		}

		/** Create table function */
		public static function install() {
			self::create_db_table();
		}

		/**
		 * Create table for Raffle
		 *
		 * @param bool $force Trigger to create the table.
		 */
		public static function create_db_table( $force = false ) {
			global $wpdb;

			$current_version = get_option( 'apc_db_version' );

			if ( $force || $current_version !== self::$version ) {
				$wpdb->hide_errors();

				$table_name      = $wpdb->prefix . self::$auction_table; // The table always must have a prefix.
				$charset_collate = $wpdb->get_charset_collate();

				$sql
					= "CREATE TABLE $table_name (
					`user_id` bigint(20) NOT NULL AUTO_INCREMENT,
					`nombre` varchar(20) NOT NULL,
					`apellidos` varchar(40) NOT NULL,
					`email` varchar(40) NOT NULL,
					PRIMARY KEY (user_id)
					) $charset_collate;";

				if ( ! function_exists( 'dbDelta' ) ) {
					require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				}
				dbDelta( $sql );  // Use DBdelta function for create the table.
				update_option( 'apc_db_version', self::$version );
			}
		}

		/**
		 * Insert Rows into table
		 *
		 * @param array $args MUST be filled with all the db fields.
		 * @return bool operation has been made or not.
		 */
		public static function insert_row_into_db( $args = array() ) {
			if ( empty( $args ) ) {
				return false;
			}

			global $wpdb;

			/* Let's extract the variables */
			$nombre_    = $args['nombre'];
			$apellidos_ = $args['apellidos'];
			$email_     = $args['email'];

			$wpdb->hide_errors();

			$table_name = $wpdb->prefix . self::$auction_table; // The table always must have a prefix.

			$sql
				= "INSERT INTO $table_name 
				( nombre, apellidos, email) VALUES
				( '$nombre_', '$apellidos_', '$email_')";

			if ( ! function_exists( 'dbDelta' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			}
			dbDelta( $sql );  // Use DBdelta function.

			return true;
		}

		/**
		 * Check whether a row exists with the same... email?
		 * 
		 * @param string email. Seems a unique field to be searched on.
		 */
		public static function is_in_db( $email_ ) {
			global $wpdb;

			$wpdb->hide_errors();

			$table_name      = $wpdb->prefix . self::$auction_table; // The table always must have a prefix.

			$results_ = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT user_id FROM wp_apc_db_raffle
					WHERE email = %s LIMIT 1",
					$email_
				)
			);

			return ( ! empty( $results_ ) );
		}

		/** Returns the whole db (used to display) */
		public static function return_all_db() {
			global $wpdb;

			$table_name = $wpdb->prefix . self::$auction_table; // The table always must have a prefix.

			$results_ = $wpdb->get_results(
				'SELECT * FROM wp_apc_db_raffle'
			);

			return $results_;
		}
	}
}
?>
