<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */


if ( ! function_exists( 'apc_tt_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function apc_tt_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = APC_TT_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
if ( ! function_exists( 'apc_db_get_view' ) ) {
	/**
	 * Apc_db_get_view
	 *
	 * @param  mixed $file_name name of the file you want to include.
	 * @param  mixed $args (array) (optional) Arguments to retrieve.
	 * @return void
	 */
	function apc_db_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = APC_DB_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


if ( ! function_exists( 'database_wp_enqueue_scripts' ) ) {
	/**
	 * CSS Register Function for FrontEnd
	 *
	 * @return void
	 */
	function database_wp_enqueue_scripts() {
		$css_url = APC_DB_DIR_ASSETS_CSS_URL . '/apc_db.css';
		wp_register_style( 'apc-database-style', $css_url );
		wp_enqueue_style( 'apc-database-style' );
	}
	add_action( 'wp_enqueue_scripts', 'database_wp_enqueue_scripts' );
}

if ( ! function_exists( 'apc_shortcode_database_form' ) ) {
	/**
	 * Shortcode of example database & its form
	 *
	 *  @param array $atts Attributes or parameters of the shortcode, look at array below for possible arguments.
	 */
	function apc_shortcode_database_form( $atts ) {
		global $wpdb;
		$results_ = APC_DB::return_all_db();
		$state_   = 0; // State in this form (error, success, original).

		if ( isset( $_POST['apc_raffle_checkbox'] ) ) {

			$user_log = wp_get_current_user();

			if ( 0 !== $user_log->ID ) { // A logged user.
				$email = $user_log->user_email;

				if ( APC_DB::is_in_db( $email ) ) {
					// Show Up if it was already in.
					$state_ = 1;

				} else {

					// Insert Here.
					$args = array(
						'nombre'    => $user_log->user_firstname,
						'apellidos' => $user_log->user_lastname,
						'email'     => $user_log->user_email,
					);
					$result_ = APC_DB::insert_row_into_db( $args );

					$state_ = 2;

					// Show Up Congrats! It's in.
				}
			} else {
				$state_ = 3;

				if ( isset ( $_POST['apc_raffle_email'] ) ) {
					if ( ! filter_var( sanitize_text_field( $_POST['apc_raffle_email'] ), FILTER_VALIDATE_EMAIL ) ) {
						$args = array(
							'nombre'    => sanitize_text_field( $_POST['apc_raffle_first_name'] ),
							'apellidos' => sanitize_text_field( $_POST['apc_raffle_last_name'] ),
							'email'     => sanitize_text_field( $_POST['apc_raffle_email'] ),
						);
						$result_ = APC_DB::insert_row_into_db( $args );

						$state_ = 2;
					} else {
						$state_ = 1;
					}
				}
			}
		}
		?>
				<table border='1'>
					<tr>
						<th>Nombre</th> 
						<th>Apellidos</th>
						<th>Email</th>
					</tr>
				<?php
				if ( ! empty( $results_ ) ) {
					foreach ( $results_ as $rows ) {
						?>
						<tr>
							<td><?php echo esc_html( $rows->nombre ); ?></td>
							<td><?php echo esc_html( $rows->apellidos ); ?></td>
							<td><?php echo esc_html( $rows->email ); ?></td> 
						</tr>
						<?php
					}
				}
				?>
				</table>
			<form action="" method="post">
				<!-- Hidden form for no-logged users-->
				<?php if ( 3 === $state_ ) { ?>
					<label class="raffle_name_input" for="raffle_name_input"><?php esc_html_e( 'Nombre', 'apc_db_raffle' ); ?></label>
					<input type="text" class="graffle_first_name_input_text" name="apc_raffle_first_name" id="apc_raffle_first_name_input"  value="">
					<label class="raffle_surname_input" for="raffle_surname_input"><?php esc_html_e( 'Apellidos', 'apc_db_raffle' ); ?></label>
					<input type="text" class="graffle_last_name_input_text" name="apc_raffle_last_name" id="apc_raffle_last_name_input"  value="">
					<label class="raffle_email_input" for="raffle_email_input"><?php esc_html_e( 'Email', 'apc_db_raffle' ); ?></label>
					<input type="text" class="graffle_email_input_text" name="apc_raffle_email" id="apc_raffle_email_input"  value="">
				<?php } ?>
				<div class="apc_button_and_alert">
					<button name="submit" type="submit" class="btn btn-primary"><?php echo esc_html_e( 'Register', 'apc_db_raffle' ); ?></button>
					<input type="hidden" name="action" value="apc_raffle_form">
					<input type="checkbox" name="apc_raffle_checkbox" id="apc_raffle_checkbox_input"> Acepto inscribirme en la encuesta. </input>
					<?php if ( 1 === $state_ ) { ?>
						<p class='apc-raffle-error'> Error, Ya estás inscrito en el sorteo. </p>  
					<?php } elseif ( 2 === $state_ ) { ?> 
						<p class='apc-raffle-success'> Éxito, disfruta del sorteo. </p>
					<?php } ?>
				</div>
			</form>
			<?php
			wp_nonce_field( 'action_raffle_form', 'raffle_form' );

	}
}
add_shortcode( 'sc_print_db_form', 'apc_shortcode_database_form' );

if ( ! function_exists( 'apc_db_give_permissions' ) ) {
	/**
	 * Give raffle permissions to admin
	 *
	 * @return void
	 */
	function apc_db_give_permissions() {
		$admin_role = get_role( 'administrator' );
		$admin_role->add_cap( 'view_apc_database' );
	}
}

add_action( 'admin_init', 'apc_db_give_permissions', 999 ); // Although admin_init, or even after_setup_theme can also be used.
?>
