<?php 

?>

<div class="wrap">
	<h2>WP_List_Table Class Example</h2>

	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<form method="post">
						<?php
						    $this->customers_obj->prepare_items();
                            $this->customers_obj->display();
                        ?>
					</form>
				</div>
			</div>
		</div>
	<br class="clear">
	</div>
</div>