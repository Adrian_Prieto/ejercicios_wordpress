<?php

/**
 * Plugin Name: APC Database
 * Description: Test Plugins for Databases
 * Version: 1.0.0
 * Author: Adrián Prieto
 * Text Domain: apc-db
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;  // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'APC_DB_VERSION' ) ) {
	define( 'APC_DB_VERSION', '1.0.0' );
}

if ( ! defined( 'APC_DB_DIR_URL' ) ) {
	define( 'APC_DB_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'APC_DB_DIR_ASSETS_URL' ) ) {
	define( 'APC_DB_DIR_ASSETS_URL', APC_DB_DIR_URL . 'assets' );
}

if ( ! defined( 'APC_DB_DIR_ASSETS_CSS_URL' ) ) {
	define( 'APC_DB_DIR_ASSETS_CSS_URL', APC_DB_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'APC_DB_DIR_ASSETS_JS_URL' ) ) {
	define( 'APC_DB_DIR_ASSETS_JS_URL', APC_DB_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'APC_DB_DIR_PATH' ) ) {
	define( 'APC_DB_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'APC_DB_DIR_INCLUDES_PATH' ) ) {
	define( 'APC_DB_DIR_INCLUDES_PATH', APC_DB_DIR_PATH . '/includes' );
}

if ( ! defined( 'APC_DB_DIR_TEMPLATES_PATH' ) ) {
	define( 'APC_DB_DIR_TEMPLATES_PATH', APC_DB_DIR_PATH . '/templates' );
}

if ( ! defined( 'APC_DB_DIR_VIEWS_PATH' ) ) {
	define( 'APC_DB_DIR_VIEWS_PATH', APC_DB_DIR_PATH . 'views' );
}

/**
 * Include the scripts
 */
if ( ! function_exists( 'apc_db_init_classes' ) ) {

	/** Calling all included files and classes*/
	function apc_db_init_classes() {

		load_plugin_textdomain( 'apc_testimonials', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins.
		require_once APC_DB_DIR_INCLUDES_PATH . '/class-apc-main-db.php';

		if ( class_exists( 'APC_Main_DB' ) ) {
			/*
			*	Call the main function
			*/
			apc_main_db_call();
			APC_DB::install();
		}
	}
}

add_action( 'plugins_loaded', 'apc_db_init_classes', 11 );




