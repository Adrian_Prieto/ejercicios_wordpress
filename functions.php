<?php
// 1.- Mostrar al final de cada post un mensaje tipo "Este post ha sido escrito por Christian
function add_text_to_content( $content ) {
	$content .= '<p>Este post ha sido escrito por Christian<p>';
	return $content;
}
add_filter( 'the_content', 'add_text_to_content' );


// 2.- Partiendo del ejercicio anterior, mostrar el nombre del autor que ha hecho ese post
function add_text_to_content2( $content ) {
	$content .= '<p>Este post ha sido escrito por ' . get_the_author() . '.<p>';
	return $content;
}
add_filter( 'the_content', 'add_text_to_content2' );

// 3.- Crear dos post distintos con contenido y al final de cada post, escribir el autor del otro post.
function add_text_to_content3( $content ) {
	$id = get_the_ID();

	if ( 35 === $id ) { // El post escrito por Juan.
		$content .= '<p>Este post me ha ayudado mi coautor Pepe<p>';
	} elseif ( 38 === $id ) { // El post escrito por Pepe.
		$content .= '<p>Este post me ha ayudado mi coautor Juan<p>';
	}
	return $content;
}
add_filter( 'the_content', 'add_text_to_content3' );


// 4.- A la hora de guardar un post, guardar una serie de datos aleatorios.
function save_metadata4( $post_id ) {
	add_post_meta( $post_id, 'nombre_madre', 'Maria' );
	add_post_meta( $post_id, 'nombre_padre', 'Jose' );
	add_post_meta( $post_id, 'edad', 20 );
}

function add_text_to_content4( $post_id, $post, $update ) {
	save_metadata4( $post_id );

	$post->post_content .= 'Hijo de ' . get_post_meta( $post_id, 'nombre_madre', true ) 
				. ' y de ' . get_post_meta( $post_id, 'nombre_padre', true )
				. ', con ' . get_post_meta( $post_id, "edad", true ) . ' años de edad.';

	return $post;
}


function add_text_to_content4_1( $data, $postarr ) {
	save_metadata4( $post_id );

	$data .= 'Hijo de ' . get_post_meta( $post_id, 'nombre_madre', true ) 
				. ' y de ' . get_post_meta( $post_id, 'nombre_padre', true )
				. ', con ' . get_post_meta( $post_id, 'edad', true ) . " años de edad.";	
	return $data;
}

add_filter( 'save_post', 'add_text_to_content4', 12, 3 );
add_filter( 'wp_insert_post_data', 'add_text_to_content4_1', 10, 2 );

// 5.- Añadir en los post el título de quién lo ha escrito. 6.- Añadir en el mismo post, la fecha de publicación del post
function add_text_to_content5( $content ) {
	$content .= '<p>' . get_the_title() . ', escrito por ' . get_the_author() . ' en ' . get_the_date() . '<p>';
	return $content;
}
add_filter( 'the_content', 'add_text_to_content5', 10, 2 );

// 7.- Del post, borrar el nombre del autor sin borrar nuestro código personalizado que hemos creado previamente.
function remove_author( $display_name ) {
	if ( ! is_admin() ) {
		return '';
	}
	return $display_name;
}
add_filter( 'the_author', 'remove_author');


// Insertar una hoja de estilo en la página de single post que cambie el color del texto 
function apc_themescripts () {
	if ( is_single() ) {
		wp_register_style( 'apc_changeColor', get_template_directory_uri() . '/assets/css/changeColor.css');
		wp_enqueue_style( 'apc_changeColor' );
	}
}
add_action( 'wp_enqueue_scripts', 'apc_themescripts' );


// Crear un shortcode que le pase el post_id y me devuelva el content
function apc_postid_content ( $atts, $content = null ) {
	$atts = shortcode_atts(
		array( 
			'id_post' => 'POST_ID',
		),
		$atts
	);

	return get_post_field( 'post_content', $atts['id_post'] );
}
add_shortcode( 'postid_content', 'apc_postid_content' );
// var_dump( do_shortcode( "[postid_content id_post=35]" ) );


//Crear un shortcode que me liste los últimos 5 post que se han creado en la web
function apc_list5posts( $atts = null, $content = null ) {
	$the_query = new WP_Query( 'posts_per_page=5' );
	echo '<ul>';
	// Start our WP Query.
	while ( $the_query->have_posts() ) :
		$the_query->the_post();
		// Display the Post Title with Hyperlink.
		echo '<li><a href="' . esc_html( the_permalink() ) . '">' . esc_html( the_title() ) . '</a></li><li>';
		// Display the Post Excerpt.
		echo esc_html( the_excerpt( __( '(more…)' ) ) ) . '</li>';
		// Repeat the process and reset once it hits the limit.
	endwhile;
	echo '</ul>';
	wp_reset_postdata();
}
add_shortcode( 'list5posts', 'apc_list5posts' );
//var_dump (do_shortcode("[list5posts]"));

//Crear un shortcode que muestre los datos de un usuario --> nombre, apellidos, nickname
function apc_get_author_data ( $atts, $content = null) {
	$atts = shortcode_atts(
		array (
			'id_author' => 'AUTHOR_ID',
		), $atts
	);

	$result = array(
		'email'      => get_the_author_meta( 'user_email', $atts['id_author']),
		'first_name' => get_the_author_meta( 'first_name', $atts['id_author']),
		'last_name'  => get_the_author_meta( 'last_name', $atts['id_author']),
		'nickname'   => get_the_author_meta( 'display_name', $atts['id_author']),
	);
	return $result;
}
add_shortcode( 'get_author_data', 'apc_get_author_data' );
//var_dump (do_shortcode("[get_author_data id_author=3]"));

?>