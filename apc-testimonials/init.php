<?php

/*
 * Plugin Name: APC Testimonials
 * Description: Test Plugins for Testimonials
 * Version: 1.0.0
 * Author: Adrián Prieto
 * Text Domain: apc_testimonial
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;  // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'APC_TT_VERSION' ) ) {
	define( 'APC_TT_VERSION', '1.0.0' );
}

if ( ! defined( 'APC_TT_DIR_URL' ) ) {
	define( 'APC_TT_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'APC_TT_DIR_ASSETS_URL' ) ) {
	define( 'APC_TT_DIR_ASSETS_URL', APC_TT_DIR_URL . 'assets' );
}

if ( ! defined( 'APC_TT_DIR_ASSETS_CSS_URL' ) ) {
	define( 'APC_TT_DIR_ASSETS_CSS_URL', APC_TT_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'APC_TT_DIR_ASSETS_JS_URL' ) ) {
	define( 'APC_TT_DIR_ASSETS_JS_URL', APC_TT_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'APC_TT_DIR_PATH' ) ) {
	define( 'APC_TT_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'APC_TT_DIR_INCLUDES_PATH' ) ) {
	define( 'APC_TT_DIR_INCLUDES_PATH', APC_TT_DIR_PATH . '/includes' );
}

if ( ! defined( 'APC_TT_DIR_TEMPLATES_PATH' ) ) {
	define( 'APC_TT_DIR_TEMPLATES_PATH', APC_TT_DIR_PATH . '/templates' );
}

if ( ! defined( 'APC_TT_DIR_VIEWS_PATH' ) ) {
	define( 'APC_TT_DIR_VIEWS_PATH', APC_TT_DIR_PATH . 'views' );
}

/**
 * Include the scripts
 */
if ( ! function_exists( 'apc_tt_init_classes' ) ) {

	/** Calling la */
	function apc_tt_init_classes() {

		load_plugin_textdomain( 'apc_testimonial', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins.
		require_once APC_TT_DIR_INCLUDES_PATH . '/class-apc-testimonial.php';

		if ( class_exists( 'APC_Testimonial' ) ) {
			/*
			*	Call the main function
			*/
			apc_testimonial_call();
		}
	}
}


add_action( 'plugins_loaded', 'apc_tt_init_classes', 11 );




