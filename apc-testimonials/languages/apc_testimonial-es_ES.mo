��    ,      |  ;   �      �     �     �     �               /     C  
   O     Z  
   h     s     {     �     �     �     �     �     �     �                    /     D     [     s     z          �     �  
   �     �     �     �     �               +     B  +   F  .   r  #   �  &   �  �  �     w     �     �     �     �     �     	     	     +	     =	  
   N	     Y	     n	     �	     �	     �	     �	     �	     �	     �	     
     
     $
     6
     J
     _
     k
     o
     �
     �
     �
  	   �
     �
  
   �
     �
     �
          +     F     J     e     �     �                                  &                   "              #   $            ,   
                 (                !   %         )   '                          	   *                        +          APC Testimonials Add New Hierarchical Add New No Hierarchical Additional information All Hierarchical All No Hierarchical Badge Color Badge Text Border radius Color link Company Company URL Edit Hierarchical Edit No Hierarchical Enable Badge Hierarchical Hover effect New Hierarchical Name New No Hierarchical Name No Hierarchical Number Parent Hierarchical Parent Hierarchical: Parent No Hierarchical Parent No Hierarchical: Rating Role Search Hierarchical Search No Hierarchical Section Show image Stars Test Plugins for Testimonials Testimonial Testimonial Options Testimonial post type Update Hierarchical Update No Hierarchical Vip taxonomy general nameHierarchical taxonomy taxonomy general nameNo Hierarchical taxonomy taxonomy singular nameHierarchical taxonomy singular nameNo Hierarchical Project-Id-Version: APC Testimonials 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/apc-testimonials
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-12-18 10:47+0100
X-Generator: Poedit 2.4.2
X-Domain: apc_testimonial
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Testimonios de APC Añadir nuevo jerárquico Añadir nuevo NO jerárquico Información adicional Todas las jerarquías Todas las NO jerarquías Color de insignia Texto de insignia Tamaño del borde Color del enlace Compañía URL de la compañía Editar jerarquía Editar sin jerarquía Activar insignia Jerárquico Efecto de destacado Nuevo nombre jerárquico Nuevo nombre sin jerarquía Sin Jerarquía Número Jerarquía padre Jerarquía padre: NO Jerarquía padre NO Jerarquía padre: Puntuación Rol Buscar jerarquía Buscar sin jerarquía Sección Mostrar imagen Estrellas Plugin de prueba de Testimonio Testimonio Opciones del testimonio Tipo de post: Testimonio Actualizar jerárquico Actualizar sin jerárquico VIP Con Taxonomía Jerárquica Sin Taxonomía Jerárquica Jerárquico Sin Jerarquía 