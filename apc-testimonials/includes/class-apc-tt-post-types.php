<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_TT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_TT_Post_Types' ) ) {
	/**
	 * Main class for CPT
	 */
	class APC_TT_Post_Types {

		/**
		 * Main instance.
		 *
		 * @var object
		 */
		private static $instance;

		/**
		 * Name of the CPT.
		 *
		 * @var string
		 */
		public static $post_type = 'apc_testimonial';

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );

			add_filter( 'register_post_type_args', 'apc_tt_change_capabilities', 10, 2 );
		}

		/**
		 * Setup the custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label'        => __( 'Testimonial', 'apc_testimonial' ),
				'description'  => __( 'Testimonial post type', 'apc_testimonial' ),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

		/** Function to set name of herarchical and non-herachical taxonomies of this CPT */
		public function register_taxonomy() {
			// Add new taxonomy, make it hierarchical (like categories).
			$labels = array(
				'name'              => _x( 'Hierarchical taxonomy', 'taxonomy general name', 'apc_testimonial' ),
				'singular_name'     => _x( 'Hierarchical', 'taxonomy singular name', 'apc_testimonial' ),
				'search_items'      => __( 'Search Hierarchical', 'apc_testimonial' ),
				'all_items'         => __( 'All Hierarchical', 'apc_testimonial' ),
				'parent_item'       => __( 'Parent Hierarchical', 'apc_testimonial' ),
				'parent_item_colon' => __( 'Parent Hierarchical:', 'apc_testimonial' ),
				'edit_item'         => __( 'Edit Hierarchical', 'apc_testimonial' ),
				'update_item'       => __( 'Update Hierarchical', 'apc_testimonial' ),
				'add_new_item'      => __( 'Add New Hierarchical', 'apc_testimonial' ),
				'new_item_name'     => __( 'New Hierarchical Name', 'apc_testimonial' ),
				'menu_name'         => __( 'Hierarchical', 'apc_testimonial' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'apc_tt_tax_hierarchical' ),
				'capabilities'      => array(
					'manage_terms' => 'manage_apc_terms',
					'edit_terms'   => 'edit_apc_terms',
					'delete_terms' => 'delete_apc_terms',
					'assign_terms' => 'assign_apc_terms',
				),
			);

			register_taxonomy( 'apc_tt_tax_hierarchical', array( self::$post_type ), $args );

			// Add new taxonomy, make it no hierarchical (like tags).
			$labels1 = array(
				'name'              => _x( 'No Hierarchical taxonomy', 'taxonomy general name', 'apc_testimonial' ),
				'singular_name'     => _x( 'No Hierarchical', 'taxonomy singular name', 'apc_testimonial' ),
				'search_items'      => __( 'Search No Hierarchical', 'apc_testimonial' ),
				'all_items'         => __( 'All No Hierarchical', 'apc_testimonial' ),
				'parent_item'       => __( 'Parent No Hierarchical', 'apc_testimonial' ),
				'parent_item_colon' => __( 'Parent No Hierarchical:', 'apc_testimonial' ),
				'edit_item'         => __( 'Edit No Hierarchical', 'apc_testimonial' ),
				'update_item'       => __( 'Update No Hierarchical', 'apc_testimonial' ),
				'add_new_item'      => __( 'Add New No Hierarchical', 'apc_testimonial' ),
				'new_item_name'     => __( 'New No Hierarchical Name', 'apc_testimonial' ),
				'menu_name'         => __( 'No Hierarchical', 'apc_testimonial' ),
			);

			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'apc_tt_no_hierarchical' ),
				'capabilities'      => array(
					'manage_terms' => 'manage_apc_terms',
					'edit_terms'   => 'edit_apc_terms',
					'delete_terms' => 'delete_apc_terms',
					'assign_terms' => 'assign_apc_terms',
				),
			);

			register_taxonomy( 'apc_tt_no_hierarchical_tax', array( self::$post_type ), $args1 );
		}
	}
}
