<?php
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'APC_TT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_TT_Frontend' ) ) {
	/**
	 * Class with functions used in frontend
	 */
	class APC_TT_Frontend {

		/**
		 * Main Instance
		 *
		 * @var object Instance of the class to be called in get_instance
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Main plugin Instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Frotend constructor.
		 */
		private function __construct() {
		}
	}
}
