<?php

/**
 * Register this widget into a hook
 *
 * @return void
 */
function register_apc_tt_widget() {
	register_widget( 'APC_TT_Widget' );
}
add_action( 'widgets_init', 'register_apc_tt_widget' );

if ( ! class_exists( 'APC_TT_Widget' ) ) {
	/**
	 * Main Example of Widget using testimonials
	 */
	class APC_TT_Widget extends WP_Widget {
		/**
		 * Mandatory constructor of the widget.
		 *
		 * @return void
		 */
		public function __construct() {
			$widget_ops = array(
				'classname'   => 'APC_Testimonial_Widget',
				'description' => 'Default template for APC Testimonials',
			);
			parent::__construct( 'APC_TT_Widget', 'APC_TT_Widget', $widget_ops );
		}
		/**
		 * Outputs the content of the widget.
		 *
		 * @param  array $args Main arguments of the Widget.
		 * @param  mixed $instance Instance with the arguments from the form.
		 * @return void
		 */
		public function widget( $args, $instance ) {
			// Args can't be escaped,  as style and content won't work. Tried with every escaping technique.
			echo $args['before_widget'];

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . esc_html( apply_filters( 'widget_title', $instance['title'] ) ) . $args['after_title'];
			}

			do_shortcode( '[sc_widget_testimonials number=' . $instance['number_show'] . ']' );
			echo $args['after_widget'];
		}

		/**
		 * Form to be displayed in admin.
		 *
		 * @param  mixed $instance Object to be copied into all the remaining functions of the widget.
		 * @return void
		 */
		public function form( $instance ) {
			$number_show = isset( $instance['number_show'] ) ? esc_attr( $instance['number_show'] ) : 3;
			?>

			<div style="max-height: 120px; overflow: auto;">
				<label for="widget_tt_number"> <?php esc_html_e( 'Number:' ); ?> </label>
				<input type="number" id="<?php echo esc_html( $this->get_field_id( 'number_show' ) ); ?>" 
					name="<?php echo esc_html( $this->get_field_name( 'number_show' ) ); ?>" 
					value="<?php echo esc_attr( $number_show ); ?>"/>
			</div>

			<?php
		}

		/**
		 * Processes widget options to be saved.
		 *
		 * @param  mixed $new_instance Instance got from the form.
		 * @param  mixed $old_instance The instance to be replaced.
		 * @return instance
		 */
		public function update( $new_instance, $old_instance ) {

			$instance                = $old_instance;
			$instance['number_show'] = ( ! empty( $new_instance['number_show'] ) ) ? $new_instance['number_show'] : 5;

			return $instance;
		}
	}
}
?>
