<?php
/**
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'APC_TT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'APC_Testimonial' ) ) {
	/** Main plugin class */
	class APC_Testimonial {
		/** Main Instance */
		private static $instance;

		/** Main Admin Instance*/
		public $admin = null;

		/** Main Frontpage Instance */
		public $frontend = null;

		/** Main plugin Instance */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 *  Class constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'apc_tt_require_class',
				array(
					'common'   => array(
						'includes/class-apc-tt-post-types.php',
						'includes/functions.php',
						'includes/class-apc-tt-widget.php',
					),
					'admin'    => array(
						'includes/class-apc-tt-admin.php',
						'includes/class-apc-tt-widget.php',
					),
					'frontend' => array(
						'includes/class-apc-tt-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			// Finally call the init function.
			$this->init();

		}

		/** Add the main classes file */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( APC_TT_DIR_PATH . $class ) ) {
						require_once APC_TT_DIR_PATH . $class;
					}
				}
			}
		}

		/** A safe way to call the construct */
		public function init_classes() {
			APC_TT_Post_Types::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = APC_TT_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = APC_TT_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'apc_testimonial_call' ) ) {
	/** Run a single instance */
	function apc_testimonial_call() {
		return APC_Testimonial::get_instance();
	}
}
